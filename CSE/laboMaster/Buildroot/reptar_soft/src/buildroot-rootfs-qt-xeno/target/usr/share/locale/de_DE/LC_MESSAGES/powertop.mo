��    r      �  �   <      �	  $   �	     �	     �	  	   �	  9   �	     *
     2
  ,   7
     d
     u
     |
     �
  "   �
  -   �
     �
  !   �
  	     
   (     3     ;     T     k     }  %   �  *   �     �               2     F     O     [  	   h  #   r  ?   �     �     �     �  /   �     .     >  	   L     V     f  
   k     v     �     �     �  !   �     �       $     -   ?     m      |     �     �     �  
   �  $   �  	     =     /   V     �     �     �     �     �                    -  .   B     q  *   y  >   �  ,   �  5     I   F     �     �     �     �      �     �     �                *  	   K  &   U  
   |  .   �  
   �     �  3   �            "        ?     K     h     ~  !   �  0   �     �     �       !   +     M  .   d     �  �  �  )   o     �     �     �  R   �             5        B     R     Z     l  #   �  2   �     �  %   �     	  
             )  "   A     d     y  (   �  )   �     �  -   	     7     R  	   o     y     �     �  ,   �  Y   �     7     D  ,   L  ;   y     �     �     �     �               $     ?      W     x  &   �     �  
   �  ,   �  6        K     Z     z  !   �      �     �  .   �  	     I     R   V     �  -   �     �  ,   �  -   $     R     b     v  -   �  5   �     �  3   �  M   2  0   �  9   �  Z   �     F      [      j   	   ~   :   �   
   �      �      �   ,   
!  !   7!     Y!  ,   l!     �!  ?   �!     �!     �!  =   "  	   F"     P"  -   ]"     �"  $   �"     �"     �"  *   �"  <   %#     b#     }#     �#  )   �#     �#  9   �#     7$                E              C   d   `   K   <   I   M   c   P   D                        J       "   B   )          5       g   f          S   !   ^      &   l   -       2   e           N       R   %                                W       [   8   
   1   6       i       H       A          $           j   	   h             '              #                 7   \      Z   b   =   p       0          ]      n   V       ?   ;      L   9   3       Y       r      O   (   .       T          :      4          m       q   _       X   o      Q      >   *       /   a      @   U   ,       F   k   G   +                  Usage     Device name
     0 mW   Core   Package  <ESC> Exit | <Enter> Toggle tunable | <r> Window refresh  CPU %i %7sW %s device %s has no runtime power management .... device %s 
 Actual Audio codec %s: %s Audio codec %s: %s (%s) Autosuspend for USB device %s [%s] Autosuspend for unknown USB device %s (%s:%s) Bad Bluetooth device interface status C0 active C0 polling CPU use Calibrating USB devices
 Calibrating backlight
 Calibrating idle
 Calibrating radio devices
 Calibrating: CPU usage on %i threads
 Calibrating: CPU wakeup power consumption
 Calibrating: disk usage 
 Cannot create temp file
 Cannot load from file Cannot save to file Category Description Device stats Disk IO/s Enable Audio codec power management Estimated power: %5.1f    Measured power: %5.1f    Sum: %5.1f

 Events/s Exit Failed to mount debugfs!
 Finishing PowerTOP power estimate calibration 
 Frequency stats GFX Wakeups/s GPU ops/s GPU ops/seconds Good Idle stats Intel built in USB hub Leaving PowerTOP Loaded %i prior measurements
 Measuring workload %s.
 NMI watchdog should be turned off Network interface: %s (%s) Overview Overview of Software Power Consumers PCI Device %s has no runtime power management PCI Device: %s PS/2 Touchpad / Keyboard / Mouse Package Parameters after calibration:
 Power Aware CPU scheduler Power est. Power est.    Usage     Device name
 PowerTOP  PowerTOP %s needs the kernel to support the 'perf' subsystem
 PowerTOP is out of memory. PowerTOP is Aborting PowerTOP version Preparing to take measurements
 Radio device: %s Runtime PM for %s device %s Runtime PM for PCI Device %s SATA controller SATA disk: %s SATA link: %s Set refresh time out Starting PowerTOP power estimate calibration 
 Summary System baseline power is estimated at %sW
 Taking %d measurement(s) for a duration of %d second(s) each.
 The battery reports a discharge rate of %sW
 The estimated remaining time is %i hours, %i minutes
 Too many open files, please increase the limit of open file descriptors.
 Tunables USB device: %s USB device: %s (%s) Unknown Unknown issue running workload!
 Usage Usage: powertop [OPTIONS] VFS ops/sec and VM writeback timeout Wake-on-lan status for device %s Wakeups/s Wireless Power Saving for interface %s [=devnode] [=iterations] number of times to run each test [=seconds] [=workload] as well as support for trace points in the kernel:
 cpu package cpu package %i cpu_idle event returned no state?
 exiting...
 file to execute for workload generate a csv report generate a html report generate a report for 'x' seconds power or cpu_frequency event returned no state?
 print this help menu print version information run in "debug" mode runs powertop in calibration mode suppress stderr output uses an Extech Power Analyzer for measurements wakeups/second Project-Id-Version: PowerTOP
Report-Msgid-Bugs-To: "powertop@lists.01.org"
POT-Creation-Date: 2014-11-21 10:37-0800
PO-Revision-Date: 2013-11-05 19:48+0000
Last-Translator: Ettore Atalan <atalanttore@googlemail.com>
Language-Team: German (Germany) (http://www.transifex.com/projects/p/PowerTOP/language/de_DE/)
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
               Auslastung     Gerätename
     0 mW Kern Paket  <ESC> Beenden | <Enter> Abstimmbare Option umschalten | <r> Fenster aktualisieren CPU %i %7sW %s Gerät %s hat keine Energieverwaltung zur Laufzeit .... Gerät %s
 Aktuell Audiocodec %s: %s Audiocodec %s: %s (%s) AutoSuspend für USB-Gerät %s [%s] AutoSuspend für unbekanntes USB-Gerät %s (%s:%s) Schlecht Bluetooth-Gerät Schnittstellenstatus C0 aktiv C0 sammeln CPU-Nutzung Kalibriere USB-Geräte
 Kalibriere Hintergrundbeleuchtung
 Kalibriere Leerlauf
 Kalibriere Funkvorrichtungen
 Kalibriere CPU-Auslastung bei %i Thread
 Kalibriere CPU-Aufwach-Leistungsaufnahme
 Kalibriere Festplattennutzung
 Temporäre Datei konnte nicht erzeugt werden
 Kann nicht von Datei laden Kann Datei nicht abspeichern Kategorie Beschreibung Gerät-Statistiken Platten-Ein-/Ausgabe/s Energieverwaltung für Audiocodec aktivieren Geschätzter Stromverbrauch: %5.1f    Gemessener Stromverbrauch: %5.1f    Summe: %5.1f\n
 Ereignisse/s Beenden Das Einhängen von debugfs ist gescheitert!
 Schließe Kalibrierung der PowerTOP-Leistungsschätzung ab
 Frequenz-Statistiken GFX-Aufwachvorgänge/s GPU-Operationen/s GPU-Operationen/Sekunde Gut Leerlauf-Statistiken Intel integrierter USB Hub PowerTOP wird verlassen %i geladene vorherige Messungen
 Auslastung %s wird gemessen.
 NMI watchdog sollte ausgeschaltet sein Netzwerkschnittstelle: %s (%s) Übersicht Übersicht der Software-Leistungsverbraucher PCI-Gerät %s hat keine Energieverwaltung zur Laufzeit PCI-Gerät: %s PS/2-Tastfeld / Tastatur / Maus Paket Parameter nach der Kalibrierung:
 Leistungsbewusster CPU-Scheduler Gesch. Leistung Gesch. Leistung    Auslastung     Gerätename
 PowerTOP  PowerTOP %s benötigt einen Kernel der das 'perf'-Subsystem unterstützt
 PowerTOP hat keinen Speicherplatz mehr im Hauptspeicher. PowerTOP wird abgebrochen PowerTOP-Version Das Vornehmen von Messungen wird vorbereitet
 Funk-Gerät:%s Laufzeit-Energieverwaltung für %s Gerät %s Laufzeit-Energieverwaltung für PCI-Gerät %s SATA-Controller SATA-Festplatte: %s SATA-Verbindung: %s Aktualisierungs-Zeitüberschreitung festlegen Starte Kalibrierung der PowerTOP-Leistungsschätzung
 Zusammenfassung Leistungsaufnahme bei Grundauslastung beträgt %sW
 %d Messung(en) mit einer Dauer von je %d Sekunde(n) wird/werden vorgenommen.
 Die Batterie meldet eine Entladungsrate von %sW
 Die geschätzte Restzeit beträgt %i Stunden, %i Minuten
 Zu viele offene Dateien. Bitte erhöhen Sie die Begrenzung von offenen Dateideskriptoren.
 Abstimmbare Optionen USB-Gerät: %s USB-Gerät: %s (%s) Unbekannt Unbekanntes Ergebnis beim Ausführen der Arbeitsbelastung
 Auslastung Verwendung: powertop [OPTIONEN] VFS-Operationen/Sekunde und Zeitüberschreitung beim VM zurückschreiben Wake-On-LAN-Status für Gerät %s Aufwachvorgänge/s Drahtloses Stromsparen für Schnittstelle %s [=Entwicklungsmodus] [=Wiederholungen] Anzahl der Wiederholungen für jeden Testlauf [=Sekunden] [=Auslastung] sowie Unterstützung für Ablaufverfolgungspunkte im Kernel:
 CPU-Paket CPU-Paket %i cpu_idle-Ereignis gab keinen Status zurück?
 Verlassen ...
 Für Auslastung auszuführende Datei Einen CSV-Bericht generieren Einen HTML-Bericht generieren Einen Bericht für 'x'-Sekunden generieren power- oder cpu_frequency-Ereignis gab kein Status zurück?
 Dieses Hilfemenü ausgeben Versionsinformation ausgeben in "Debug"-Modus ausführen führt Powertop im Kalibrierungsmodus aus stderr-Ausgabe unterdrücken verwendet einen Extech Leistungsanalysator für Messungen Aufwachvorgänge/Sekunde 