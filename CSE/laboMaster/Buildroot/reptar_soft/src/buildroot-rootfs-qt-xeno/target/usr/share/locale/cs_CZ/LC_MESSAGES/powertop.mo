��    q      �  �   ,      �	  $   �	     �	     �	  	   �	  9   �	     
     

  ,   
     <
     M
     T
     g
  "   
  -   �
     �
  !   �
  	   �
  
                   ,     C     U  %   p  *   �     �     �     �     
          '     3  	   @  #   J  ?   n     �     �     �  /   �            	   $     .     >  
   C     N     e     v     �  !   �     �     �  $   �  -        E      T     u     }     �  
   �  $   �  	   �  =   �  /   .     ^     o     �     �     �     �     �     �            .   .     ]  *   e  >   �  ,   �  5   �     2     ;     J     ^      f     �     �     �     �      �  	   �  &   �  
     .   )  
   X     c  3   o     �     �  "   �     �     �     
        !   7  0   Y     �     �     �  !   �  .   �       �  -  /        G     P     Y  @   e     �     �  7   �     �  
               1   7  >   i     �  &   �     �     �     �     �          2     I  &   f  /   �     �  !   �     �       	   )     3     9  	   E  '   O  E   w  
   �     �     �  6   �  	   #     -  	   >     H     W  	   ^     h     �     �     �  '   �     �       #     8   =     v  "   �  	   �     �     �     �  3   �  	   /  <   9  2   v     �     �     �     �      �  "        ?     M     [     i  5   �     �  5   �  =   �  %   2  G   X     �     �     �  	   �  1   �  	        (     D     R  !   g     �  -   �  
   �  -   �  
   �        3      
   K      V   #   d      �   %   �      �      �   %   �   =   !     W!     p!     �!  (   �!  /   �!     �!         K             >                  ^   G   f   .   &   \   
   V   _   3       k                  g   (           4   *           =   :       7   n   l   `   F   6       T   #       	   m      b           h   ]              R       <   A   p   e         Y   B   c       J              '   O       U   %   D         $   8         Z      q   I         X      W   -          /                 2         ,   o   N   @             C   9   0   M   H       P              a   d          S       !           1   "   L   5   E      )          [       Q   j   +       ;   ?   i                  Usage     Device name
     0 mW   Core   Package  <ESC> Exit | <Enter> Toggle tunable | <r> Window refresh  CPU %i %7sW %s device %s has no runtime power management .... device %s 
 Actual Audio codec %s: %s Audio codec %s: %s (%s) Autosuspend for USB device %s [%s] Autosuspend for unknown USB device %s (%s:%s) Bad Bluetooth device interface status C0 active C0 polling CPU use Calibrating USB devices
 Calibrating backlight
 Calibrating idle
 Calibrating radio devices
 Calibrating: CPU usage on %i threads
 Calibrating: CPU wakeup power consumption
 Calibrating: disk usage 
 Cannot create temp file
 Cannot load from file Cannot save to file Category Description Device stats Disk IO/s Enable Audio codec power management Estimated power: %5.1f    Measured power: %5.1f    Sum: %5.1f

 Events/s Exit Failed to mount debugfs!
 Finishing PowerTOP power estimate calibration 
 Frequency stats GFX Wakeups/s GPU ops/s GPU ops/seconds Good Idle stats Intel built in USB hub Leaving PowerTOP Loaded %i prior measurements
 Measuring workload %s.
 NMI watchdog should be turned off Network interface: %s (%s) Overview Overview of Software Power Consumers PCI Device %s has no runtime power management PCI Device: %s PS/2 Touchpad / Keyboard / Mouse Package Parameters after calibration:
 Power Aware CPU scheduler Power est. Power est.    Usage     Device name
 PowerTOP  PowerTOP %s needs the kernel to support the 'perf' subsystem
 PowerTOP is out of memory. PowerTOP is Aborting PowerTOP version Preparing to take measurements
 Quite mode failed!
 Radio device: %s Runtime PM for %s device %s Runtime PM for PCI Device %s SATA controller SATA disk: %s SATA link: %s Set refresh time out Starting PowerTOP power estimate calibration 
 Summary System baseline power is estimated at %sW
 Taking %d measurement(s) for a duration of %d second(s) each.
 The battery reports a discharge rate of %sW
 The estimated remaining time is %i hours, %i minutes
 Tunables USB device: %s USB device: %s (%s) Unknown Unknown issue running workload!
 Usage Usage: powertop [OPTIONS] VFS ops/sec and VM writeback timeout Wake-on-lan status for device %s Wakeups/s Wireless Power Saving for interface %s [=devnode] [=iterations] number of times to run each test [=seconds] [=workload] as well as support for trace points in the kernel:
 cpu package cpu package %i cpu_idle event returned no state?
 exiting...
 file to execute for workload generate a csv report generate a html report generate a report for 'x' seconds power or cpu_frequency event returned no state?
 print this help menu print version information run in "debug" mode runs powertop in calibration mode uses an Extech Power Analyzer for measurements wakeups/second Project-Id-Version: PowerTOP
Report-Msgid-Bugs-To: "powertop@lists.01.org"
POT-Creation-Date: 2014-11-21 10:37-0800
PO-Revision-Date: 2013-11-05 08:40+0000
Last-Translator: Margie Foster <margie@linux.intel.com>
Language-Team: Czech (Czech Republic) (http://www.transifex.com/projects/p/PowerTOP/language/cs_CZ/)
Language: cs_CZ
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
               Použití     Jméno zařízení
     0 mW   Jádro   Balíček  <ESC> Konec | <Enter> Přepnout změnitelné | <r> Obnovit okno  CPU %i %7sW %s zařízení %s nemá spustitelnou správu napájení .... zařízení %s 
 Aktuální Audio kodek %s: %s Audio kodek %s: %s (%s) Automatické uspání pro USB zařízení %s [%s] Automatické uspání pro neznámé USB zařízení %s (%s:%s) Špatné Status rozhraní zařízení Bluetooth C0 aktivní C0 sdílení využití CPU Kalibrace USB zařízení
 Kalibrace podsvícení
 Kalibrace nečinnosti
 Kalibrace radio zařízení
 Kalibrace: CPU použito na %i vláken
 Kalibrace: spotřeba energie na probuzení CPU
 Kalibrace: užití disku 
 Nelze vytvořit dočasný soubor
 Nelze načíst ze souboru Nelze uložit soubor Kategorie Popis Zařízení Disk IO/s Zapnout správu napájení audio kodeku Odhad energie: %5.1f    Změřená energie: %5.1f    Součet: %5.1f

 Událost/i Konec Selhalo připojení debugfs!
 Ukončování kalibrace odhadování energie PowerTOP
 Frekvence GFX Probuzení/s GPU ops/s GPU ops/sekund Dobré Nečinné Intel vestavěný v USB hubu Ukončuji PowerTOP Nahráno %i měření

 Měření zatížení %s.
 Hlídací pes NMI by měl být vypnutý Síťové rozhraní: %s (%s) Přehled Přehled Software Power uživatelů PCI Zařízení %s nemá spustitelnou správu napájení PCI Zařízení: %s PS/2 Touchpad / Klávesnice / Myš Balíček Parametry po kalibraci:
 Power Aware CPU plánovač Energie zbývá Energie zbývá   Použití     Jméno zařízení
 PowerTOP  PowerTOP %s potřebuje jádro k podpoře 'perf' subsystému
 PowerTOPu došla paměť. Power TOP je přerušen. PowerTOP verze Příprava k měření
 Selhal tichý mód!
 Radio zařízení: %s Runtime PM pro %s zařízení %s Trvání PM pro PCI zařízení %s SATA ovladač SATA disk: %s SATA link: %s Nastavit čas obnovení Začínání kalibrace odhadování energie PowerTOP
 Souhrn Systém základního napájení je odhadován na %sW
 Změřeno %d výsledků za dobu trvání %d vteřin každý.
 Spotřeba při chodu na baterii: %sW
 Odhadovaný zbývající čas do vybití baterie je %i hodin, %i minut
 Možnosti vyladění USB zařízení: %s USB zařízení: %s (%s) Neznámé Neznámý problém při spuštění zatížení!
 Využití Použití: powertop [VOLBY] VFS ops/sec a VM writeback timeout Wake-on-lan status zařízení %s Probuzení/s Úspora energie bezdrátových zařízení %s [=devnode] [=iterace] počet spuštění každého testu [=sekundy] [=zatížení] stejně jako podpora pro sledované body v jádru:
 cpu svazek cpu svazek %i cpu_idle událost nevrátila stav?
 ukončuji...
 soubor ke spuštění pro zatížení vytvoření csv hlášení vytvoření html hlášení vytvoření hlášení pro "x" sekund power nebo cpu_frequecny událost nevrátila žádný stav?

 vypíše tuto nápovědu vypíše informace o verzi spustit v "debug" módu spustit powertop v kalibračním režimu využití Extech Power Analyzéru pro měření probuzení/sekund 