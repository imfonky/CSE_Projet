export TSLIB_TSEVENTTYPE=INPUT
export TSLIB_TSDEVICE=/dev/input/by-path/platform-48060000.i2c-event
export TSLIB_CALIBFILE=/etc/pointercal
export TSLIB_CONFFILE=/etc/ts.conf
export TSLIB_PLUGINDIR=/usr/lib/ts
export TSLIB_FBDEVICE=/dev/fb0
export TSLIB_CONSOLEDEVICE=none
export QWS_MOUSE_PROTO=tslib:/dev/input/by-path/platform-48060000.i2c-event

