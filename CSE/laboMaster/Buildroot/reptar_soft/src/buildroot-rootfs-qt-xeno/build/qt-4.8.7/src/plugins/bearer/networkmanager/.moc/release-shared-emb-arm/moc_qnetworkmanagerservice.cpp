/****************************************************************************
** Meta object code from reading C++ file 'qnetworkmanagerservice.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qnetworkmanagerservice.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qnetworkmanagerservice.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QNetworkManagerInterface[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x05,
      55,   25,   25,   25, 0x05,
      88,   86,   25,   25, 0x05,
     138,   86,   25,   25, 0x05,
     168,   25,   25,   25, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QNetworkManagerInterface[] = {
    "QNetworkManagerInterface\0\0"
    "deviceAdded(QDBusObjectPath)\0"
    "deviceRemoved(QDBusObjectPath)\0,\0"
    "propertiesChanged(QString,QMap<QString,QVariant>)\0"
    "stateChanged(QString,quint32)\0"
    "activationFinished(QDBusPendingCallWatcher*)\0"
};

void QNetworkManagerInterface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QNetworkManagerInterface *_t = static_cast<QNetworkManagerInterface *>(_o);
        switch (_id) {
        case 0: _t->deviceAdded((*reinterpret_cast< QDBusObjectPath(*)>(_a[1]))); break;
        case 1: _t->deviceRemoved((*reinterpret_cast< QDBusObjectPath(*)>(_a[1]))); break;
        case 2: _t->propertiesChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QMap<QString,QVariant>(*)>(_a[2]))); break;
        case 3: _t->stateChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint32(*)>(_a[2]))); break;
        case 4: _t->activationFinished((*reinterpret_cast< QDBusPendingCallWatcher*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QNetworkManagerInterface::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QNetworkManagerInterface::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QNetworkManagerInterface,
      qt_meta_data_QNetworkManagerInterface, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QNetworkManagerInterface::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QNetworkManagerInterface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QNetworkManagerInterface::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QNetworkManagerInterface))
        return static_cast<void*>(const_cast< QNetworkManagerInterface*>(this));
    return QObject::qt_metacast(_clname);
}

int QNetworkManagerInterface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void QNetworkManagerInterface::deviceAdded(QDBusObjectPath _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QNetworkManagerInterface::deviceRemoved(QDBusObjectPath _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QNetworkManagerInterface::propertiesChanged(const QString & _t1, QMap<QString,QVariant> _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QNetworkManagerInterface::stateChanged(const QString & _t1, quint32 _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QNetworkManagerInterface::activationFinished(QDBusPendingCallWatcher * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
static const uint qt_meta_data_QNetworkManagerInterfaceAccessPoint[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      37,   36,   36,   36, 0x05,
      81,   79,   36,   36, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QNetworkManagerInterfaceAccessPoint[] = {
    "QNetworkManagerInterfaceAccessPoint\0"
    "\0propertiesChanged(QMap<QString,QVariant>)\0"
    ",\0propertiesChanged(QString,QMap<QString,QVariant>)\0"
};

void QNetworkManagerInterfaceAccessPoint::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QNetworkManagerInterfaceAccessPoint *_t = static_cast<QNetworkManagerInterfaceAccessPoint *>(_o);
        switch (_id) {
        case 0: _t->propertiesChanged((*reinterpret_cast< QMap<QString,QVariant>(*)>(_a[1]))); break;
        case 1: _t->propertiesChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QMap<QString,QVariant>(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QNetworkManagerInterfaceAccessPoint::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QNetworkManagerInterfaceAccessPoint::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QNetworkManagerInterfaceAccessPoint,
      qt_meta_data_QNetworkManagerInterfaceAccessPoint, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QNetworkManagerInterfaceAccessPoint::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QNetworkManagerInterfaceAccessPoint::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QNetworkManagerInterfaceAccessPoint::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QNetworkManagerInterfaceAccessPoint))
        return static_cast<void*>(const_cast< QNetworkManagerInterfaceAccessPoint*>(this));
    return QObject::qt_metacast(_clname);
}

int QNetworkManagerInterfaceAccessPoint::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void QNetworkManagerInterfaceAccessPoint::propertiesChanged(QMap<QString,QVariant> _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QNetworkManagerInterfaceAccessPoint::propertiesChanged(const QString & _t1, QMap<QString,QVariant> _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
static const uint qt_meta_data_QNetworkManagerInterfaceDevice[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      34,   32,   31,   31, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QNetworkManagerInterfaceDevice[] = {
    "QNetworkManagerInterfaceDevice\0\0,\0"
    "stateChanged(QString,quint32)\0"
};

void QNetworkManagerInterfaceDevice::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QNetworkManagerInterfaceDevice *_t = static_cast<QNetworkManagerInterfaceDevice *>(_o);
        switch (_id) {
        case 0: _t->stateChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint32(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QNetworkManagerInterfaceDevice::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QNetworkManagerInterfaceDevice::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QNetworkManagerInterfaceDevice,
      qt_meta_data_QNetworkManagerInterfaceDevice, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QNetworkManagerInterfaceDevice::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QNetworkManagerInterfaceDevice::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QNetworkManagerInterfaceDevice::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QNetworkManagerInterfaceDevice))
        return static_cast<void*>(const_cast< QNetworkManagerInterfaceDevice*>(this));
    return QObject::qt_metacast(_clname);
}

int QNetworkManagerInterfaceDevice::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QNetworkManagerInterfaceDevice::stateChanged(const QString & _t1, quint32 _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_QNetworkManagerInterfaceDeviceWired[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      39,   37,   36,   36, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QNetworkManagerInterfaceDeviceWired[] = {
    "QNetworkManagerInterfaceDeviceWired\0"
    "\0,\0propertiesChanged(QString,QMap<QString,QVariant>)\0"
};

void QNetworkManagerInterfaceDeviceWired::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QNetworkManagerInterfaceDeviceWired *_t = static_cast<QNetworkManagerInterfaceDeviceWired *>(_o);
        switch (_id) {
        case 0: _t->propertiesChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QMap<QString,QVariant>(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QNetworkManagerInterfaceDeviceWired::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QNetworkManagerInterfaceDeviceWired::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QNetworkManagerInterfaceDeviceWired,
      qt_meta_data_QNetworkManagerInterfaceDeviceWired, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QNetworkManagerInterfaceDeviceWired::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QNetworkManagerInterfaceDeviceWired::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QNetworkManagerInterfaceDeviceWired::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QNetworkManagerInterfaceDeviceWired))
        return static_cast<void*>(const_cast< QNetworkManagerInterfaceDeviceWired*>(this));
    return QObject::qt_metacast(_clname);
}

int QNetworkManagerInterfaceDeviceWired::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QNetworkManagerInterfaceDeviceWired::propertiesChanged(const QString & _t1, QMap<QString,QVariant> _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_QNetworkManagerInterfaceDeviceWireless[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      42,   40,   39,   39, 0x05,
      92,   40,   39,   39, 0x05,
     134,   40,   39,   39, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QNetworkManagerInterfaceDeviceWireless[] = {
    "QNetworkManagerInterfaceDeviceWireless\0"
    "\0,\0propertiesChanged(QString,QMap<QString,QVariant>)\0"
    "accessPointAdded(QString,QDBusObjectPath)\0"
    "accessPointRemoved(QString,QDBusObjectPath)\0"
};

void QNetworkManagerInterfaceDeviceWireless::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QNetworkManagerInterfaceDeviceWireless *_t = static_cast<QNetworkManagerInterfaceDeviceWireless *>(_o);
        switch (_id) {
        case 0: _t->propertiesChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QMap<QString,QVariant>(*)>(_a[2]))); break;
        case 1: _t->accessPointAdded((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QDBusObjectPath(*)>(_a[2]))); break;
        case 2: _t->accessPointRemoved((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QDBusObjectPath(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QNetworkManagerInterfaceDeviceWireless::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QNetworkManagerInterfaceDeviceWireless::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QNetworkManagerInterfaceDeviceWireless,
      qt_meta_data_QNetworkManagerInterfaceDeviceWireless, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QNetworkManagerInterfaceDeviceWireless::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QNetworkManagerInterfaceDeviceWireless::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QNetworkManagerInterfaceDeviceWireless::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QNetworkManagerInterfaceDeviceWireless))
        return static_cast<void*>(const_cast< QNetworkManagerInterfaceDeviceWireless*>(this));
    return QObject::qt_metacast(_clname);
}

int QNetworkManagerInterfaceDeviceWireless::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void QNetworkManagerInterfaceDeviceWireless::propertiesChanged(const QString & _t1, QMap<QString,QVariant> _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QNetworkManagerInterfaceDeviceWireless::accessPointAdded(const QString & _t1, QDBusObjectPath _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QNetworkManagerInterfaceDeviceWireless::accessPointRemoved(const QString & _t1, QDBusObjectPath _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
static const uint qt_meta_data_QNetworkManagerSettings[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      25,   24,   24,   24, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QNetworkManagerSettings[] = {
    "QNetworkManagerSettings\0\0"
    "newConnection(QDBusObjectPath)\0"
};

void QNetworkManagerSettings::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QNetworkManagerSettings *_t = static_cast<QNetworkManagerSettings *>(_o);
        switch (_id) {
        case 0: _t->newConnection((*reinterpret_cast< QDBusObjectPath(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QNetworkManagerSettings::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QNetworkManagerSettings::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QNetworkManagerSettings,
      qt_meta_data_QNetworkManagerSettings, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QNetworkManagerSettings::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QNetworkManagerSettings::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QNetworkManagerSettings::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QNetworkManagerSettings))
        return static_cast<void*>(const_cast< QNetworkManagerSettings*>(this));
    return QObject::qt_metacast(_clname);
}

int QNetworkManagerSettings::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QNetworkManagerSettings::newConnection(QDBusObjectPath _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_QNetworkManagerSettingsConnection[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      44,   35,   34,   34, 0x05,
      73,   68,   34,   34, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QNetworkManagerSettingsConnection[] = {
    "QNetworkManagerSettingsConnection\0\0"
    "settings\0updated(QNmSettingsMap)\0path\0"
    "removed(QString)\0"
};

void QNetworkManagerSettingsConnection::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QNetworkManagerSettingsConnection *_t = static_cast<QNetworkManagerSettingsConnection *>(_o);
        switch (_id) {
        case 0: _t->updated((*reinterpret_cast< const QNmSettingsMap(*)>(_a[1]))); break;
        case 1: _t->removed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QNetworkManagerSettingsConnection::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QNetworkManagerSettingsConnection::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QNetworkManagerSettingsConnection,
      qt_meta_data_QNetworkManagerSettingsConnection, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QNetworkManagerSettingsConnection::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QNetworkManagerSettingsConnection::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QNetworkManagerSettingsConnection::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QNetworkManagerSettingsConnection))
        return static_cast<void*>(const_cast< QNetworkManagerSettingsConnection*>(this));
    return QObject::qt_metacast(_clname);
}

int QNetworkManagerSettingsConnection::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void QNetworkManagerSettingsConnection::updated(const QNmSettingsMap & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QNetworkManagerSettingsConnection::removed(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
static const uint qt_meta_data_QNetworkManagerConnectionActive[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      33,   32,   32,   32, 0x05,
      77,   75,   32,   32, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_QNetworkManagerConnectionActive[] = {
    "QNetworkManagerConnectionActive\0\0"
    "propertiesChanged(QList<QDBusObjectPath>)\0"
    ",\0propertiesChanged(QString,QMap<QString,QVariant>)\0"
};

void QNetworkManagerConnectionActive::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QNetworkManagerConnectionActive *_t = static_cast<QNetworkManagerConnectionActive *>(_o);
        switch (_id) {
        case 0: _t->propertiesChanged((*reinterpret_cast< QList<QDBusObjectPath>(*)>(_a[1]))); break;
        case 1: _t->propertiesChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QMap<QString,QVariant>(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QNetworkManagerConnectionActive::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QNetworkManagerConnectionActive::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QNetworkManagerConnectionActive,
      qt_meta_data_QNetworkManagerConnectionActive, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QNetworkManagerConnectionActive::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QNetworkManagerConnectionActive::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QNetworkManagerConnectionActive::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QNetworkManagerConnectionActive))
        return static_cast<void*>(const_cast< QNetworkManagerConnectionActive*>(this));
    return QObject::qt_metacast(_clname);
}

int QNetworkManagerConnectionActive::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void QNetworkManagerConnectionActive::propertiesChanged(QList<QDBusObjectPath> _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QNetworkManagerConnectionActive::propertiesChanged(const QString & _t1, QMap<QString,QVariant> _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
static const uint qt_meta_data_QNetworkManagerIp4Config[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QNetworkManagerIp4Config[] = {
    "QNetworkManagerIp4Config\0"
};

void QNetworkManagerIp4Config::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QNetworkManagerIp4Config::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QNetworkManagerIp4Config::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QNetworkManagerIp4Config,
      qt_meta_data_QNetworkManagerIp4Config, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QNetworkManagerIp4Config::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QNetworkManagerIp4Config::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QNetworkManagerIp4Config::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QNetworkManagerIp4Config))
        return static_cast<void*>(const_cast< QNetworkManagerIp4Config*>(this));
    return QObject::qt_metacast(_clname);
}

int QNetworkManagerIp4Config::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
