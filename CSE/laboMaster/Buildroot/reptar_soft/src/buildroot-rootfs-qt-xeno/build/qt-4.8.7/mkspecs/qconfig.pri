#configuration
CONFIG +=  cross_compile shared def_files_disabled no_mocdepend release qt_no_framework
QT_ARCH = arm
QT_EDITION = OpenSource
QT_CONFIG +=  minimal-config small-config medium-config large-config full-config shared embedded reduce_exports ipv6 clock-gettime clock-monotonic mremap getaddrinfo ipv6ifname getifaddrs inotify system-jpeg no-mng system-png png no-tiff no-freetype zlib glib dbus alsa concurrent multimedia svg script release

#versioning
QT_VERSION = 4.8.7
QT_MAJOR_VERSION = 4
QT_MINOR_VERSION = 8
QT_PATCH_VERSION = 7

#namespaces
QT_LIBINFIX = 
QT_NAMESPACE = 
QT_NAMESPACE_MAC_CRC = 

QT_GCC_MAJOR_VERSION = 5
QT_GCC_MINOR_VERSION = 1
QT_GCC_PATCH_VERSION = 1
