/****************************************************************************
** Meta object code from reading C++ file 'qdbusconnection.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qdbusconnection.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdbusconnection.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QDBusConnection[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       3,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, flags, count, data
      16, 0x0,    3,   26,
      24, 0x1,   18,   32,
      40, 0x0,    2,   68,

 // enum data: key, value
      55, uint(QDBusConnection::SessionBus),
      66, uint(QDBusConnection::SystemBus),
      76, uint(QDBusConnection::ActivationBus),
      90, uint(QDBusConnection::ExportAdaptors),
     105, uint(QDBusConnection::ExportScriptableSlots),
     127, uint(QDBusConnection::ExportScriptableSignals),
     151, uint(QDBusConnection::ExportScriptableProperties),
     178, uint(QDBusConnection::ExportScriptableInvokables),
     205, uint(QDBusConnection::ExportScriptableContents),
     230, uint(QDBusConnection::ExportNonScriptableSlots),
     255, uint(QDBusConnection::ExportNonScriptableSignals),
     282, uint(QDBusConnection::ExportNonScriptableProperties),
     312, uint(QDBusConnection::ExportNonScriptableInvokables),
     342, uint(QDBusConnection::ExportNonScriptableContents),
     370, uint(QDBusConnection::ExportAllSlots),
     385, uint(QDBusConnection::ExportAllSignals),
     402, uint(QDBusConnection::ExportAllProperties),
     422, uint(QDBusConnection::ExportAllInvokables),
     442, uint(QDBusConnection::ExportAllContents),
     460, uint(QDBusConnection::ExportAllSignal),
     476, uint(QDBusConnection::ExportChildObjects),
     495, uint(QDBusConnection::UnregisterNode),
     510, uint(QDBusConnection::UnregisterTree),

       0        // eod
};

static const char qt_meta_stringdata_QDBusConnection[] = {
    "QDBusConnection\0BusType\0RegisterOptions\0"
    "UnregisterMode\0SessionBus\0SystemBus\0"
    "ActivationBus\0ExportAdaptors\0"
    "ExportScriptableSlots\0ExportScriptableSignals\0"
    "ExportScriptableProperties\0"
    "ExportScriptableInvokables\0"
    "ExportScriptableContents\0"
    "ExportNonScriptableSlots\0"
    "ExportNonScriptableSignals\0"
    "ExportNonScriptableProperties\0"
    "ExportNonScriptableInvokables\0"
    "ExportNonScriptableContents\0ExportAllSlots\0"
    "ExportAllSignals\0ExportAllProperties\0"
    "ExportAllInvokables\0ExportAllContents\0"
    "ExportAllSignal\0ExportChildObjects\0"
    "UnregisterNode\0UnregisterTree\0"
};

const QMetaObject QDBusConnection::staticMetaObject = {
    { 0, qt_meta_stringdata_QDBusConnection,
      qt_meta_data_QDBusConnection, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QDBusConnection::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION
QT_END_MOC_NAMESPACE
