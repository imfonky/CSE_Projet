/****************************************************************************
** Meta object code from reading C++ file 'qconnmanengine.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qconnmanengine.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qconnmanengine.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QConnmanEngine[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      37,   34,   15,   15, 0x08,
      97,   34,   15,   15, 0x08,
     150,   34,   15,   15, 0x08,

 // methods: signature, parameters, type, tag, flags
     213,   15,   15,   15, 0x02,
     226,   15,   15,   15, 0x02,

       0        // eod
};

static const char qt_meta_stringdata_QConnmanEngine[] = {
    "QConnmanEngine\0\0doRequestUpdate()\0,,\0"
    "servicePropertyChangedContext(QString,QString,QDBusVariant)\0"
    "propertyChangedContext(QString,QString,QDBusVariant)\0"
    "technologyPropertyChangedContext(QString,QString,QDBusVariant)\0"
    "initialize()\0requestUpdate()\0"
};

void QConnmanEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QConnmanEngine *_t = static_cast<QConnmanEngine *>(_o);
        switch (_id) {
        case 0: _t->doRequestUpdate(); break;
        case 1: _t->servicePropertyChangedContext((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QDBusVariant(*)>(_a[3]))); break;
        case 2: _t->propertyChangedContext((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QDBusVariant(*)>(_a[3]))); break;
        case 3: _t->technologyPropertyChangedContext((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QDBusVariant(*)>(_a[3]))); break;
        case 4: _t->initialize(); break;
        case 5: _t->requestUpdate(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QConnmanEngine::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QConnmanEngine::staticMetaObject = {
    { &QBearerEngineImpl::staticMetaObject, qt_meta_stringdata_QConnmanEngine,
      qt_meta_data_QConnmanEngine, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QConnmanEngine::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QConnmanEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QConnmanEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QConnmanEngine))
        return static_cast<void*>(const_cast< QConnmanEngine*>(this));
    return QBearerEngineImpl::qt_metacast(_clname);
}

int QConnmanEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBearerEngineImpl::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
