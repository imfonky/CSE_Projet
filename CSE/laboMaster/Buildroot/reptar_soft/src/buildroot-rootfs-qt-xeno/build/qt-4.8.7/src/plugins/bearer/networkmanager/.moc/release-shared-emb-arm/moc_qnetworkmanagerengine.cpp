/****************************************************************************
** Meta object code from reading C++ file 'qnetworkmanagerengine.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qnetworkmanagerengine.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qnetworkmanagerengine.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QNetworkManagerEngine[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      39,   23,   22,   22, 0x08,
      98,   23,   22,   22, 0x08,
     164,   23,   22,   22, 0x08,
     225,  220,   22,   22, 0x08,
     254,  220,   22,   22, 0x08,
     299,  285,   22,   22, 0x08,
     355,  220,   22,   22, 0x28,
     386,  220,   22,   22, 0x08,
     421,  412,   22,   22, 0x08,
     462,  454,   22,   22, 0x08,
     523,  507,   22,   22, 0x08,
     563,  507,   22,   22, 0x08,
     610,  606,   22,   22, 0x08,

 // methods: signature, parameters, type, tag, flags
     652,   22,   22,   22, 0x02,
     665,   22,   22,   22, 0x02,

       0        // eod
};

static const char qt_meta_stringdata_QNetworkManagerEngine[] = {
    "QNetworkManagerEngine\0\0path,properties\0"
    "interfacePropertiesChanged(QString,QMap<QString,QVariant>)\0"
    "activeConnectionPropertiesChanged(QString,QMap<QString,QVariant>)\0"
    "devicePropertiesChanged(QString,QMap<QString,QVariant>)\0"
    "path\0deviceAdded(QDBusObjectPath)\0"
    "deviceRemoved(QDBusObjectPath)\0"
    "path,settings\0"
    "newConnection(QDBusObjectPath,QNetworkManagerSettings*)\0"
    "newConnection(QDBusObjectPath)\0"
    "removeConnection(QString)\0settings\0"
    "updateConnection(QNmSettingsMap)\0"
    "watcher\0activationFinished(QDBusPendingCallWatcher*)\0"
    "path,objectPath\0newAccessPoint(QString,QDBusObjectPath)\0"
    "removeAccessPoint(QString,QDBusObjectPath)\0"
    "map\0updateAccessPoint(QMap<QString,QVariant>)\0"
    "initialize()\0requestUpdate()\0"
};

void QNetworkManagerEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QNetworkManagerEngine *_t = static_cast<QNetworkManagerEngine *>(_o);
        switch (_id) {
        case 0: _t->interfacePropertiesChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QMap<QString,QVariant>(*)>(_a[2]))); break;
        case 1: _t->activeConnectionPropertiesChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QMap<QString,QVariant>(*)>(_a[2]))); break;
        case 2: _t->devicePropertiesChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QMap<QString,QVariant>(*)>(_a[2]))); break;
        case 3: _t->deviceAdded((*reinterpret_cast< const QDBusObjectPath(*)>(_a[1]))); break;
        case 4: _t->deviceRemoved((*reinterpret_cast< const QDBusObjectPath(*)>(_a[1]))); break;
        case 5: _t->newConnection((*reinterpret_cast< const QDBusObjectPath(*)>(_a[1])),(*reinterpret_cast< QNetworkManagerSettings*(*)>(_a[2]))); break;
        case 6: _t->newConnection((*reinterpret_cast< const QDBusObjectPath(*)>(_a[1]))); break;
        case 7: _t->removeConnection((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->updateConnection((*reinterpret_cast< const QNmSettingsMap(*)>(_a[1]))); break;
        case 9: _t->activationFinished((*reinterpret_cast< QDBusPendingCallWatcher*(*)>(_a[1]))); break;
        case 10: _t->newAccessPoint((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QDBusObjectPath(*)>(_a[2]))); break;
        case 11: _t->removeAccessPoint((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QDBusObjectPath(*)>(_a[2]))); break;
        case 12: _t->updateAccessPoint((*reinterpret_cast< const QMap<QString,QVariant>(*)>(_a[1]))); break;
        case 13: _t->initialize(); break;
        case 14: _t->requestUpdate(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QNetworkManagerEngine::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QNetworkManagerEngine::staticMetaObject = {
    { &QBearerEngineImpl::staticMetaObject, qt_meta_stringdata_QNetworkManagerEngine,
      qt_meta_data_QNetworkManagerEngine, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QNetworkManagerEngine::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QNetworkManagerEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QNetworkManagerEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QNetworkManagerEngine))
        return static_cast<void*>(const_cast< QNetworkManagerEngine*>(this));
    return QBearerEngineImpl::qt_metacast(_clname);
}

int QNetworkManagerEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBearerEngineImpl::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
