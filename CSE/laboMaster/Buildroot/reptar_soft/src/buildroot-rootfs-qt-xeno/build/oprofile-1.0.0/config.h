/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* whether popt prototype takes a const char ** */
#define CONST_POPT 1

/* Define to 1 if you have the declaration of `basename', and to 0 if you
   don't. */
#define HAVE_DECL_BASENAME 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the `iberty' library (-liberty). */
#define HAVE_LIBIBERTY 1

/* Define to 1 if you have the <libiberty.h> header file. */
/* #undef HAVE_LIBIBERTY_H */

/* Define to 1 if libpfm is available */
/* #undef HAVE_LIBPFM */

/* Define to 1 if using libpfm3; 0 if using newer libpfm */
/* #undef HAVE_LIBPFM3 */

/* Define to 1 if you have the `popt' library (-lpopt). */
#define HAVE_LIBPOPT 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the `perfmonctl' function. */
/* #undef HAVE_PERFMONCTL */

/* Kernel support for perf_events exists */
#define HAVE_PERF_EVENTS 1

/* PERF_RECORD_MISC_GUEST_KERNEL is defined in perf_event.h */
#define HAVE_PERF_GUEST_MACROS 1

/* precise_ip is defined in perf_event.h */
#define HAVE_PERF_PRECISE_IP 1

/* Define to 1 if you have the `sched_setaffinity' function. */
#define HAVE_SCHED_SETAFFINITY 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if you have the `xcalloc' function. */
#define HAVE_XCALLOC 1

/* Define to 1 if you have the `xmemdup' function. */
#define HAVE_XMEMDUP 1

/* Define to the sub-directory in which libtool stores uninstalled libraries.
   */
#define LT_OBJDIR ".libs/"

/* whether malloc attribute is understood */
/* #undef MALLOC_ATTRIBUTE_OK */

/* package binary directory */
#define OP_BINDIR "/usr/bin/"

/* package data directory */
#define OP_DATADIR "/usr/share/oprofile/"

/* Name of package */
#define PACKAGE "oprofile"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT ""

/* Define to the full name of this package. */
#define PACKAGE_NAME "OProfile"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "OProfile 1.0.0"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "oprofile"

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.0.0"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Synthesize special symbols when needed */
#define SYNTHESIZE_SYMBOLS 0

/* whether bfd.h defines bool values */
/* #undef TRUE_FALSE_ALREADY_DEFINED */

/* Version number of package */
#define VERSION "1.0.0"

/* Define to 1 if the X Window System is missing or not being used. */
#define X_DISPLAY_MISSING 1
