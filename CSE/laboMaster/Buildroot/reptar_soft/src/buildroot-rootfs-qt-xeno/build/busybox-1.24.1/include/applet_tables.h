/* This is a generated file, don't edit */

#define NUM_APPLETS 239

const char applet_names[] ALIGN1 = ""
"[" "\0"
"[[" "\0"
"addgroup" "\0"
"adduser" "\0"
"ar" "\0"
"arp" "\0"
"arping" "\0"
"ash" "\0"
"awk" "\0"
"basename" "\0"
"blkid" "\0"
"bunzip2" "\0"
"bzcat" "\0"
"cat" "\0"
"catv" "\0"
"chattr" "\0"
"chgrp" "\0"
"chmod" "\0"
"chown" "\0"
"chroot" "\0"
"chrt" "\0"
"chvt" "\0"
"cksum" "\0"
"clear" "\0"
"cmp" "\0"
"cp" "\0"
"cpio" "\0"
"crond" "\0"
"crontab" "\0"
"cut" "\0"
"date" "\0"
"dc" "\0"
"dd" "\0"
"deallocvt" "\0"
"delgroup" "\0"
"deluser" "\0"
"devmem" "\0"
"df" "\0"
"diff" "\0"
"dirname" "\0"
"dmesg" "\0"
"dnsd" "\0"
"dnsdomainname" "\0"
"dos2unix" "\0"
"du" "\0"
"dumpkmap" "\0"
"echo" "\0"
"egrep" "\0"
"eject" "\0"
"env" "\0"
"ether-wake" "\0"
"expr" "\0"
"false" "\0"
"fbset" "\0"
"fdflush" "\0"
"fdformat" "\0"
"fdisk" "\0"
"fgrep" "\0"
"find" "\0"
"flock" "\0"
"fold" "\0"
"free" "\0"
"freeramdisk" "\0"
"fsck" "\0"
"fstrim" "\0"
"fuser" "\0"
"getopt" "\0"
"getty" "\0"
"grep" "\0"
"gunzip" "\0"
"gzip" "\0"
"halt" "\0"
"hdparm" "\0"
"head" "\0"
"hexdump" "\0"
"hostid" "\0"
"hostname" "\0"
"hwclock" "\0"
"i2cdetect" "\0"
"i2cdump" "\0"
"i2cget" "\0"
"i2cset" "\0"
"id" "\0"
"ifconfig" "\0"
"ifdown" "\0"
"ifup" "\0"
"inetd" "\0"
"init" "\0"
"insmod" "\0"
"install" "\0"
"ip" "\0"
"ipaddr" "\0"
"ipcrm" "\0"
"ipcs" "\0"
"iplink" "\0"
"iproute" "\0"
"iprule" "\0"
"iptunnel" "\0"
"kill" "\0"
"killall" "\0"
"killall5" "\0"
"klogd" "\0"
"last" "\0"
"less" "\0"
"linux32" "\0"
"linux64" "\0"
"linuxrc" "\0"
"ln" "\0"
"loadfont" "\0"
"loadkmap" "\0"
"logger" "\0"
"login" "\0"
"logname" "\0"
"losetup" "\0"
"ls" "\0"
"lsattr" "\0"
"lsmod" "\0"
"lsof" "\0"
"lspci" "\0"
"lsusb" "\0"
"lzcat" "\0"
"lzma" "\0"
"makedevs" "\0"
"md5sum" "\0"
"mdev" "\0"
"mesg" "\0"
"microcom" "\0"
"mkdir" "\0"
"mkfifo" "\0"
"mknod" "\0"
"mkswap" "\0"
"mktemp" "\0"
"modprobe" "\0"
"more" "\0"
"mount" "\0"
"mountpoint" "\0"
"mt" "\0"
"mv" "\0"
"nameif" "\0"
"netstat" "\0"
"nice" "\0"
"nohup" "\0"
"nslookup" "\0"
"od" "\0"
"openvt" "\0"
"passwd" "\0"
"patch" "\0"
"pidof" "\0"
"ping" "\0"
"pipe_progress" "\0"
"pivot_root" "\0"
"poweroff" "\0"
"printenv" "\0"
"printf" "\0"
"ps" "\0"
"pwd" "\0"
"rdate" "\0"
"readlink" "\0"
"readprofile" "\0"
"realpath" "\0"
"reboot" "\0"
"renice" "\0"
"reset" "\0"
"resize" "\0"
"rm" "\0"
"rmdir" "\0"
"rmmod" "\0"
"route" "\0"
"run-parts" "\0"
"runlevel" "\0"
"sed" "\0"
"seq" "\0"
"setarch" "\0"
"setconsole" "\0"
"setkeycodes" "\0"
"setlogcons" "\0"
"setserial" "\0"
"setsid" "\0"
"sh" "\0"
"sha1sum" "\0"
"sha256sum" "\0"
"sha3sum" "\0"
"sha512sum" "\0"
"sleep" "\0"
"sort" "\0"
"start-stop-daemon" "\0"
"strings" "\0"
"stty" "\0"
"su" "\0"
"sulogin" "\0"
"swapoff" "\0"
"swapon" "\0"
"switch_root" "\0"
"sync" "\0"
"sysctl" "\0"
"syslogd" "\0"
"tail" "\0"
"tar" "\0"
"tee" "\0"
"telnet" "\0"
"test" "\0"
"tftp" "\0"
"time" "\0"
"top" "\0"
"touch" "\0"
"tr" "\0"
"traceroute" "\0"
"true" "\0"
"truncate" "\0"
"tty" "\0"
"udhcpc" "\0"
"uevent" "\0"
"umount" "\0"
"uname" "\0"
"uniq" "\0"
"unix2dos" "\0"
"unlink" "\0"
"unlzma" "\0"
"unxz" "\0"
"unzip" "\0"
"uptime" "\0"
"usleep" "\0"
"uudecode" "\0"
"uuencode" "\0"
"vconfig" "\0"
"vi" "\0"
"vlock" "\0"
"watch" "\0"
"watchdog" "\0"
"wc" "\0"
"wget" "\0"
"which" "\0"
"who" "\0"
"whoami" "\0"
"xargs" "\0"
"xz" "\0"
"xzcat" "\0"
"yes" "\0"
"zcat" "\0"
;

#define APPLET_NO_addgroup 2
#define APPLET_NO_adduser 3
#define APPLET_NO_ar 4
#define APPLET_NO_arp 5
#define APPLET_NO_arping 6
#define APPLET_NO_ash 7
#define APPLET_NO_awk 8
#define APPLET_NO_basename 9
#define APPLET_NO_blkid 10
#define APPLET_NO_bunzip2 11
#define APPLET_NO_bzcat 12
#define APPLET_NO_cat 13
#define APPLET_NO_catv 14
#define APPLET_NO_chattr 15
#define APPLET_NO_chgrp 16
#define APPLET_NO_chmod 17
#define APPLET_NO_chown 18
#define APPLET_NO_chroot 19
#define APPLET_NO_chrt 20
#define APPLET_NO_chvt 21
#define APPLET_NO_cksum 22
#define APPLET_NO_clear 23
#define APPLET_NO_cmp 24
#define APPLET_NO_cp 25
#define APPLET_NO_cpio 26
#define APPLET_NO_crond 27
#define APPLET_NO_crontab 28
#define APPLET_NO_cut 29
#define APPLET_NO_date 30
#define APPLET_NO_dc 31
#define APPLET_NO_dd 32
#define APPLET_NO_deallocvt 33
#define APPLET_NO_delgroup 34
#define APPLET_NO_deluser 35
#define APPLET_NO_devmem 36
#define APPLET_NO_df 37
#define APPLET_NO_diff 38
#define APPLET_NO_dirname 39
#define APPLET_NO_dmesg 40
#define APPLET_NO_dnsd 41
#define APPLET_NO_dnsdomainname 42
#define APPLET_NO_dos2unix 43
#define APPLET_NO_du 44
#define APPLET_NO_dumpkmap 45
#define APPLET_NO_echo 46
#define APPLET_NO_egrep 47
#define APPLET_NO_eject 48
#define APPLET_NO_env 49
#define APPLET_NO_expr 51
#define APPLET_NO_false 52
#define APPLET_NO_fbset 53
#define APPLET_NO_fdflush 54
#define APPLET_NO_fdformat 55
#define APPLET_NO_fdisk 56
#define APPLET_NO_fgrep 57
#define APPLET_NO_find 58
#define APPLET_NO_flock 59
#define APPLET_NO_fold 60
#define APPLET_NO_free 61
#define APPLET_NO_freeramdisk 62
#define APPLET_NO_fsck 63
#define APPLET_NO_fstrim 64
#define APPLET_NO_fuser 65
#define APPLET_NO_getopt 66
#define APPLET_NO_getty 67
#define APPLET_NO_grep 68
#define APPLET_NO_gunzip 69
#define APPLET_NO_gzip 70
#define APPLET_NO_halt 71
#define APPLET_NO_hdparm 72
#define APPLET_NO_head 73
#define APPLET_NO_hexdump 74
#define APPLET_NO_hostid 75
#define APPLET_NO_hostname 76
#define APPLET_NO_hwclock 77
#define APPLET_NO_i2cdetect 78
#define APPLET_NO_i2cdump 79
#define APPLET_NO_i2cget 80
#define APPLET_NO_i2cset 81
#define APPLET_NO_id 82
#define APPLET_NO_ifconfig 83
#define APPLET_NO_ifdown 84
#define APPLET_NO_ifup 85
#define APPLET_NO_inetd 86
#define APPLET_NO_init 87
#define APPLET_NO_insmod 88
#define APPLET_NO_install 89
#define APPLET_NO_ip 90
#define APPLET_NO_ipaddr 91
#define APPLET_NO_ipcrm 92
#define APPLET_NO_ipcs 93
#define APPLET_NO_iplink 94
#define APPLET_NO_iproute 95
#define APPLET_NO_iprule 96
#define APPLET_NO_iptunnel 97
#define APPLET_NO_kill 98
#define APPLET_NO_killall 99
#define APPLET_NO_killall5 100
#define APPLET_NO_klogd 101
#define APPLET_NO_last 102
#define APPLET_NO_less 103
#define APPLET_NO_linux32 104
#define APPLET_NO_linux64 105
#define APPLET_NO_linuxrc 106
#define APPLET_NO_ln 107
#define APPLET_NO_loadfont 108
#define APPLET_NO_loadkmap 109
#define APPLET_NO_logger 110
#define APPLET_NO_login 111
#define APPLET_NO_logname 112
#define APPLET_NO_losetup 113
#define APPLET_NO_ls 114
#define APPLET_NO_lsattr 115
#define APPLET_NO_lsmod 116
#define APPLET_NO_lsof 117
#define APPLET_NO_lspci 118
#define APPLET_NO_lsusb 119
#define APPLET_NO_lzcat 120
#define APPLET_NO_lzma 121
#define APPLET_NO_makedevs 122
#define APPLET_NO_md5sum 123
#define APPLET_NO_mdev 124
#define APPLET_NO_mesg 125
#define APPLET_NO_microcom 126
#define APPLET_NO_mkdir 127
#define APPLET_NO_mkfifo 128
#define APPLET_NO_mknod 129
#define APPLET_NO_mkswap 130
#define APPLET_NO_mktemp 131
#define APPLET_NO_modprobe 132
#define APPLET_NO_more 133
#define APPLET_NO_mount 134
#define APPLET_NO_mountpoint 135
#define APPLET_NO_mt 136
#define APPLET_NO_mv 137
#define APPLET_NO_nameif 138
#define APPLET_NO_netstat 139
#define APPLET_NO_nice 140
#define APPLET_NO_nohup 141
#define APPLET_NO_nslookup 142
#define APPLET_NO_od 143
#define APPLET_NO_openvt 144
#define APPLET_NO_passwd 145
#define APPLET_NO_patch 146
#define APPLET_NO_pidof 147
#define APPLET_NO_ping 148
#define APPLET_NO_pipe_progress 149
#define APPLET_NO_pivot_root 150
#define APPLET_NO_poweroff 151
#define APPLET_NO_printenv 152
#define APPLET_NO_printf 153
#define APPLET_NO_ps 154
#define APPLET_NO_pwd 155
#define APPLET_NO_rdate 156
#define APPLET_NO_readlink 157
#define APPLET_NO_readprofile 158
#define APPLET_NO_realpath 159
#define APPLET_NO_reboot 160
#define APPLET_NO_renice 161
#define APPLET_NO_reset 162
#define APPLET_NO_resize 163
#define APPLET_NO_rm 164
#define APPLET_NO_rmdir 165
#define APPLET_NO_rmmod 166
#define APPLET_NO_route 167
#define APPLET_NO_runlevel 169
#define APPLET_NO_sed 170
#define APPLET_NO_seq 171
#define APPLET_NO_setarch 172
#define APPLET_NO_setconsole 173
#define APPLET_NO_setkeycodes 174
#define APPLET_NO_setlogcons 175
#define APPLET_NO_setserial 176
#define APPLET_NO_setsid 177
#define APPLET_NO_sh 178
#define APPLET_NO_sha1sum 179
#define APPLET_NO_sha256sum 180
#define APPLET_NO_sha3sum 181
#define APPLET_NO_sha512sum 182
#define APPLET_NO_sleep 183
#define APPLET_NO_sort 184
#define APPLET_NO_strings 186
#define APPLET_NO_stty 187
#define APPLET_NO_su 188
#define APPLET_NO_sulogin 189
#define APPLET_NO_swapoff 190
#define APPLET_NO_swapon 191
#define APPLET_NO_switch_root 192
#define APPLET_NO_sync 193
#define APPLET_NO_sysctl 194
#define APPLET_NO_syslogd 195
#define APPLET_NO_tail 196
#define APPLET_NO_tar 197
#define APPLET_NO_tee 198
#define APPLET_NO_telnet 199
#define APPLET_NO_test 200
#define APPLET_NO_tftp 201
#define APPLET_NO_time 202
#define APPLET_NO_top 203
#define APPLET_NO_touch 204
#define APPLET_NO_tr 205
#define APPLET_NO_traceroute 206
#define APPLET_NO_true 207
#define APPLET_NO_truncate 208
#define APPLET_NO_tty 209
#define APPLET_NO_udhcpc 210
#define APPLET_NO_uevent 211
#define APPLET_NO_umount 212
#define APPLET_NO_uname 213
#define APPLET_NO_uniq 214
#define APPLET_NO_unix2dos 215
#define APPLET_NO_unlink 216
#define APPLET_NO_unlzma 217
#define APPLET_NO_unxz 218
#define APPLET_NO_unzip 219
#define APPLET_NO_uptime 220
#define APPLET_NO_usleep 221
#define APPLET_NO_uudecode 222
#define APPLET_NO_uuencode 223
#define APPLET_NO_vconfig 224
#define APPLET_NO_vi 225
#define APPLET_NO_vlock 226
#define APPLET_NO_watch 227
#define APPLET_NO_watchdog 228
#define APPLET_NO_wc 229
#define APPLET_NO_wget 230
#define APPLET_NO_which 231
#define APPLET_NO_who 232
#define APPLET_NO_whoami 233
#define APPLET_NO_xargs 234
#define APPLET_NO_xz 235
#define APPLET_NO_xzcat 236
#define APPLET_NO_yes 237
#define APPLET_NO_zcat 238

#ifndef SKIP_applet_main
int (*const applet_main[])(int argc, char **argv) = {
test_main,
test_main,
addgroup_main,
adduser_main,
ar_main,
arp_main,
arping_main,
ash_main,
awk_main,
basename_main,
blkid_main,
bunzip2_main,
bunzip2_main,
cat_main,
catv_main,
chattr_main,
chgrp_main,
chmod_main,
chown_main,
chroot_main,
chrt_main,
chvt_main,
cksum_main,
clear_main,
cmp_main,
cp_main,
cpio_main,
crond_main,
crontab_main,
cut_main,
date_main,
dc_main,
dd_main,
deallocvt_main,
deluser_main,
deluser_main,
devmem_main,
df_main,
diff_main,
dirname_main,
dmesg_main,
dnsd_main,
hostname_main,
dos2unix_main,
du_main,
dumpkmap_main,
echo_main,
grep_main,
eject_main,
env_main,
ether_wake_main,
expr_main,
false_main,
fbset_main,
freeramdisk_main,
fdformat_main,
fdisk_main,
grep_main,
find_main,
flock_main,
fold_main,
free_main,
freeramdisk_main,
fsck_main,
fstrim_main,
fuser_main,
getopt_main,
getty_main,
grep_main,
gunzip_main,
gzip_main,
halt_main,
hdparm_main,
head_main,
hexdump_main,
hostid_main,
hostname_main,
hwclock_main,
i2cdetect_main,
i2cdump_main,
i2cget_main,
i2cset_main,
id_main,
ifconfig_main,
ifupdown_main,
ifupdown_main,
inetd_main,
init_main,
insmod_main,
install_main,
ip_main,
ipaddr_main,
ipcrm_main,
ipcs_main,
iplink_main,
iproute_main,
iprule_main,
iptunnel_main,
kill_main,
kill_main,
kill_main,
klogd_main,
last_main,
less_main,
setarch_main,
setarch_main,
init_main,
ln_main,
loadfont_main,
loadkmap_main,
logger_main,
login_main,
logname_main,
losetup_main,
ls_main,
lsattr_main,
lsmod_main,
lsof_main,
lspci_main,
lsusb_main,
unlzma_main,
unlzma_main,
makedevs_main,
md5_sha1_sum_main,
mdev_main,
mesg_main,
microcom_main,
mkdir_main,
mkfifo_main,
mknod_main,
mkswap_main,
mktemp_main,
modprobe_main,
more_main,
mount_main,
mountpoint_main,
mt_main,
mv_main,
nameif_main,
netstat_main,
nice_main,
nohup_main,
nslookup_main,
od_main,
openvt_main,
passwd_main,
patch_main,
pidof_main,
ping_main,
pipe_progress_main,
pivot_root_main,
halt_main,
printenv_main,
printf_main,
ps_main,
pwd_main,
rdate_main,
readlink_main,
readprofile_main,
realpath_main,
halt_main,
renice_main,
reset_main,
resize_main,
rm_main,
rmdir_main,
rmmod_main,
route_main,
run_parts_main,
runlevel_main,
sed_main,
seq_main,
setarch_main,
setconsole_main,
setkeycodes_main,
setlogcons_main,
setserial_main,
setsid_main,
ash_main,
md5_sha1_sum_main,
md5_sha1_sum_main,
md5_sha1_sum_main,
md5_sha1_sum_main,
sleep_main,
sort_main,
start_stop_daemon_main,
strings_main,
stty_main,
su_main,
sulogin_main,
swap_on_off_main,
swap_on_off_main,
switch_root_main,
sync_main,
sysctl_main,
syslogd_main,
tail_main,
tar_main,
tee_main,
telnet_main,
test_main,
tftp_main,
time_main,
top_main,
touch_main,
tr_main,
traceroute_main,
true_main,
truncate_main,
tty_main,
udhcpc_main,
uevent_main,
umount_main,
uname_main,
uniq_main,
dos2unix_main,
unlink_main,
unlzma_main,
unxz_main,
unzip_main,
uptime_main,
usleep_main,
uudecode_main,
uuencode_main,
vconfig_main,
vi_main,
vlock_main,
watch_main,
watchdog_main,
wc_main,
wget_main,
which_main,
who_main,
whoami_main,
xargs_main,
unxz_main,
unxz_main,
yes_main,
gunzip_main,
};
#endif

const uint16_t applet_nameofs[] ALIGN2 = {
0x0000,
0x0002,
0x0005,
0x000e,
0x0016,
0x0019,
0x001d,
0x0024,
0x0028,
0x002c,
0x0035,
0x003b,
0x0043,
0x0049,
0x004d,
0x0052,
0x0059,
0x005f,
0x0065,
0x006b,
0x0072,
0x0077,
0x007c,
0x0082,
0x0088,
0x008c,
0x008f,
0x0094,
0x809a,
0x00a2,
0x00a6,
0x00ab,
0x00ae,
0x00b1,
0x00bb,
0x00c4,
0x00cc,
0x00d3,
0x00d6,
0x00db,
0x00e3,
0x00e9,
0x00ee,
0x00fc,
0x0105,
0x0108,
0x0111,
0x0116,
0x011c,
0x0122,
0x0126,
0x0131,
0x0136,
0x013c,
0x0142,
0x014a,
0x0153,
0x0159,
0x015f,
0x0164,
0x016a,
0x016f,
0x0174,
0x0180,
0x0185,
0x018c,
0x0192,
0x0199,
0x019f,
0x01a4,
0x01ab,
0x01b0,
0x01b5,
0x01bc,
0x01c1,
0x01c9,
0x01d0,
0x01d9,
0x01e1,
0x01eb,
0x01f3,
0x01fa,
0x0201,
0x0204,
0x020d,
0x0214,
0x0219,
0x021f,
0x0224,
0x022b,
0x0233,
0x0236,
0x023d,
0x0243,
0x0248,
0x024f,
0x0257,
0x025e,
0x0267,
0x026c,
0x0274,
0x027d,
0x0283,
0x0288,
0x028d,
0x0295,
0x029d,
0x02a5,
0x02a8,
0x02b1,
0x02ba,
0x82c1,
0x02c7,
0x02cf,
0x02d7,
0x02da,
0x02e1,
0x02e7,
0x02ec,
0x02f2,
0x02f8,
0x02fe,
0x0303,
0x030c,
0x0313,
0x0318,
0x031d,
0x0326,
0x032c,
0x0333,
0x0339,
0x0340,
0x0347,
0x0350,
0x4355,
0x035b,
0x0366,
0x0369,
0x036c,
0x0373,
0x037b,
0x0380,
0x0386,
0x038f,
0x0392,
0x8399,
0x03a0,
0x03a6,
0x43ac,
0x03b1,
0x03bf,
0x03ca,
0x03d3,
0x03dc,
0x03e3,
0x03e6,
0x03ea,
0x03f0,
0x03f9,
0x0405,
0x040e,
0x0415,
0x041c,
0x0422,
0x0429,
0x042c,
0x0432,
0x0438,
0x043e,
0x0448,
0x0451,
0x0455,
0x0459,
0x0461,
0x046c,
0x0478,
0x0483,
0x048d,
0x0494,
0x0497,
0x049f,
0x04a9,
0x04b1,
0x04bb,
0x04c1,
0x04c6,
0x04d8,
0x04e0,
0x84e5,
0x04e8,
0x04f0,
0x04f8,
0x04ff,
0x050b,
0x0510,
0x0517,
0x051f,
0x0524,
0x0528,
0x052c,
0x0533,
0x0538,
0x053d,
0x0542,
0x0546,
0x054c,
0x454f,
0x055a,
0x055f,
0x0568,
0x056c,
0x0573,
0x057a,
0x0581,
0x0587,
0x058c,
0x0595,
0x059c,
0x05a3,
0x05a8,
0x05ae,
0x05b5,
0x05bc,
0x05c5,
0x05ce,
0x05d6,
0x85d9,
0x05df,
0x05e5,
0x05ee,
0x05f1,
0x05f6,
0x05fc,
0x0600,
0x0607,
0x060d,
0x0610,
0x0616,
0x061a,
};

const uint8_t applet_install_loc[] ALIGN1 = {
0x33,
0x44,
0x23,
0x14,
0x33,
0x32,
0x13,
0x11,
0x11,
0x41,
0x33,
0x33,
0x13,
0x41,
0x33,
0x31,
0x31,
0x44,
0x12,
0x33,
0x41,
0x31,
0x13,
0x11,
0x33,
0x34,
0x41,
0x41,
0x12,
0x33,
0x33,
0x22,
0x32,
0x21,
0x11,
0x21,
0x32,
0x33,
0x21,
0x44,
0x44,
0x23,
0x22,
0x24,
0x32,
0x22,
0x33,
0x22,
0x22,
0x31,
0x24,
0x33,
0x11,
0x10,
0x24,
0x13,
0x23,
0x11,
0x32,
0x33,
0x33,
0x32,
0x32,
0x13,
0x13,
0x12,
0x12,
0x11,
0x11,
0x12,
0x31,
0x33,
0x33,
0x13,
0x11,
0x22,
0x31,
0x11,
0x34,
0x34,
0x32,
0x33,
0x11,
0x22,
0x21,
0x31,
0x21,
0x43,
0x31,
0x31,
0x33,
0x13,
0x23,
0x13,
0x21,
0x22,
0x12,
0x22,
0x13,
0x33,
0x33,
0x33,
0x31,
0x13,
0x33,
0x22,
0x11,
0x33,
0x33,
0x33,
0x13,
0x33,
0x12,
0x13,
0x32,
0x33,
0x33,
0x33,
0x33,
0x01,
};
