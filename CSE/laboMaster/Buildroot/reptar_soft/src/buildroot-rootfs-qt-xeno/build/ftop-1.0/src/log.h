/*
 * ftop - show progress of open files and file systems
 * Copyright (C) 2009  Jason Todd <jtodd1@earthlink.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* log.h - ftop log facility declarations */

#ifndef __LOG_H__
#define __LOG_H__

#include <inttypes.h>       /* uint32_t */

#include "ftop.h"

int snapshot_log(snapshot *s, uint32_t level, const char *fmt, ...);
int show_log(ftop_data *p);

#endif  /* __LOG_H__ */
