.\" AUTHOR: Jason Todd
.\" Copyright (C) 2009 Jason Todd <jtodd1@earthlink.net>
.\"
.\" This is free documentation; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License as
.\" published by the Free Software Foundation; either version 3 of
.\" the License, or (at your option) any later version.
.\"
.\" The GNU General Public License's references to "object code"
.\" and "executables" are to be interpreted as the output of any
.\" document formatting or typesetting system, including
.\" intermediate and printed output.
.\"
.\" This manual is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public
.\" License along with this manual; if not, write to the Free
.\" Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111,
.\" USA.
.TH FTOP 1 "2009-02-16" "ftop 1.0"
.SH NAME
ftop \- show progress of open files and file systems
.SH SYNOPSIS
.B ftop
[options]
.SH COPYRIGHT
ftop is Copyright (C) 2009 Jason Todd.  Send bug reports and
suggestions/patches/etc. to
.IR jtodd1@earthlink.net .
.SH DESCRIPTION
The
.B ftop
program displays progress information for the open files and file systems in a
Linux system.  As processes read and write files, ftop displays data rates and
time estimates.  Its feature-rich interface is similar to
.BR top ,
and includes extensive run-time configuration options.
.PP
While this manual page contains the full documentation for
.BR ftop ,
the built-in online help is the best source for the most up to date
documentation.  To access the online help, run ftop with the
.B -h
command line option, or simply press
.RB ' h '
while ftop is running.
.SH OPTIONS
Every option listed below is supported as a command-line argument (for example,
.BR -f )
and as a run-time keyboard command
.RB ( f ) .
Thus, any option can be set when ftop is started, and can be changed later
while it is running.  Options with uppercase characters are boolean values
(enabled or disabled), and lowercase option characters require other types of
values.
.PP
Boolean options are disabled by default.  When supplied as a command-line
argument, the option is enabled.  If the corresponding key is pressed while
ftop is running, the option's value is toggled.
.PP
Non-boolean options accept values of the type shown next to their option
characters.
.I List
values accept one or more values, separated by comma characters (with no
spaces).  Values of type
.I num
are numeric, and values of type
.I user
can be specified as either a valid login or numeric UID.
.PP
Please refer to the additional details following the list of options.
.PP
.PD 0
.TP 9
.BI \-a "\| list\^"
Additional files/fs
.TP
.BR
Manually specify files or file systems to monitor.  These are in
addition to all currently open files.  If any directories are
specified, their file system usage is displayed.  This option is
useful for monitoring files written by NFS clients, or for observing
the addition of a large number of files to a file system (for example,
while extracting a large archive or restoring from a backup).
.TP
.BR
.TP
.B \-A
Addl. files/fs only
.TP
.BR
Only show the files and file systems specified in 'Additional files/fs' (see
above) and 'File system types' (see below).
.TP
.BR
.TP
.B \-B
Expand at-beginning
.TP
.BR
Show the progress bar for files that are still at the beginning.  When
this option is disabled, a file's progress bar doesn't appear until
there has been some activity.  Often, there are processes that hold
log files open for append, but haven't yet written anything new. 
Leaving this disabled reduces the clutter in the presence of those
(and similar) situations.
.TP
.BR
.TP
.BI \-c "\| num\^"
Closed files count
.TP
.BR
Additionally show the specified number of most recently closed files
for each displayed process.
.TP
.BR
.TP
.BI \-d "\| num\^"
Delay
.TP
.BR
Change the time between updates.  The value is specified in seconds,
with optional tenths or hundredths (for example: 1 or 0.5 or 1.25 are
all valid).
.TP
.BR
.TP
.B \-D
Hide path directories
.TP
.BR
Hide all leading directory components of each file path.  If the file
path refers to an unnamed pipe or socket, or otherwise is not valid,
then the full path will still be displayed.
.TP
.BR
.TP
.B \-E
Expand at-end
.TP
.BR
Show the progress bar for files that are at the end.  When this option
is disabled, a file's progress bar disappears once the process reaches
the end of the file.  Whenever a process writes to a file, the
resulting position is at the end.  Processes that write to log files
tend to have several files open in this state.  Leaving this disabled "\
reduces the clutter in the presence of those (and similar)
situations.  See also 'File keywords & sizes' below.
.TP
.BR
.TP
.BI \-f "\| list\^"
File keywords
.TP
.BR
Only show files whose path contains, anywhere within, one of the
specified keywords.  For instance, if a file /tmp/live-cd.iso is open,
and the keyword list contains cd.i, then that file will be shown.
.TP
.BR
.TP
.B \-F
Show un-expanded files
.TP
.BR
Show all files that are open within the displayed processes.  See also
'Show un-expanded procs' below.
.TP
.BR
.TP
.B \-h
Help
.TP
.BR
Toggle the help screen.
.TP
.BR
.TP
.BI \-l "\| list\^"
Log types
.TP
.BR
Specify the type(s) of messages to log.  Valid types are none, all, or
one or more of the following: err, warn, info, debug.  The value none
disables logging.  If logging is enabled, and 'Limited output mode' (see below)
is enabled, the log is output on stderr.  See also 'View log' below.
.TP
.BR
.TP
.B \-L
View log
.TP
.BR
Toggle the log screen, if logging is enabled.
.TP
.BR
.TP
.BI \-m "\| list\^"
Access modes
.TP
.BR
Specify the access mode(s) of files to show.  Valid modes are one or
more of the following: r, w, rw.  If r is specified, files open for
read-only access will be shown.  If w is specified, files open for
write-only access will be shown.  If rw is specified, then files open
for read and write will be shown.  Note, rw does not include read-only
or write-only files.
.TP
.BR
.TP
.BI \-n "\| num\^"
Iterations
.TP
.BR
Specify the total number of update iterations to run, after which ftop
will exit.
.TP
.BR
.TP
.B \-N
Show all FDs numeric
.TP
.BR
Display all file descriptors as their numeric values.  When this
option is disabled, file descriptors 0, 1, and 2 are given the
symbolic names in, out, and err, respectively, to reflect stdin,
stdout, and stderr.
.TP
.BR
.TP
.B \-o
View options
.TP
.BR
Toggle the options summary screen.
.TP
.BR
.TP
.B \-O
Limited output mode
.TP
.BR
Toggle between full output mode and limited output mode.  By default,
full mode is selected if the ncurses library is available and stdout
is a terminal (e.g. not redirected or piped).  If neither of the above
is true, then only limited mode will be available.  If this option is
specified on the command line, limited mode will be used initially. 
In limited mode, all control keys can still be used, but scrolling is
not available.
.TP
.BR
.TP
.BI \-p "\| list\^"
Process keywords/PIDs
.TP
.BR
Show only processes whose command line contains, anywhere within, one
the specified keywords, or whose PID equals a specified value.  For
instance, the list 1,bash,X will show the init process, all instances
of bash, and any running X servers (if X is within their command
line).  Note, processes with no open files that match any of the other
criteria will still not be shown.  See also 'Show un-expanded procs' below.
.TP
.BR
.TP
.B \-P
Show un-expanded procs
.TP
.BR
Show all processes.  See also 'Show un-expanded files' and 'Process
keywords/PIDs' above.
.TP
.BR
.TP
.B \-q
Quit
.TP
.BR
Exit from ftop.
.TP
.BR
.TP
.B \-Q
Hide tput & est. time
.TP
.BR
Do not show any throughput or estimated time remaining calculations. 
This is useful when simply monitoring how far into files the processes
are, especially if used with a very small delay (see 'Delay' above).
.TP
.BR
.TP
.B \-r
Refresh display
.TP
.BR
Force a refresh of the display.
.TP
.BR
.TP
.BI \-t "\| list\^"
File types
.TP
.BR
Specify the type(s) of files to show.  Valid types are all, or one or
more of the following: f, d, c, b, p, s, x.  Also allowed is r, an
alias for f.  The meaning of each is: f (regular file), d (directory),
c (character device), b (block device), p (pipe/FIFO), s (socket), and
x (miscellaneous - unknown or unclassifiable).
.TP
.BR
.TP
.BI \-u "\| user\^"
User name/UID
.TP
.BR
Only show processes with the given real user id.  The user can be
specified either as a name or as a UID.
.TP
.BR
.TP
.B \-U
Units in base 10
.TP
.BR
If this option is disabled, sizes and throughputs will be reported in
units based on powers of 2.  Specifically, M (megabyte) corresponds to
2 to the 20th power and G (gigabyte) corresponds to 2 to the 30th.  If
this option is enabled, the units e6 (million bytes) and e9 (billion
bytes) will be used instead.  Note, either form is always acceptable
when entering size values, regardless of this setting.
.TP
.BR
.TP
.B \-v
Version and license
.TP
.BR
Toggle the version and license screen.
.TP
.BR
.TP
.BI \-x "\| num\^"
Max no-match iterations
.TP
.BR
Specify the number of iterations to remain running when there are no
open files matching any of the other criteria.  At least one match has
to have been shown, and subsequently been closed, before the remaining
iterations are counted.  If during those iterations, another matching
file is shown, the counter is reset.  See also 'Iterations' above.
.TP
.BR
.TP
.BI \-y "\| list\^"
File system types
.TP
.BR
Show all mounted filesystems of the specified types.  Valid types are
all, or one or more of the types supported by the kernel (refer to
/proc/filesystems).  The display is updated to reflect the mounting
or unmounting of matching file systems.
.TP
.BR
.TP
.BI \-z "\| list\^"
File keywords & sizes
.TP
.BR
Specify pre-determined final sizes to use for files opened for write
access and for file systems (see 'Additional files/fs' above).  The values
in the list must be of the form keyword=size, where keyword is applied
as in 'File keywords' (see above) and size is given in bytes,
megabytes (or million bytes), or gigabytes (or billion bytes) with
optional tenths value.  See 'Units in base 10' above.  The following is an
example of a valid list:
.in +4n
data_out.bin=1.5G,/tmp/=700e6,some_file.copy=120000
.in
.PD
.SH NOTES
The three characters displayed after a file's descriptor represent
the type of file and the access mode.  See 'File types' and 'Access modes'
for details on each.  In the case of a file system, FS-
will be displayed.  If a file is opened in append mode, an uppercase W
will be displayed instead of a lowercase w.
.PP
The two characters following the access mode represent activity.  If
-- is displayed, there is no activity.  If the position is advancing
in a forward direction, then ->, >>, or >- will be displayed depending
on how much progress is occurring.  If the position is moving
backward, then -<, <<, or <- will be displayed.  If the position is
moving in a somewhat random fashion, then <> will be displayed. 
Finally, any time a different file is open for the given descriptor,
** is displayed.
.PP
When monitoring additional files, only the current total size of the
file can be obtained; no read or write position is available.  Thus,
for display purposes, the file is reported as being opened for
write-only access, and the current file size is used as its position. 
If the ultimate size of the file is known, it can be supplied with 'File
keywords & sizes' and that value will then be reported as the size.
.PP
For file systems, the amount of space currently consumed is used as
the position.  In both cases (additional files and file systems), the
reported position value is still used in throughput calculations and
remaining time estimates.
.SH EXAMPLES
To see a real-time graphic representation of all mounted ext3 and
nfs file systems:
.sp
.in +4n
ftop -QAy ext3,nfs
.in
.sp
The following command provides very interesting feedback when
working with large gzipped or bzip2'ed tar files.  Note, the exact
same command can be used to monitor both creating the archive file,
and extracting its contents.  The specified file system (/share/ in
this example) should be where the file is being created, or where its
contents are being placed (in which case multiple file systems may
need to be given).  Also, the delay time may need to be adjusted to
achieve the desired effects.
.sp
.in +4n
ftop -d .1 -c 4 -p tar,gzip -t f,p -a /share/
.in
.sp
If a file /tmp/output.bin is to be created by another process, and
it is known that its resulting size will be approximately 6.5GB, then
the following command will monitor the progress for at most 60
seconds, but will exit earlier if the process completes early or the
file is otherwised closed for more than 1 second.
.sp
.in +4n
ftop -f mp/outp -z output.bin=6.5G -n 60 -x 1
.in
.sp
If, in the example above, the file is being created by an NFS
client, then the file will not be open locally.  In this case, the
command would be as follows:
.sp
.in +4n
ftop -Aa /tmp/output.bin -z output.bin=6.5G -n 60 -x 1
.in
.sp
It can be interesting to see disk access patterns in different
operating systems as they boot and perform their duties (or as a
system is booting from a CD or DVD).  With an emulator or
virtualization environment, and provided the emulator doesn't do much
caching of the virtualized disks, this behavior can be monitored for
any number of disk image files with a command such as the following:
.sp
.in +4n
ftop -f cd.img,hdd.img -d 0.1 -QBED
.in
.sp
To capture a log of progress's activity for one iteration (useful if
sending a bug report):
.sp
.in +4n
ftop -On 1 -l all 2> log.txt
.in
.SH SEE ALSO
.BR top (1)
