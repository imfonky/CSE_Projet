/*
 * ftop - show progress of open files and file systems
 * Copyright (C) 2009  Jason Todd <jtodd1@earthlink.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* help.c - ftop help screen */

#include "ftop.h"
#include "ui.h"
#include "help.h"

static const char *help_intro[] = HELP_INTRO_ARRAY;
static const char *help_end[] = HELP_END_ARRAY;
static const help_section_def help_section[] = HELP_SECTION_ARRAY;

int show_help(ftop_data *p, int part)
{
    size_t i;
    const help_section_def *h;

    screen_title(p, "HELP", OPT_HELP_CHAR);

    for (i = 0; help_intro[i] != NULL; i++)
        show_paragraph(p, (char *)(help_intro[i]), 2, part);

    for (i = 0; help_section[i].c != '\0'; i++)
    {
        h = &(help_section[i]);
        p_printf(p, "  ");
        p_attron(p, A_BOLD);
        p_printf(p, "%c", h->c);
        p_attroff(p, A_BOLD);

        p_printf(p, "  %-4s  %s ", h->type_str, h->name_str);
        p_eol(p, part);

        show_paragraph(p, h->help_str, 11, part);
        p_eol(p, part);
    }

    for (i = 0; help_end[i] != NULL; i++)
        show_paragraph(p, (char *)(help_end[i]), 2, part);

    return 1;
}
