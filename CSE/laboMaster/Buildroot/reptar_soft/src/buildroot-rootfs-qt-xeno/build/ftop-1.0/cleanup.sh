#!/bin/bash

# ftop - show progress of open files and file systems
# Copyright (C) 2009  Jason Todd <jtodd1@earthlink.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# cleanup.sh - make pre-dist, ONLY to be used to assist development of ftop

make uninstall &> /dev/null
make distclean &> /dev/null
rm -rf .deps/ .gdb_history Makefile.in Makefile aclocal.m4 autom4te.cache/ autoscan.log config.guess config.h config.h.in config.log config.status config.sub configure.scan configure depcomp install-sh ltmain.sh missing stamp-h1
rm -f support/*
make -C src/ -f makefile-dev clean &> /dev/null
rm -rf src/.deps/ src/Makefile src/Makefile.in src/*.dSYM/
