/*
 * ftop - show progress of open files and file systems
 * Copyright (C) 2009  Jason Todd <jtodd1@earthlink.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* version.c - ftop version & license screen */

#include <string.h>         /* strchr */

#include "ftop.h"
#include "ui.h"
#include "version.h"
#include "license.h"
#include "options.h"

int show_version(ftop_data *p, int part)
{
    char *next, *newline;

    screen_title(p, "VERSION & LICENSE", OPT_VERSION_CHAR);

    p_printf(p, "%s  %s", PACKAGE_STRING, FTOP_COPYRIGHT_STRING);
    p_eol(p, part);
    p_eol(p, part);

    show_paragraph(p, FTOP_LICENSE_SUMMARY, 0, part);
    p_eol(p, part);

    if (p->term_size.ws_col < 80)
    {
        p_attron(p, A_BOLD);
        show_paragraph(p,
                       "Warning: this display will not show the license " \
                       "properly; please view on a display with a minimum " \
                       "of 80 columns.", 0, part);

        p_attroff(p, A_BOLD);
        p_eol(p, part);
    }

    p_eol(p, part);
    next = (char *)license;
    while (1)
    {
        if ((newline = strchr(next, '\n')) != NULL)
            *newline = '\0';

        p_printf(p, "%s", next);
        p_eol(p, part);

        if (newline == NULL)
            break;

        *(newline++) = '\n';
        next = newline;
    }

    return 1;
}
