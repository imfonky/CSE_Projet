/*
 * ftop - show progress of open files and file systems
 * Copyright (C) 2009  Jason Todd <jtodd1@earthlink.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ui.h - ftop user interface declarations */

#ifndef __UI_H__
#define __UI_H__

#ifdef HAVE_LIBCURSES
#include <curses.h>         /* A_BOLD, A_REVERSE, KEY_DOWN, KEY_UP, */
                            /* KEY_NPAGE, KEY_PPAGE, KEY_HOME, KEY_END, */
                            /* KEY_BACKSPACE */
#endif

#include "ftop.h"

#ifndef HAVE_LIBCURSES
#define A_BOLD              0
#define A_REVERSE           0
#define KEY_DOWN            256
#define KEY_UP              257
#define KEY_NPAGE           258
#define KEY_PPAGE           259
#define KEY_HOME            260
#define KEY_END             261
#define KEY_BACKSPACE       262
#endif

/* primitives */
int init_terminal(ftop_data *p);
int restore_terminal(ftop_data *p);
int p_printf(ftop_data *p, const char *fmt, ...);
int p_repeat(ftop_data *p, int cols, int min, char c);
int p_eol(ftop_data *p, int part);
int p_move(ftop_data *p, int y, int x);
int p_attron(ftop_data *p, int attrs);
int p_attroff(ftop_data *p, int attrs);
int p_endoutput(ftop_data *p);
int p_refresh(ftop_data *p);
int p_getkey(ftop_data *p, int *c);

/* higher level functions */
int show_paragraph(ftop_data *p, char *str, int indent, int part);
int p_input_key(ftop_data *p, char c);

#endif  /* __UI_H__ */
