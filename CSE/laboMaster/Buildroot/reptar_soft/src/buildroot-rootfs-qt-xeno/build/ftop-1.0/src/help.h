/*
 * ftop - show progress of open files and file systems
 * Copyright (C) 2009  Jason Todd <jtodd1@earthlink.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* help.h - ftop help screen text and layout information */

#ifndef __HELP_H__
#define __HELP_H__

#include "ftop.h"
#include "options.h"

#define HELP_INTRO_1 \
    "Every option listed below is supported as a command-line argument " \
    "(for example, -f) and as a run-time keyboard command (f).  Thus, any " \
    "option can be set when ftop is started, and can be changed later while " \
    "it is running.  Options with uppercase characters are boolean values " \
    "(enabled or disabled), and lowercase option characters require other " \
    "types of values.\n"

#define HELP_INTRO_2 \
    "Boolean options are disabled by default.  When supplied as a " \
    "command-line argument, the option is enabled.  If the corresponding " \
    "key is pressed while ftop is running, the option's value is toggled.\n"

#define HELP_INTRO_3 \
    "Non-boolean options accept values of the type shown next to their " \
    "option characters.  \"List\" values accept one or more values, " \
    "separated by comma characters (with no spaces).  Values of type " \
    "\"num\" are numeric, and values of type \"user\" can be specified as " \
    "either a valid login or numeric UID.\n"

#define HELP_INTRO_4 \
    "Please refer to the additional details following the list of options.\n"

#define HELP_INTRO_ARRAY \
    {HELP_INTRO_1, HELP_INTRO_2, HELP_INTRO_3, HELP_INTRO_4, NULL}

#define HELP_END_1 "\bNOTES"

#define HELP_END_2 \
    "\tThe three characters displayed after a file's descriptor represent " \
    "the type of file and the access mode.  See '" OPT_TYPE_STR "' and '" \
    OPT_MODE_STR "' for details on each.  In the case of a file system, FS- " \
    "will be displayed.  If a file is opened in append mode, an uppercase W " \
    "will be displayed instead of a lowercase w.\n"

#define HELP_END_3 \
    "\tThe two characters following the access mode represent activity.  If " \
    "-- is displayed, there is no activity.  If the position is advancing " \
    "in a forward direction, then ->, >>, or >- will be displayed depending " \
    "on how much progress is occurring.  If the position is moving " \
    "backward, then -<, <<, or <- will be displayed.  If the position is " \
    "moving in a somewhat random fashion, then <> will be displayed.  " \
    "Finally, any time a different file is open for the given descriptor, " \
    "** is displayed.\n"

#define HELP_END_4 \
    "\tWhen monitoring additional files, only the current total size of the " \
    "file can be obtained; no read or write position is available.  Thus, " \
    "for display purposes, the file is reported as being opened for " \
    "write-only access, and the current file size is used as its position.  " \
    "If the ultimate size of the file is known, it can be supplied with '" \
    OPT_SIZE_STR "' and that value will then be reported as the size.\n"

#define HELP_END_5 \
    "\tFor file systems, the amount of space currently consumed is used as " \
    "the position.  In both cases (additional files and file systems), the " \
    "reported position value is still used in throughput calculations and " \
    "remaining time estimates.\n"

#define HELP_END_6 "\bEXAMPLES"

#define HELP_END_7 \
    "\tTo see a real-time graphic representation of all mounted ext3 and " \
    "nfs file systems:\n"

#define HELP_END_8 \
    "\t\t\tftop -QAy ext3,nfs\n"

#define HELP_END_9 \
    "\tThe following command provides very interesting feedback when " \
    "working with large gzipped or bzip2'ed tar files.  Note, the exact " \
    "same command can be used to monitor both creating the archive file, " \
    "and extracting its contents.  The specified file system (/share/ in " \
    "this example) should be where the file is being created, or where its " \
    "contents are being placed (in which case multiple file systems may " \
    "need to be given).  Also, the delay time may need to be adjusted to " \
    "achieve the desired effects.\n"

#define HELP_END_10 \
    "\t\t\tftop -d .1 -c 4 -p tar,gzip -t f,p -a /share/\n"

#define HELP_END_11 \
    "\tIf a file /tmp/output.bin is to be created by another process, and " \
    "it is known that its resulting size will be approximately 6.5GB, then " \
    "the following command will monitor the progress for at most 60 " \
    "seconds, but will exit earlier if the process completes early or the " \
    "file is otherwised closed for more than 1 second.\n"

#define HELP_END_12 \
    "\t\t\tftop -f mp/outp -z output.bin=6.5G -n 60 -x 1\n"

#define HELP_END_13 \
    "\tIf, in the example above, the file is being created by an NFS " \
    "client, then the file will not be open locally.  In this case, the " \
    "command would be as follows:\n"

#define HELP_END_14 \
    "\t\t\tftop -Aa /tmp/output.bin -z output.bin=6.5G -n 60 -x 1\n"

#define HELP_END_15 \
    "\tIt can be interesting to see disk access patterns in different " \
    "operating systems as they boot and perform their duties (or as a " \
    "system is booting from a CD or DVD).  With an emulator or " \
    "virtualization environment, and provided the emulator doesn't do much " \
    "caching of the virtualized disks, this behavior can be monitored for " \
    "any number of disk image files with a command such as the following:\n"

#define HELP_END_16 \
    "\t\t\tftop -f cd.img,hdd.img -d 0.1 -QBED\n"

#define HELP_END_17 \
    "\tTo capture a log of progress's activity for one iteration (useful if " \
    "sending a bug report):\n"

#define HELP_END_18 \
    "\t\t\tftop -On 1 -l all 2> log.txt\n"

#define HELP_END_ARRAY \
    {HELP_END_1, HELP_END_2, HELP_END_3, HELP_END_4, HELP_END_5, HELP_END_6, \
     HELP_END_7, HELP_END_8, HELP_END_9, HELP_END_10, HELP_END_11, \
     HELP_END_12, HELP_END_13, HELP_END_14, HELP_END_15, HELP_END_16, \
     HELP_END_17, HELP_END_18, NULL}

typedef struct _help_section_def
{
    char c;
    char *type_str;
    char *name_str;
    char *help_str;
} help_section_def;

#define HELP_SECTION_ARRAY \
{ \
    {OPT_ADD_CHAR, "list", OPT_ADD_STR, OPT_ADD_HELP}, \
    {OPT_ADD_ONLY_CHAR, "", OPT_ADD_ONLY_STR, OPT_ADD_ONLY_HELP}, \
    {OPT_EXPAND_ATBEGIN_CHAR, "", OPT_EXPAND_ATBEGIN_STR, \
     OPT_EXPAND_ATBEGIN_HELP}, \
    {OPT_CLOSED_CHAR, "num", OPT_CLOSED_STR, OPT_CLOSED_HELP}, \
    {OPT_DELAY_CHAR, "num", OPT_DELAY_STR, OPT_DELAY_HELP}, \
    {OPT_HIDE_DIR_CHAR, "", OPT_HIDE_DIR_STR, OPT_HIDE_DIR_HELP}, \
    {OPT_EXPAND_ATEND_CHAR, "", OPT_EXPAND_ATEND_STR, OPT_EXPAND_ATEND_HELP}, \
    {OPT_FILE_CHAR, "list", OPT_FILE_STR, OPT_FILE_HELP}, \
    {OPT_UNEXP_FILES_CHAR, "", OPT_UNEXP_FILES_STR, OPT_UNEXP_FILES_HELP}, \
    {OPT_HELP_CHAR, "", OPT_HELP_STR, OPT_HELP_HELP}, \
    {OPT_LOG_CHAR, "list", OPT_LOG_STR, OPT_LOG_HELP}, \
    {OPT_VIEW_LOG_CHAR, "", OPT_VIEW_LOG_STR, OPT_VIEW_LOG_HELP}, \
    {OPT_MODE_CHAR, "list", OPT_MODE_STR, OPT_MODE_STR_HELP}, \
    {OPT_ITERATIONS_CHAR, "num", OPT_ITERATIONS_STR, OPT_ITERATIONS_HELP}, \
    {OPT_FD_NUMBERS_CHAR, "", OPT_FD_NUMBERS_STR, OPT_FD_NUMBERS_HELP}, \
    {OPT_OPTIONS_CHAR, "", OPT_OPTIONS_STR, OPT_OPTIONS_HELP}, \
    {OPT_SWITCH_OUTPUT_CHAR, "", OPT_SWITCH_OUTPUT_STR, \
     OPT_SWITCH_OUTPUT_HELP}, \
    {OPT_PROCS_CHAR, "list", OPT_PROCS_STR, OPT_PROCS_HELP}, \
    {OPT_UNEXP_PROCS_CHAR, "", OPT_UNEXP_PROCS_STR, OPT_UNEXP_PROCS_HELP}, \
    {OPT_QUIT_CHAR, "", OPT_QUIT_STR, OPT_QUIT_HELP}, \
    {OPT_QUIET_CHAR, "", OPT_QUIET_STR, OPT_QUIET_HELP}, \
    {OPT_REFRESH_CHAR, "", OPT_REFRESH_STR, OPT_REFRESH_HELP}, \
    {OPT_TYPE_CHAR, "list", OPT_TYPE_STR, OPT_TYPE_HELP}, \
    {OPT_UID_CHAR, "user", OPT_UID_STR, OPT_UID_HELP}, \
    {OPT_UNITS_BASE10_CHAR, "", OPT_UNITS_BASE10_STR, OPT_UNITS_BASE10_HELP}, \
    {OPT_VERSION_CHAR, "", OPT_VERSION_STR, OPT_VERSION_HELP}, \
    {OPT_MAX_NO_MATCH_CHAR, "num", OPT_MAX_NO_MATCH_STR, \
     OPT_MAX_NO_MATCH_HELP}, \
    {OPT_FSTYPE_CHAR, "list", OPT_FSTYPE_STR, OPT_FSTYPE_HELP}, \
    {OPT_SIZE_CHAR, "list", OPT_SIZE_STR, OPT_SIZE_HELP}, \
    {'\0', NULL, NULL, NULL} \
}

int show_help(ftop_data *p, int part);

#endif  /* __HELP_H__ */
