# This is a -*-perl-*- script
#
# Set variables that were defined by configure, in case we need them
# during the tests.

%CONFIG_FLAGS = (
    AM_LDFLAGS   => '-Wl,--export-dynamic',
    AR           => '/home/redsuser/Desktop/reptar_soft/src/buildroot-rootfs-qt-xeno/host/usr/bin/arm-linux-gnueabihf-ar',
    CC           => '/home/redsuser/Desktop/reptar_soft/src/buildroot-rootfs-qt-xeno/host/usr/bin/arm-linux-gnueabihf-gcc',
    CFLAGS       => '-D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64  -O3 ',
    CPP          => '/home/redsuser/Desktop/reptar_soft/src/buildroot-rootfs-qt-xeno/host/usr/bin/arm-linux-gnueabihf-cpp',
    CPPFLAGS     => '-D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64',
    GUILE_CFLAGS => '',
    GUILE_LIBS   => '',
    LDFLAGS      => '',
    LIBS         => '-ldl '
);

1;
