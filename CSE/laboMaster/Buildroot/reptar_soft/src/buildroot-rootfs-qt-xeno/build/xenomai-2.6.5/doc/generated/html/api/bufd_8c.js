var bufd_8c =
[
    [ "xnbufd_copy_from_kmem", "group__bufd.html#ga626f17350a3f93fb3f8010bc6c16b560", null ],
    [ "xnbufd_copy_to_kmem", "group__bufd.html#ga709e59aa0c61033bcd9f43cf42501dfe", null ],
    [ "xnbufd_invalidate", "group__bufd.html#gad2c63d67c47dd4f7f4d2eef19ba7ca5c", null ],
    [ "xnbufd_unmap_kread", "group__bufd.html#ga26c4848d4354c10ece65202120da0e36", null ],
    [ "xnbufd_unmap_kwrite", "group__bufd.html#gaae53741b014081f950351ad91298c304", null ],
    [ "xnbufd_unmap_uread", "group__bufd.html#gaf6f7858e18500662fcd0ff3ff4777a74", null ],
    [ "xnbufd_unmap_uwrite", "group__bufd.html#ga018218c68a27436fc90b1d5486ab1c71", null ]
];