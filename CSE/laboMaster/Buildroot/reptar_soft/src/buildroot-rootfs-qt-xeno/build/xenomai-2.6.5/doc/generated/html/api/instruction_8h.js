var instruction_8h =
[
    [ "A4L_INSN_BITS", "group__sync1__lib.html#gad02735fca71fa7e07dad273e4cd957a8", null ],
    [ "A4L_INSN_CONFIG", "group__sync1__lib.html#gaa5e0f8dade0defa8b9c9f0788dbf6e6f", null ],
    [ "A4L_INSN_GTOD", "group__sync1__lib.html#gad342149a83edc18ca7960274b6775681", null ],
    [ "A4L_INSN_INTTRIG", "group__sync1__lib.html#ga7ad6aa3c3f85aba61da5335d54c41b56", null ],
    [ "A4L_INSN_READ", "group__sync1__lib.html#ga63e8c577aa294bf49711299240c29354", null ],
    [ "A4L_INSN_WAIT", "group__sync1__lib.html#ga1b099a4e1edc630ac74e3e64aa4d479b", null ],
    [ "A4L_INSN_WAIT_MAX", "group__sync1__lib.html#ga976d8de479578a3e1f5d844401fe4153", null ],
    [ "A4L_INSN_WRITE", "group__sync1__lib.html#ga3039f40be8811e7393a8c1d3e0eebe6e", null ]
];