var sys_8c =
[
    [ "a4l_sys_attach", "group__attach__sys.html#ga29c9cb07069b7e574f76c7c8c9cb082d", null ],
    [ "a4l_sys_bufcfg", "group__attach__sys.html#ga8a09c9aad6ecfd08340c323f427c1555", null ],
    [ "a4l_sys_close", "group__basic__sys.html#gaf3d4f7cf52691b72fdcc76cab4eb0b16", null ],
    [ "a4l_sys_detach", "group__attach__sys.html#gac4a293d8e2ed56d3f6661a375ca94511", null ],
    [ "a4l_sys_open", "group__basic__sys.html#ga91598f2303b3d33a0cec672d098312e3", null ],
    [ "a4l_sys_read", "group__basic__sys.html#gaa0aca0a2619418924133caf456ca30b2", null ],
    [ "a4l_sys_write", "group__basic__sys.html#ga12e1790de04a08f8a6b64e4714c7a2c8", null ]
];