var native_2heap_8h =
[
    [ "rt_heap_info", "structrt__heap__info.html", null ],
    [ "RT_HEAP_INFO", "native_2heap_8h.html#a546374c171464e2a1c754cfcbeebc7d7", null ],
    [ "rt_heap_alloc", "group__native__heap.html#ga52678149ff2b66e47aa8cdcddab653a4", null ],
    [ "rt_heap_create", "group__native__heap.html#ga1d19ad24dc9f94b969aa0f574170bdc4", null ],
    [ "rt_heap_delete", "group__native__heap.html#ga08f8d244a545fd6a5719ad5708552552", null ],
    [ "rt_heap_free", "group__native__heap.html#ga300ca8c868d02dad99790fe39b443f6f", null ],
    [ "rt_heap_inquire", "group__native__heap.html#ga6af669528e1bcae4f1ed15b292bda08e", null ]
];