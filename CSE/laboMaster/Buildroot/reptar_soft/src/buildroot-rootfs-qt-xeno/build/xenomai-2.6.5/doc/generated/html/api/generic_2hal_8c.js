var generic_2hal_8c =
[
    [ "rthal_apc_alloc", "group__hal.html#gafb91722da28118853a09b1a0d98dd169", null ],
    [ "rthal_apc_free", "group__hal.html#gaf11636f94b0a6913241679d84265eba6", null ],
    [ "rthal_irq_release", "group__hal.html#gad7d90c5882463a1d36b7a0ad74a9d4ff", null ],
    [ "rthal_irq_request", "group__hal.html#gaadf0f98059a5bf1caddda9dd48d51f20", null ],
    [ "rthal_trap_catch", "group__hal.html#ga3f868180536815c231983781afef99c4", null ]
];