var async_8c =
[
    [ "a4l_async_read", "group__async2__lib.html#gafb12b8f4eed71008a8b4225e3d90dcea", null ],
    [ "a4l_async_write", "group__async2__lib.html#gad2ba21d06b77b8b25a80029484731ee6", null ],
    [ "a4l_get_bufsize", "group__async1__lib.html#gad6a58d164ad6edccf9ef213778102ca0", null ],
    [ "a4l_mark_bufrw", "group__async1__lib.html#gaa95b4dc428000e0b41047c3ed4c5b61c", null ],
    [ "a4l_mmap", "group__async1__lib.html#gad1ba6a0d5954e5e68365a206d55ee251", null ],
    [ "a4l_poll", "group__async1__lib.html#ga43ab969b00aae973ef4b67c234b17720", null ],
    [ "a4l_set_bufsize", "group__async1__lib.html#ga247528c4d75db12782aad05b41faf394", null ],
    [ "a4l_snd_cancel", "group__async1__lib.html#ga70dd6f96f60e9c839eb1ffc8538fc6c7", null ],
    [ "a4l_snd_command", "group__async1__lib.html#gae802726a40cc65522546fb2f75cd5f3f", null ]
];