var nucleus_2sched_8h =
[
    [ "XNHDEFER", "group__sched.html#ga3ce2ed190992e69e07b6433794c6904c", null ],
    [ "XNHTICK", "group__sched.html#ga8f0b4d155c033d2b114048584297f38f", null ],
    [ "XNINIRQ", "group__sched.html#ga6ffc2b94bfc0ce2c0fcfce31fe5cb805", null ],
    [ "XNINLOCK", "group__sched.html#gafe08c742fcc43105818b10bb530ff109", null ],
    [ "XNINSW", "group__sched.html#gafe5a3d33e5a6346af13c13c16417c1f5", null ],
    [ "XNINTCK", "group__sched.html#ga7f2497dd28326f2056c63829867270e4", null ],
    [ "XNKCOUT", "group__sched.html#ga773676aa91e705edcd8fade6ed5492d8", null ],
    [ "XNRESCHED", "group__sched.html#gafa67595d7852acec33ea04f89df8d0eb", null ],
    [ "XNRPICK", "group__sched.html#ga6f83e366bf254b6aaad4533c2428640b", null ],
    [ "xnsched_t", "group__sched.html#gaee183ef5060a8a65301f8a18b4ba6c57", null ],
    [ "xnsched_rotate", "group__sched.html#ga4cb702f4a99401c3c1c73811f55e990f", null ]
];