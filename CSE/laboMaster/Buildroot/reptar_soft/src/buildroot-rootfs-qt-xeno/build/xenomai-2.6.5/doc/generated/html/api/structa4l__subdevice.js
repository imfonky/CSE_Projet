var structa4l__subdevice =
[
    [ "buf", "structa4l__subdevice.html#a102957cf30f6328e9b9cac05e4f980ec", null ],
    [ "cancel", "structa4l__subdevice.html#a1b04d2e942c08a5f68e0df97b368c1aa", null ],
    [ "chan_desc", "structa4l__subdevice.html#a435021004757c2b3d30422183f5c9ef5", null ],
    [ "cmd_mask", "structa4l__subdevice.html#ad15a08eed66ded21d0e918d1565801bf", null ],
    [ "dev", "structa4l__subdevice.html#a7c24cee67a888f7c6786e76a5375b534", null ],
    [ "do_cmd", "structa4l__subdevice.html#a5294c9db70bab602e5157575d7856d55", null ],
    [ "do_cmdtest", "structa4l__subdevice.html#a755103cc69a2c2ebb63f2ed4f493a849", null ],
    [ "flags", "structa4l__subdevice.html#a1c2510cef59e5ec58cb3edee397d886f", null ],
    [ "idx", "structa4l__subdevice.html#ad19d9a857a45957e0bbbb8d28baab791", null ],
    [ "insn_bits", "structa4l__subdevice.html#abcbf6dfa8a956d92fc328ca3df15f38b", null ],
    [ "insn_config", "structa4l__subdevice.html#a9d19a2d6c6561060e39fa0bdc9ec5447", null ],
    [ "insn_read", "structa4l__subdevice.html#a2766dcf976e1bbc8029d628b351d153a", null ],
    [ "insn_write", "structa4l__subdevice.html#a86b4f2af2836d7813de23a8f73483a02", null ],
    [ "list", "structa4l__subdevice.html#a3231505de3d86eea6446e5df3d508ef2", null ],
    [ "munge", "structa4l__subdevice.html#a563a579a22acc33d6a25bedb81f976ef", null ],
    [ "priv", "structa4l__subdevice.html#ae6526aded52b0793cd9f032472bc5b41", null ],
    [ "rng_desc", "structa4l__subdevice.html#a79d92d091d353625d8e63c4e550d7d32", null ],
    [ "status", "structa4l__subdevice.html#af9aac6252faa3c04e045819823977ac8", null ],
    [ "trigger", "structa4l__subdevice.html#a663894bec709ae7c2fe1906b85896086", null ]
];