var NAVTREE =
[
  [ "Xenomai API", "index.html", [
    [ "Deprecated List", "deprecated.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ],
    [ "Examples", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"8255_8c.html",
"group__async1__lib.html#ga5d237ebfb439ecb144cf0b2ddba73848",
"group__devregister.html#ga7c66ec8f269c701237437177af0704e8",
"group__mutex.html#ga39b3a0e7f6f6ee41b8068ed59e25d1d1",
"group__pod.html#gacf5b53f0405351327b89b0cc4976b962",
"group__rtcan.html#ga1c314e1f81a7211a9778da835202a741",
"group__rtdmsync.html#ga1217402b82034b26fe25c26f1e5b32c9",
"group__rtipc.html#gaf2d1ed6a34336a6f3df80fb518325846",
"group__task.html#ga3bed8d001e212a3328a4e7e73f1765e8",
"include_2nucleus_2timer_8h_source.html",
"structcan__filter.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';