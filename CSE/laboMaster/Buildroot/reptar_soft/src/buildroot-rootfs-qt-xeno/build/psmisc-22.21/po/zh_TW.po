# Traditional Chinese Messages for psmisc.
# Copyright (C) 2008 Free Software Foundation, Inc.
# This file is distributed under the same license as the psmisc package.
# Wei-Lun Chao <bluebat@member.fsf.org>, 2009, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: psmisc 22.20rc1\n"
"Report-Msgid-Bugs-To: csmall@small.dropbear.id.au\n"
"POT-Creation-Date: 2014-02-02 17:04+1100\n"
"PO-Revision-Date: 2013-05-31 23:39+0800\n"
"Last-Translator: Wei-Lun Chao <bluebat@member.fsf.org>\n"
"Language-Team: Chinese (traditional) <zh-l10n@linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: src/fuser.c:133
#, c-format
msgid ""
"Usage: fuser [-fMuvw] [-a|-s] [-4|-6] [-c|-m|-n SPACE] [-k [-i] [-SIGNAL]] "
"NAME...\n"
"       fuser -l\n"
"       fuser -V\n"
"Show which processes use the named files, sockets, or filesystems.\n"
"\n"
"  -a,--all              display unused files too\n"
"  -i,--interactive      ask before killing (ignored without -k)\n"
"  -k,--kill             kill processes accessing the named file\n"
"  -l,--list-signals     list available signal names\n"
"  -m,--mount            show all processes using the named filesystems or "
"block device\n"
"  -M,--ismountpoint     fulfill request only if NAME is a mount point\n"
"  -n,--namespace SPACE  search in this name space (file, udp, or tcp)\n"
"  -s,--silent           silent operation\n"
"  -SIGNAL               send this signal instead of SIGKILL\n"
"  -u,--user             display user IDs\n"
"  -v,--verbose          verbose output\n"
"  -w,--writeonly        kill only processes with write access\n"
"  -V,--version          display version information\n"
msgstr ""
"用法：fuser [-fMuvw] [-a|-s] [-4|-6] [-c|-m|-n 空格] [-k [-i] [-信號]] 名"
"稱…\n"
"     fuser -l\n"
"     fuser -V\n"
"顯示何項行程使用指名的檔案、通訊端或檔案系統。\n"
"\n"
"  -a,--all              也顯示未使用的檔案\n"
"  -i,--interactive      砍除之前先詢問 (若無 -k 則忽略)\n"
"  -k,--kill             砍除存取指名檔案的行程\n"
"  -l,--list-signals     列出可用的信號名稱\n"
"  -m,--mount            顯示所有使用指名檔案系統或區塊裝置的行程\n"
"  -M,--ismountpoint     只有當名稱是掛載點時才完全達到要求\n"
"  -n,--namespace 空間    在這個名稱空間中搜尋 (檔案、udp 或 tcp)\n"
"  -s,--silent           安靜地作業\n"
"  -SIGNAL               發送這個信號以代替 SIGKILL\n"
"  -u,--user             顯示使用者識別號\n"
"  -v,--verbose          詳細的輸出\n"
"  -w,--writeonly        只砍除有寫入權限的行程\n"
"  -V,--version          顯示版本資訊\n"

#: src/fuser.c:150
#, c-format
msgid ""
"  -4,--ipv4             search IPv4 sockets only\n"
"  -6,--ipv6             search IPv6 sockets only\n"
msgstr ""
"  -4,--ipv4             只搜尋 IPv4 通訊端\n"
"  -6,--ipv6             只搜尋 IPv6 通訊端\n"

#: src/fuser.c:153
#, c-format
msgid ""
"  -                     reset options\n"
"\n"
"  udp/tcp names: [local_port][,[rmt_host][,[rmt_port]]]\n"
"\n"
msgstr ""
"  -                     重置選項\n"
"\n"
"  udp/tcp 名稱：[本地埠號][,[遠端主機][,[遠端埠號]]]\n"
"\n"

#: src/fuser.c:160
#, c-format
msgid "fuser (PSmisc) %s\n"
msgstr "fuser (PSmisc) %s\n"

#: src/fuser.c:163
#, c-format
msgid ""
"Copyright (C) 1993-2010 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"著作權©1993-2010 Werner Almesberger 和 Craig Small\n"
"\n"

#: src/fuser.c:165 src/killall.c:653 src/peekfd.c:114 src/prtstat.c:68
#: src/pstree.c:1030
#, c-format
msgid ""
"PSmisc comes with ABSOLUTELY NO WARRANTY.\n"
"This is free software, and you are welcome to redistribute it under\n"
"the terms of the GNU General Public License.\n"
"For more information about these matters, see the files named COPYING.\n"
msgstr ""
"PSmisc 完全不附帶任何擔保。\n"
"這是自由軟體，並且歡迎您依照 GNU 通用公共授權\n"
"來再次散布它。\n"
"請參看名為 COPYING 的檔案，以獲得更多關於這些問題的資訊。\n"

#: src/fuser.c:184
#, c-format
msgid "Cannot open /proc directory: %s\n"
msgstr "無法開啟 /proc 目錄：%s\n"

#: src/fuser.c:397 src/fuser.c:450 src/fuser.c:1946
#, c-format
msgid "Cannot allocate memory for matched proc: %s\n"
msgstr "無法為符合的行程配置記憶體：%s\n"

#: src/fuser.c:478
#, c-format
msgid "Specified filename %s does not exist.\n"
msgstr "指定的檔名 %s 不存在。\n"

#: src/fuser.c:481
#, c-format
msgid "Cannot stat %s: %s\n"
msgstr "無法顯示 %s:%s\n"

#: src/fuser.c:618
#, c-format
msgid "Cannot resolve local port %s: %s\n"
msgstr "無法解析本機通訊埠 %s:%s\n"

#: src/fuser.c:636
#, c-format
msgid "Unknown local port AF %d\n"
msgstr "不明的本機通訊埠 AF %d\n"

#: src/fuser.c:724
#, c-format
msgid "Cannot open protocol file \"%s\": %s\n"
msgstr "無法開啟協定檔案「%s」：%s\n"

#: src/fuser.c:896
#, fuzzy, c-format
msgid "Specified filename %s is not a mountpoint.\n"
msgstr "指定的檔名 %s 不存在。\n"

#: src/fuser.c:995
#, c-format
msgid "%s: Invalid option %s\n"
msgstr "%s：無效的選項 %s\n"

#: src/fuser.c:1046
msgid "Namespace option requires an argument."
msgstr "名稱空間選項需要一個引數。"

#: src/fuser.c:1064
msgid "Invalid namespace name"
msgstr "無效的名稱空間名稱"

#: src/fuser.c:1125
msgid "You can only use files with mountpoint options"
msgstr "您只能使用具備掛載點選項的檔案"

#: src/fuser.c:1174
msgid "No process specification given"
msgstr "沒有給定任何行程規格"

#: src/fuser.c:1186
msgid "all option cannot be used with silent option."
msgstr "所有選項無法與安靜選項一起使用。"

#: src/fuser.c:1191
msgid "You cannot search for only IPv4 and only IPv6 sockets at the same time"
msgstr "您無法同時只搜尋 IPv4 又只搜尋 IPv6 通訊端"

#: src/fuser.c:1269
#, c-format
msgid "%*s USER        PID ACCESS COMMAND\n"
msgstr "%*s 使用者      PID 存取命令\n"

#: src/fuser.c:1302 src/fuser.c:1359
msgid "(unknown)"
msgstr "(不明)"

#: src/fuser.c:1438 src/fuser.c:1477
#, c-format
msgid "Cannot stat file %s: %s\n"
msgstr "無法顯示檔案 %s：%s\n"

#: src/fuser.c:1563
#, c-format
msgid "Cannot open /proc/net/unix: %s\n"
msgstr "無法開啟 /proc/net/unix：%s\n"

#: src/fuser.c:1639
#, c-format
msgid "Kill process %d ? (y/N) "
msgstr "砍除行程 %d？(y/N) "

#: src/fuser.c:1675
#, c-format
msgid "Could not kill process %d: %s\n"
msgstr "無法砍除行程 %d：%s\n"

#: src/fuser.c:1690
#, c-format
msgid "Cannot open a network socket.\n"
msgstr "無法開啟網路通訊端。\n"

#: src/fuser.c:1694
#, c-format
msgid "Cannot find socket's device number.\n"
msgstr "找不到通訊端裝置編號。\n"

#: src/killall.c:100
#, c-format
msgid "Kill %s(%s%d) ? (y/N) "
msgstr "砍除 %s(%s%d)？(y/N) "

#: src/killall.c:103
#, c-format
msgid "Signal %s(%s%d) ? (y/N) "
msgstr "信號 %s(%s%d)？(y/N) "

#: src/killall.c:211
#, c-format
msgid "killall: Cannot get UID from process status\n"
msgstr "killall：無法從程序狀態提取 UID\n"

#: src/killall.c:237
#, c-format
msgid "killall: Bad regular expression: %s\n"
msgstr "killall：不當的正規表示式：%s\n"

#: src/killall.c:447
#, c-format
msgid "killall: skipping partial match %s(%d)\n"
msgstr "killall：跳過部分匹配 %s(%d)\n"

#: src/killall.c:562
#, c-format
msgid "Killed %s(%s%d) with signal %d\n"
msgstr "藉由信號 %4$d 砍除 %1$s(%2$s%3$d)\n"

#: src/killall.c:576
#, c-format
msgid "%s: no process found\n"
msgstr "%s：找不到任何行程\n"

#: src/killall.c:615
#, c-format
msgid ""
"Usage: killall [-Z CONTEXT] [-u USER] [ -eIgiqrvw ] [ -SIGNAL ] NAME...\n"
msgstr ""
"用法：killall [-Z 上下文] [-u 使用者] [ -eIgiqrvw ] [ -SIGNAL ] 名稱…\n"

#: src/killall.c:618
#, c-format
msgid "Usage: killall [OPTION]... [--] NAME...\n"
msgstr "用法：killall [選項]… [--] 名稱…\n"

#: src/killall.c:621
#, c-format
msgid ""
"       killall -l, --list\n"
"       killall -V, --version\n"
"\n"
"  -e,--exact          require exact match for very long names\n"
"  -I,--ignore-case    case insensitive process name match\n"
"  -g,--process-group  kill process group instead of process\n"
"  -y,--younger-than   kill processes younger than TIME\n"
"  -o,--older-than     kill processes older than TIME\n"
"  -i,--interactive    ask for confirmation before killing\n"
"  -l,--list           list all known signal names\n"
"  -q,--quiet          don't print complaints\n"
"  -r,--regexp         interpret NAME as an extended regular expression\n"
"  -s,--signal SIGNAL  send this signal instead of SIGTERM\n"
"  -u,--user USER      kill only process(es) running as USER\n"
"  -v,--verbose        report if the signal was successfully sent\n"
"  -V,--version        display version information\n"
"  -w,--wait           wait for processes to die\n"
msgstr ""
"       killall -l, --list\n"
"       killall -V, --version\n"
"\n"
"  -e,--exact          對於很長的名稱需要精確的符合\n"
"  -I,--ignore-case    行程名稱符合時大小寫不須相符\n"
"  -g,--process-group  砍除行程群組以代替行程\n"
"  -y,--younger-than   砍除時間較新的行程\n"
"  -o,--older-than     砍除時間較舊的行程\n"
"  -i,--interactive    砍除之前徵詢確認\n"
"  -l,--list           列出所有已知信號名稱\n"
"  -q,--quiet          不印出抱怨訊息\n"
"  -r,--regexp         將名稱以進階正規表示式解譯\n"
"  -s,--signal 信號    發送這個信號以代替 SIGTERM\n"
"  -u,--user 使用者    只砍除指定使用者執行的行程\n"
"  -v,--verbose        如果信號已被成功發送則回報\n"
"  -V,--version        顯示版本資訊\n"
"  -w,--wait           等待行程的消滅\n"

#: src/killall.c:639
#, c-format
msgid ""
"  -Z,--context REGEXP kill only process(es) having context\n"
"                      (must precede other arguments)\n"
msgstr ""
"  -Z,--context REGEXP 只砍除有此上下文的行程\n"
"                     (必須位於其他引數之前)\n"

#: src/killall.c:651
#, c-format
msgid ""
"Copyright (C) 1993-2012 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"著作權©1993-2012 Werner Almesberger 和 Craig Small\n"
"\n"

#: src/killall.c:741 src/killall.c:747
msgid "Invalid time format"
msgstr "無效的時間格式"

#: src/killall.c:767
#, c-format
msgid "Cannot find user %s\n"
msgstr "找不到使用者 %s\n"

#: src/killall.c:798
#, c-format
msgid "Bad regular expression: %s\n"
msgstr "不當的正規表示式：%s\n"

#: src/killall.c:830
#, c-format
msgid "killall: Maximum number of names is %d\n"
msgstr "killall：名稱數量最大值是 %d\n"

#: src/killall.c:835
#, c-format
msgid "killall: %s lacks process entries (not mounted ?)\n"
msgstr "killall：%s 缺乏行程條目 (未掛載？)\n"

#: src/peekfd.c:102
#, c-format
msgid "Error attaching to pid %i\n"
msgstr "附加到 pid %i 時發生錯誤\n"

#: src/peekfd.c:110
#, c-format
msgid "peekfd (PSmisc) %s\n"
msgstr "peekfd (PSmisc) %s\n"

#: src/peekfd.c:112
#, c-format
msgid ""
"Copyright (C) 2007 Trent Waddington\n"
"\n"
msgstr ""
"著作權 © 2007 Trent Waddington\n"
"\n"

#: src/peekfd.c:122
#, c-format
msgid ""
"Usage: peekfd [-8] [-n] [-c] [-d] [-V] [-h] <pid> [<fd> ..]\n"
"    -8 output 8 bit clean streams.\n"
"    -n don't display read/write from fd headers.\n"
"    -c peek at any new child processes too.\n"
"    -d remove duplicate read/writes from the output.\n"
"    -V prints version info.\n"
"    -h prints this help.\n"
"\n"
"  Press CTRL-C to end output.\n"
msgstr ""
"用法：peekfd [-8] [-n] [-c] [-d] [-V] [-h] <pid> [<fd> ..]\n"
"    -8 輸出不含第八位元的資料流。\n"
"    -n 不從 fd 標頭顯示讀取/寫入。\n"
"    -c 也取自任何新的子行程。\n"
"    -d 從輸出移除重複的讀取/寫入。\n"
"    -V 印出版本資訊。\n"
"    -h 印出這個說明。\n"
"\n"
"  按下 CTRL-C 以結束輸出。\n"

#: src/prtstat.c:54
#, c-format
msgid ""
"Usage: prtstat [options] PID ...\n"
"       prtstat -V\n"
"Print information about a process\n"
"    -r,--raw       Raw display of information\n"
"    -V,--version   Display version information and exit\n"
msgstr ""
"用法：prtstat [選項] PID…\n"
"     prtstat -V\n"
"印出行程的相關資訊\n"
"    -r,--raw       資訊的原始顯示\n"
"    -V,--version   顯示版本資訊然後離開\n"

#: src/prtstat.c:65
#, c-format
msgid "prtstat (PSmisc) %s\n"
msgstr "prtstat (PSmisc) %s\n"

#: src/prtstat.c:66
#, c-format
msgid ""
"Copyright (C) 2009 Craig Small\n"
"\n"
msgstr ""
"著作權©2009 Craig Small\n"
"\n"

#: src/prtstat.c:78
msgid "running"
msgstr "執行中"

#: src/prtstat.c:80
msgid "sleeping"
msgstr "暫停中"

#: src/prtstat.c:82
msgid "disk sleep"
msgstr "磁碟暫停"

#: src/prtstat.c:84
msgid "zombie"
msgstr "殭屍"

#: src/prtstat.c:86
msgid "traced"
msgstr "追蹤"

#: src/prtstat.c:88
msgid "paging"
msgstr "分頁"

#: src/prtstat.c:90
msgid "unknown"
msgstr "不明"

#: src/prtstat.c:164
#, c-format
msgid ""
"Process: %-14s\t\tState: %c (%s)\n"
"  CPU#:  %-3d\t\tTTY: %s\tThreads: %ld\n"
msgstr ""
"行程：%-14s\t\t狀態：%c (%s)\n"
"  CPU#: %-3d\t\tTTY：%s\t執行緒：%ld\n"

#: src/prtstat.c:169
#, c-format
msgid ""
"Process, Group and Session IDs\n"
"  Process ID: %d\t\t  Parent ID: %d\n"
"    Group ID: %d\t\t Session ID: %d\n"
"  T Group ID: %d\n"
"\n"
msgstr ""
"行程、群組和執行階段識別碼\n"
"    行程識別號：%d\t\t     上層識別號：%d\n"
"    群組識別號：%d\t\t 執行階段識別碼：%d\n"
"  T 群組識別號：%d\n"
"\n"

#: src/prtstat.c:175
#, c-format
msgid ""
"Page Faults\n"
"  This Process    (minor major): %8lu  %8lu\n"
"  Child Processes (minor major): %8lu  %8lu\n"
msgstr ""
"分頁錯誤\n"
"  此行程 (次要 主要)：%8lu  %8lu\n"
"  子行程 (次要 主要)：%8lu  %8lu\n"

#: src/prtstat.c:180
#, c-format
msgid ""
"CPU Times\n"
"  This Process    (user system guest blkio): %6.2f %6.2f %6.2f %6.2f\n"
"  Child processes (user system guest):       %6.2f %6.2f %6.2f\n"
msgstr ""
"CPU 時間\n"
"  此行程 (使用者 系統 訪客 區塊輸出入)：%6.2f %6.2f %6.2f %6.2f\n"
"  子行程 (使用者 系統 訪客)：           %6.2f %6.2f %6.2f\n"

#: src/prtstat.c:189
#, c-format
msgid ""
"Memory\n"
"  Vsize:       %-10s\n"
"  RSS:         %-10s \t\t RSS Limit: %s\n"
"  Code Start:  %#-10lx\t\t Code Stop:  %#-10lx\n"
"  Stack Start: %#-10lx\n"
"  Stack Pointer (ESP): %#10lx\t Inst Pointer (EIP): %#10lx\n"
msgstr ""
"記憶體\n"
"  Vsize：      %-10s\n"
"  RSS：        %-10s\t\t RSS 限制：%s\n"
"  程式開始：    %#-10lx\t\t 程式停止： %#-10lx\n"
"  堆疊開始：    %#-10lx\n"
"  堆疊指標 (ESP)：%#10lx\t 指令指標 (EIP)：%#10lx\n"

#: src/prtstat.c:199
#, c-format
msgid ""
"Scheduling\n"
"  Policy: %s\n"
"  Nice:   %ld \t\t RT Priority: %ld %s\n"
msgstr ""
"排程\n"
"  策略：%s\n"
"  善意：%ld \t\t RT 優先權：%ld %s\n"

#: src/prtstat.c:221
msgid "asprintf in print_stat failed.\n"
msgstr "print_stat 中的 asprintf 失敗。\n"

#: src/prtstat.c:226
#, c-format
msgid "Process with pid %d does not exist.\n"
msgstr "具有 pid %d 的行程不存在。\n"

#: src/prtstat.c:228
#, c-format
msgid "Unable to open stat file for pid %d (%s)\n"
msgstr "無法開啟 pid %d (%s) 的 stat 檔案\n"

#: src/prtstat.c:308
msgid "Invalid option"
msgstr "無效的選項"

#: src/prtstat.c:313
msgid "You must provide at least one PID."
msgstr "您必須提供至少一個 PID。"

#: src/prtstat.c:317
#, c-format
msgid "/proc is not mounted, cannot stat /proc/self/stat.\n"
msgstr "/proc 未被掛載，無法取得 /proc/self/stat 的狀態。\n"

#: src/pstree.c:961
#, c-format
msgid "%s is empty (not mounted ?)\n"
msgstr "%s 是空的 (尚未掛載？)\n"

#: src/pstree.c:993
#, fuzzy, c-format
msgid ""
"Usage: pstree [ -a ] [ -c ] [ -h | -H PID ] [ -l ] [ -n ] [ -p ] [ -g ] [ -"
"u ]\n"
"              [ -A | -G | -U ] [ PID | USER ]\n"
"       pstree -V\n"
"Display a tree of processes.\n"
"\n"
"  -a, --arguments     show command line arguments\n"
"  -A, --ascii         use ASCII line drawing characters\n"
"  -c, --compact       don't compact identical subtrees\n"
"  -h, --highlight-all highlight current process and its ancestors\n"
"  -H PID,\n"
"  --highlight-pid=PID highlight this process and its ancestors\n"
"  -g, --show-pgids    show process group ids; implies -c\n"
"  -G, --vt100         use VT100 line drawing characters\n"
"  -l, --long          don't truncate long lines\n"
"  -n, --numeric-sort  sort output by PID\n"
"  -N type,\n"
"  --ns-sort=type      sort by namespace type (ipc, mnt, net, pid, user, "
"uts)\n"
"  -p, --show-pids     show PIDs; implies -c\n"
"  -s, --show-parents  show parents of the selected process\n"
"  -S, --ns-changes    show namespace transitions\n"
"  -u, --uid-changes   show uid transitions\n"
"  -U, --unicode       use UTF-8 (Unicode) line drawing characters\n"
"  -V, --version       display version information\n"
msgstr ""
"用法：pstree [-a] [-c] [-h|-H PID] [-l] [-n] [-p] [-g] [-u]\n"
"             [-A|-G|-U] [PID|使用者]\n"
"      pstree -V\n"
"顯示樹的行程。\n"
"\n"
"  -a, --arguments     顯示命令列引數\n"
"  -A, --ascii         使用 ASCII 線條繪製字元\n"
"  -c, --compact       不壓縮相同的子樹\n"
"  -h, --highlight-all 標示目前行程和它的原始節點\n"
"  -H PID，\n"
"  --highlight-pid=PID 標示這個行程和它的原始節點\n"
"  -g, --show-pgids    顯示行程群組識別號；意味著 -c\n"
"  -G, --vt100         使用 VT100 線條繪製字元\n"
"  -l, --long          不截斷長列\n"
"  -n, --numeric-sort  依照 PID 排序輸出\n"
"  -p, --show-pids     顯示 PIDs；隱含 -c\n"
"  -s, --show-parents  顯示已選行程的上層\n"
"  -u, --uid-changes   顯示 uid 轉換\n"
"  -U, --unicode       使用 UTF-8 (萬國碼) 線條繪製字元\n"
"  -V, --version       顯示版本資訊\n"

#: src/pstree.c:1016
#, c-format
msgid "  -Z     show         SELinux security contexts\n"
msgstr "  -Z     顯示         SELinux 安全語境\n"

#: src/pstree.c:1018
#, c-format
msgid ""
"  PID    start at this PID; default is 1 (init)\n"
"  USER   show only trees rooted at processes of this user\n"
"\n"
msgstr ""
"  PID    從這個 PID 開始；預設是 1 (init)\n"
"  使用者  只顯示源自此使用者行程的樹狀結構\n"
"\n"

#: src/pstree.c:1025
#, c-format
msgid "pstree (PSmisc) %s\n"
msgstr "pstree (PSmisc) %s\n"

#: src/pstree.c:1028
#, c-format
msgid ""
"Copyright (C) 1993-2009 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"著作權©1993-2009 Werner Almesberger 和 Craig Small\n"
"\n"

#: src/pstree.c:1142
#, c-format
msgid "TERM is not set\n"
msgstr "TERM 尚未設定\n"

#: src/pstree.c:1146
#, c-format
msgid "Can't get terminal capabilities\n"
msgstr "無法取得終端機功能\n"

#: src/pstree.c:1164
#, c-format
msgid "procfs file for %s namespace not available\n"
msgstr ""

#: src/pstree.c:1211
#, c-format
msgid "No such user name: %s\n"
msgstr "無此類使用者名稱：%s\n"

#: src/pstree.c:1237
#, c-format
msgid "No processes found.\n"
msgstr "找不到任何行程。\n"

#: src/pstree.c:1243
#, c-format
msgid "Press return to close\n"
msgstr "按下輸入鍵以關閉\n"

#: src/signals.c:84
#, c-format
msgid "%s: unknown signal; %s -l lists signals.\n"
msgstr "%s: 不明的信號；%s -l 列出信號。\n"
