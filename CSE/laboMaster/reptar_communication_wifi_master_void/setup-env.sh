#TOOLCHAIN_DIR="/opt/gcc-linaro-5.1-2015.08-x86_64_arm-linux-gnueabihf"
TOOLCHAIN_DIR="/opt/linaro-arm-linux-gnueabihf"

#------------------- DO NOT EDIT BELOW THIS LINE -------------------------------

SCRIPT_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"

# External toolchain is provided. Sanity check
if [ ! -d "$TOOLCHAIN_DIR" ]
then
	echo "External toolchain directory provided ($TOOLCHAIN_DIR) but does not exist! Exiting..."
	exit -1
fi

TOOLCHAIN_BIN="$TOOLCHAIN_DIR/bin"

# Avoid adding to PATH if already in PATH
if [[ ":$PATH:" != *":$TOOLCHAIN_BIN:"* ]]
then
	# Add toolchain to PATH
	export PATH="$TOOLCHAIN_BIN:$PATH"
fi



