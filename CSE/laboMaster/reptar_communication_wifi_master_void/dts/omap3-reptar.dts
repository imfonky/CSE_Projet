/*
 * Device Tree Source for REDS Reptar Board
 *
 * Copyright (C) 2016 Florian Vaussard <florian.vaussard@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "omap3-var-som.dtsi"

#include <dt-bindings/input/input.h>

#define DSS_USE_LCD
//#define DSS_USE_HDMI

#if defined(DSS_USE_LCD) && defined(DSS_USE_HDMI)
	#error "You cannot use both the LCD and HDMI port at the same time"
#endif

/ {
	model = "REDS Reptar";
	compatible = "reds,omap3-reptar", "variscite,omap3-var-som", "ti,omap36xx", "ti,omap3";

	leds {
		pinctrl-names = "default";
		pinctrl-0 = <&leds_pins>;
		compatible = "gpio-leds";

		/* D2 */
		cpu_led0 {
			label = "reptar:green:cpu_led0";
			gpios = <&gpio5 15 GPIO_ACTIVE_HIGH>;
			linux,default-trigger = "heartbeat";
		};

		/* D3 */
		cpu_led1 {
			label = "reptar:green:cpu_led1";
			gpios = <&gpio5 13 GPIO_ACTIVE_HIGH>;
		};

		/* D11: connected to SoC _and_ SP6 */
		cpu_led2 {
			label = "reptar:green:cpu_led2";
			gpios = <&gpio4 5 GPIO_ACTIVE_HIGH>;
		};

		/* D14: connected to SoC _and_ SP3 + SP6 */
		cpu_led3 {
			label = "reptar:green:cpu_led3";
			gpios = <&gpio4 6 GPIO_ACTIVE_HIGH>;
		};

		/* D18 */
		fpga_led0 {
			label = "reptar:green:fpga_led0";
			gpios = <&gpio4 14 GPIO_ACTIVE_HIGH>;
		};

		/* D19 */
		fpga_led1 {
			label = "reptar:green:fpga_led1";
			gpios = <&gpio4 13 GPIO_ACTIVE_HIGH>;
		};

		/* D20 */
		fpga_led2 {
			label = "reptar:green:fpga_led2";
			gpios = <&gpio3 31 GPIO_ACTIVE_HIGH>;
		};
	};

	gpio-keys {
		compatible = "gpio-keys";
		pinctrl-names = "default";
		pinctrl-0 = <&button_pins>;

		/* SW1 */
		cpu_button0 {
			label = "cpu_sw0";
			linux,code = <KEY_0>;
			gpios = <&gpio5 12 GPIO_ACTIVE_HIGH>;
			gpio-key,wakeup;
		};

		/* SW2 */
		cpu_button1 {
			label = "cpu_sw1";
			linux,code = <KEY_1>;
			gpios = <&gpio5 14 GPIO_ACTIVE_HIGH>;
			gpio-key,wakeup;
		};

		/* SW3: connected to SoC _and_ SP6 */
		cpu_button2 {
			label = "cpu_sw2";
			linux,code = <KEY_2>;
			gpios = <&gpio6 7 GPIO_ACTIVE_HIGH>;
			gpio-key,wakeup;
		};

		/* SW4: connected to SoC _and_ SP6 */
		cpu_button3 {
			label = "cpu_sw3";
			linux,code = <KEY_3>;
			gpios = <&gpio4 1 GPIO_ACTIVE_HIGH>;
			gpio-key,wakeup;
		};

		/* SW5: connected to SoC _and_ SP6 */
		cpu_button4 {
			label = "cpu_sw4";
			linux,code = <KEY_4>;
			gpios = <&gpio4 30 GPIO_ACTIVE_HIGH>;
			gpio-key,wakeup;
		};

		/* SW9 */
		fpga_button0 {
			label = "fpga_sw1";
			linux,code = <KEY_D>;
			gpios = <&gpio4 10 GPIO_ACTIVE_HIGH>;
			gpio-key,wakeup;
		};

		/* SW10 */
		fpga_button1 {
			label = "fpga_sw2";
			linux,code = <KEY_C>;
			gpios = <&gpio4 11 GPIO_ACTIVE_HIGH>;
			gpio-key,wakeup;
		};

		/* SW11 */
		fpga_button2 {
			label = "fpga_sw3";
			linux,code = <KEY_B>;
			gpios = <&gpio4 4 GPIO_ACTIVE_HIGH>;
			gpio-key,wakeup;
		};

		/* SW12 */
		fpga_button3 {
			label = "fpga_sw4";
			linux,code = <KEY_A>;
			gpios = <&gpio4 3 GPIO_ACTIVE_HIGH>;
			gpio-key,wakeup;
		};
	};

	wl12xx_vmmc2: wl12xx_vmmc2 {
		compatible = "regulator-fixed";
		regulator-name = "vwl1271";
		pinctrl-names = "default";
		pinctrl-0 = <&wl12xx_gpio>;
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		gpio = <&gpio6 18 GPIO_ACTIVE_HIGH>; /* gpio178 */
		startup-delay-us = <20000>;
		enable-active-high;
	};

	sound {
		compatible = "ti,omap-twl4030";
		ti,model = "reptar_snd";
		ti,mcbsp = <&mcbsp2>;
	};

	adc_vref: adc_vref {
	compatible = "regulator-fixed";
	regulator-name = "vref-adc-supply";
	regulator-min-microvolt = <2500000>;
	regulator-max-microvolt = <2500000>;
	regulator-boot-on;
	};

	dac_supply: dac_vref {
	compatible = "regulator-fixed";
	regulator-name = "vref-dac-supply";
	regulator-min-microvolt = <3300000>;
	regulator-max-microvolt = <3300000>;
	regulator-boot-on;
	};

#if defined(DSS_USE_HDMI)
	aliases {
		display0 = &dvi0;
	};

	tfp410: encoder@0 {
		pinctrl-names = "default";
		pinctrl-0 = <&tfp410_pins>;
		powerdown-gpios = <&gpio1 26 GPIO_ACTIVE_LOW>; /* gpio26 */
		compatible = "ti,tfp410";

		ports {
			#address-cells = <1>;
			#size-cells = <0>;

			port@0 {
				reg = <0>;

				tfp410_in: endpoint@0 {
					remote-endpoint = <&dpi_out>;
				};
			};

			port@1 {
				reg = <1>;

				tfp410_out: endpoint@0 {
					remote-endpoint = <&dvi_connector_in>;
				};
			};
		};
	};

	dvi0: connector@0 {
		compatible = "dvi-connector";
		label = "dvi";

		digital;
		/* ddc-i2c-bus = <&i2c3>; */

		port {
			dvi_connector_in: endpoint {
				remote-endpoint = <&tfp410_out>;
			};
		};
	};
#elif defined(DSS_USE_LCD)
	aliases {
		display0 = &lcd0;
	};

	lcd0: display@0 {
		compatible = "emerging,etm070001dh6", "panel-dpi";
		label = "lcd";

		port {
			lcd_in: endpoint {
				remote-endpoint = <&dpi_out>;
			};
		};

		panel-timing {
			clock-frequency = <33260000>;
			hactive = <800>;
			vactive = <480>;
			hfront-porch = <10>;
			hback-porch = <10>;
			hsync-len = <128>;
			vback-porch = <11>;
			vfront-porch = <4>;
			vsync-len = <2>;

			hsync-active = <0>;
			vsync-active = <0>;
			de-active = <1>;
			pixelclk-active = <1>;
		};
	};
#endif /* DSS_USE_LCD */
};

&omap3_pmx_core {
	leds_pins: pinmux_leds_pins {
		pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x2172, PIN_OUTPUT | MUX_MODE4)	/* mcbsp3_fsx.gpio_143 (cpu_led0) */
			OMAP3_CORE1_IOPAD(0x216e, PIN_OUTPUT | MUX_MODE4)	/* mcbsp3_dr.gpio_141 (cpu_led1) */
#if 0
			/* check first how are configured the SP3 and SP6 to avoid conflicts */
			OMAP3_CORE1_IOPAD(0x211a, PIN_OUTPUT | MUX_MODE4)	/* cam_d2.gpio_101 (cpu_led2) */
			OMAP3_CORE1_IOPAD(0x211c, PIN_OUTPUT | MUX_MODE4)	/* cam_d3.gpio_102 (cpu_led3) */
#endif
			OMAP3_CORE1_IOPAD(0x212c, PIN_OUTPUT | MUX_MODE4)	/* cam_d11.gpio_110 (fpga_led0) */
			OMAP3_CORE1_IOPAD(0x212a, PIN_OUTPUT | MUX_MODE4)	/* cam_d10.gpio_109 (fpga_led1) */
			OMAP3_CORE1_IOPAD(0x210e, PIN_OUTPUT | MUX_MODE4)	/* cam_vs.gpio_95 (fpga_led2) */
		>;
	};

	audio_pins: pinmux_msbsp2_pins {
			pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x213c, PIN_INPUT | MUX_MODE0)	/* mcbsp2_fsx.gpio_116 */
			OMAP3_CORE1_IOPAD(0x213e, PIN_INPUT | MUX_MODE0)	/* mcbsp2_clkx.gpio_117*/
			OMAP3_CORE1_IOPAD(0x2140, PIN_INPUT | MUX_MODE0)	/* mcbsp2_dr.gpio_118  */
			OMAP3_CORE1_IOPAD(0x2142, PIN_OUTPUT | MUX_MODE0)	/* mcbsp2_dx.gpio_119  */
		>;
	};

	button_pins: pinmux_button_pins {
		pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x216c, PIN_INPUT | MUX_MODE4)	/* mcbsp3_dx.gpio_140 (cpu_sw0) */
			OMAP3_CORE1_IOPAD(0x2170, PIN_INPUT | MUX_MODE4)	/* mcbsp3_clkx.gpio_142 (cpu_sw1) */
			OMAP3_CORE1_IOPAD(0x2130, PIN_INPUT | MUX_MODE4)	/* cam_wen.gpio_167 (cpu_sw2) */
			OMAP3_CORE1_IOPAD(0x2112, PIN_INPUT | MUX_MODE4)	/* cam_pclk.gpio_97 (cpu_sw3) */
			OMAP3_CORE1_IOPAD(0x2132, PIN_INPUT | MUX_MODE4)	/* cam_strobe.gpio_126 (cpu_sw4) */
			OMAP3_CORE1_IOPAD(0x2124, PIN_INPUT | MUX_MODE4)	/* cam_d7.gpio_106 (fpga_sw1) */
			OMAP3_CORE1_IOPAD(0x2126, PIN_INPUT | MUX_MODE4)	/* cam_d8.gpio_107 (fpga_sw2) */
			OMAP3_CORE1_IOPAD(0x2118, PIN_INPUT | MUX_MODE4)	/* cam_d1.gpio_100 (fpga_sw3) */
			OMAP3_CORE1_IOPAD(0x2116, PIN_INPUT | MUX_MODE4)	/* cam_d0.gpio_99 (fpga_sw4) */
		>;
	};

	/* Bluetooth module */
	uart1_pins: pinmux_uart1_pins {
		pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x217c, PIN_OUTPUT | MUX_MODE0)	/* uart1_tx.uart1_tx */
			OMAP3_CORE1_IOPAD(0x217e, PIN_OUTPUT | MUX_MODE0)	/* uart1_rts.uart1_rts */
			OMAP3_CORE1_IOPAD(0x2180, PIN_INPUT | MUX_MODE0)	/* uart1_cts.uart1_cts */
			OMAP3_CORE1_IOPAD(0x2182, PIN_INPUT | MUX_MODE0)	/* uart1_rx.uart1_rx */
		>;
	};

	/* Console */
	uart3_pins: pinmux_uart3_pins {
		pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x219a, PIN_INPUT | MUX_MODE0)	/* uart3_cts.uart3_cts */
			OMAP3_CORE1_IOPAD(0x219c, PIN_OUTPUT | MUX_MODE0)	/* uart3_rts.uart3_rts */
			OMAP3_CORE1_IOPAD(0x219e, PIN_INPUT | MUX_MODE0)	/* uart3_rx.uart3_rx */
			OMAP3_CORE1_IOPAD(0x21a0, PIN_OUTPUT | MUX_MODE0)	/* uart3_tx.uart3_tx */
		>;
	};

	/* SD card */
	mmc1_pins: pinmux_mmc1_pins {
		pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x2144, PIN_INPUT_PULLUP | MUX_MODE0)	/* sdmmc1_clk.sdmmc1_clk */
			OMAP3_CORE1_IOPAD(0x2146, PIN_INPUT_PULLUP | MUX_MODE0)	/* sdmmc1_cmd.sdmmc1_cmd */
			OMAP3_CORE1_IOPAD(0x2148, PIN_INPUT_PULLUP | MUX_MODE0) 	/* sdmmc1_dat0.sdmmc1_dat0 */
			OMAP3_CORE1_IOPAD(0x214a, PIN_INPUT_PULLUP | MUX_MODE0)	/* sdmmc1_dat1.sdmmc1_dat1 */
			OMAP3_CORE1_IOPAD(0x214c, PIN_INPUT_PULLUP | MUX_MODE0)	/* sdmmc1_dat2.sdmmc1_dat2 */
			OMAP3_CORE1_IOPAD(0x214e, PIN_INPUT_PULLUP | MUX_MODE0)	/* sdmmc1_dat3.sdmmc1_dat3 */
		>;
	};

	/* Wifi module */
	mmc2_pins: pinmux_mmc2_pins {
		pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x2158, PIN_INPUT_PULLUP | MUX_MODE0)	/* sdmmc2_clk.sdmmc2_clk */
			OMAP3_CORE1_IOPAD(0x215a, PIN_INPUT_PULLUP | MUX_MODE0)	/* sdmmc2_cmd.sdmmc2_cmd */
			OMAP3_CORE1_IOPAD(0x215c, PIN_INPUT_PULLUP | MUX_MODE0) 	/* sdmmc2_dat0.sdmmc2_dat0 */
			OMAP3_CORE1_IOPAD(0x215e, PIN_INPUT_PULLUP | MUX_MODE0)	/* sdmmc2_dat1.sdmmc2_dat1 */
			OMAP3_CORE1_IOPAD(0x2160, PIN_INPUT_PULLUP | MUX_MODE0)	/* sdmmc2_dat2.sdmmc2_dat2 */
			OMAP3_CORE1_IOPAD(0x2162, PIN_INPUT_PULLUP | MUX_MODE0)	/* sdmmc2_dat3.sdmmc2_dat3 */
		>;
	};

	wl12xx_gpio: pinmux_wl12xx_gpio {
		pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x2164, PIN_INPUT | MUX_MODE4)	/* sdmmc2_dat4.gpio_136 */
			OMAP3_CORE1_IOPAD(0x21d6, PIN_OUTPUT | MUX_MODE4)	/* mcspi2_clk.gpio_178 */
		>;
	};

	i2c3_pins: pinmux_i2c3_pins {
		pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x21c2, PIN_INPUT | MUX_MODE0)   /* i2c3_scl.i2c3_scl */
			OMAP3_CORE1_IOPAD(0x21c4, PIN_INPUT | MUX_MODE0)   /* i2c3_sda.i2c3_sda */
		>;
	};

	mcspi4_pins: pinmux_mcspi4_pins {
		pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x218c, PIN_INPUT | MUX_MODE1)	/* mcbsp1_clkr.mcspi4_clk */
			OMAP3_CORE1_IOPAD(0x2190, PIN_OUTPUT | MUX_MODE1)	/* mcbsp1_dx.mcspi4_simo */
			OMAP3_CORE1_IOPAD(0x2192, PIN_INPUT | MUX_MODE1)	/* mcbsp1_dr.mcspi4_somi */
			OMAP3_CORE1_IOPAD(0x2196, PIN_OUTPUT | MUX_MODE1)	/* mcbsp1_fsx.mcspi4_cs0 */
		>;
	};

	edt_ft5x06_pins: pinmux_edt_ft5x06_pins {
		pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x21da, PIN_OUTPUT | MUX_MODE4) /* mcspi2_somi.gpio_180 */
			OMAP3_CORE1_IOPAD(0x21dc, PIN_OUTPUT | MUX_MODE4) /* mcspi2_cs0.gpio_181 */
			OMAP3_CORE1_IOPAD(0x21c6, PIN_INPUT | MUX_MODE4)  /* hdq_sio.gpio_170 */
		>;
	};

	dss_dpi_pins: pinmux_dss_dpi_pins {
		pinctrl-single,pins = <
			OMAP3_CORE1_IOPAD(0x20d4, PIN_OUTPUT | MUX_MODE0)	/* dss_pclk.dss_pclk */
			OMAP3_CORE1_IOPAD(0x20d6, PIN_OUTPUT | MUX_MODE0)	/* dss_hsync.dss_hsync */
			OMAP3_CORE1_IOPAD(0x20d8, PIN_OUTPUT | MUX_MODE0)	/* dss_vsync.dss_vsync */
			OMAP3_CORE1_IOPAD(0x20da, PIN_OUTPUT | MUX_MODE0)	/* dss_acbias.dss_acbias */
			OMAP3_CORE1_IOPAD(0x20dc, PIN_OUTPUT | MUX_MODE0)	/* dss_data0.dss_data0 */
			OMAP3_CORE1_IOPAD(0x20de, PIN_OUTPUT | MUX_MODE0)	/* dss_data1.dss_data1 */
			OMAP3_CORE1_IOPAD(0x20e0, PIN_OUTPUT | MUX_MODE0)	/* dss_data2.dss_data2 */
			OMAP3_CORE1_IOPAD(0x20e2, PIN_OUTPUT | MUX_MODE0)	/* dss_data3.dss_data3 */
			OMAP3_CORE1_IOPAD(0x20e4, PIN_OUTPUT | MUX_MODE0)	/* dss_data4.dss_data4 */
			OMAP3_CORE1_IOPAD(0x20e6, PIN_OUTPUT | MUX_MODE0)	/* dss_data5.dss_data5 */
			OMAP3_CORE1_IOPAD(0x20e8, PIN_OUTPUT | MUX_MODE0)	/* dss_data6.dss_data6 */
			OMAP3_CORE1_IOPAD(0x20ea, PIN_OUTPUT | MUX_MODE0)	/* dss_data7.dss_data7 */
			OMAP3_CORE1_IOPAD(0x20ec, PIN_OUTPUT | MUX_MODE0)	/* dss_data8.dss_data8 */
			OMAP3_CORE1_IOPAD(0x20ee, PIN_OUTPUT | MUX_MODE0)	/* dss_data9.dss_data9 */
			OMAP3_CORE1_IOPAD(0x20f0, PIN_OUTPUT | MUX_MODE0)	/* dss_data10.dss_data10 */
			OMAP3_CORE1_IOPAD(0x20f2, PIN_OUTPUT | MUX_MODE0)	/* dss_data11.dss_data11 */
			OMAP3_CORE1_IOPAD(0x20f4, PIN_OUTPUT | MUX_MODE0)	/* dss_data12.dss_data12 */
			OMAP3_CORE1_IOPAD(0x20f6, PIN_OUTPUT | MUX_MODE0)	/* dss_data13.dss_data13 */
			OMAP3_CORE1_IOPAD(0x20f8, PIN_OUTPUT | MUX_MODE0)	/* dss_data14.dss_data14 */
			OMAP3_CORE1_IOPAD(0x20fa, PIN_OUTPUT | MUX_MODE0)	/* dss_data15.dss_data15 */
			OMAP3_CORE1_IOPAD(0x20fc, PIN_OUTPUT | MUX_MODE0)	/* dss_data16.dss_data16 */
			OMAP3_CORE1_IOPAD(0x20fe, PIN_OUTPUT | MUX_MODE0)	/* dss_data17.dss_data17 */
			OMAP3_CORE1_IOPAD(0x2100, PIN_OUTPUT | MUX_MODE0)	/* dss_data18.dss_data18 */
			OMAP3_CORE1_IOPAD(0x2102, PIN_OUTPUT | MUX_MODE0)	/* dss_data19.dss_data19 */
			OMAP3_CORE1_IOPAD(0x2104, PIN_OUTPUT | MUX_MODE0)	/* dss_data20.dss_data20 */
			OMAP3_CORE1_IOPAD(0x2106, PIN_OUTPUT | MUX_MODE0)	/* dss_data21.dss_data21 */
			OMAP3_CORE1_IOPAD(0x2108, PIN_OUTPUT | MUX_MODE0)	/* dss_data22.dss_data22 */
			OMAP3_CORE1_IOPAD(0x210a, PIN_OUTPUT | MUX_MODE0)	/* dss_data23.dss_data23 */
		>;
	};

	tfp410_pins: pinmux_tfp410_pins {
		pinctrl-single,pins = <
			OMAP3630_CORE2_IOPAD(0x25f4, PIN_OUTPUT | MUX_MODE4)	/* hsusb2_dir.gpio_26 */
		>;
	};
};

&mcbsp2 {
	pinctrl-names = "default";
	pinctrl-0 = <&audio_pins>;
	status = "okay";
};

&dss {
	status = "ok";

	pinctrl-names = "default";
	pinctrl-0 = <&dss_dpi_pins>;

	port {
		dpi_out: endpoint {
		#if defined(DSS_USE_HDMI)
			remote-endpoint = <&tfp410_in>;
		#elif defined(DSS_USE_LCD)
			remote-endpoint = <&lcd_in>;
		#endif
			data-lines = <24>;
		};
	};
};

/* Bluetooth */
&uart1 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart1_pins>;
};

/* Console */
&uart3 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart3_pins>;
};

&i2c3 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c3_pins>;
	clock-frequency = <100000>;

	sfh5712: sfh5712@29 {
		compatible = "osram,sfh5712";
		reg = <0x29>;
	};

	polytouch: edt-ft5x06@38 {
		compatible = "edt,edt-ft5406", "edt,edt-ft5x06";
		reg = <0x38>;
		pinctrl-names = "default";
		pinctrl-0 = <&edt_ft5x06_pins>;
		interrupt-parent = <&gpio6>;
		interrupts = <10 0>;
		reset-gpios = <&gpio6 20 1>;
		wake-gpios = <&gpio6 21 0>;
	};

	tmp103: tmp103@71 {
		compatible = "ti,tmp103";
		reg = <0x71>;
	};
};

/* SD card */
&mmc1 {
	pinctrl-names = "default";
	pinctrl-0 = <&mmc1_pins>;
	vmmc-supply = <&vmmc1>;
	bus-width = <4>;
	cd-gpios = <&twl_gpio 0 GPIO_ACTIVE_LOW>;
};

/* Wifi module */
&mmc2 {
	pinctrl-names = "default";
	pinctrl-0 = <&mmc2_pins>;

	vmmc-supply = <&wl12xx_vmmc2>;

	non-removable;
	bus-width = <4>;
	cap-power-off-card;

	#address-cells = <1>;
	#size-cells = <0>;
	wlcore: wlcore@2 {
		compatible = "ti,wl1271";
		reg = <2>;
		interrupt-parent = <&gpio5>;
		interrupts = <8 IRQ_TYPE_LEVEL_HIGH>; /* gpio 136 */
		ref-clock-frequency = <38400000>;
	};
};
