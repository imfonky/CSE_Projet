/*------------------------------------------------------------------------
* But               : fonctions d'affichage pour le LCD de la board REPTAR
*
* Auteurs           : Michel Starkier, Cédric Bardet, Daniel Rossier
* Date              : 16.10.2007
* Version           : 1.0
*
* Fichier           : lcdlib.c
*-----------------------------------------------------------------------------*/

#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <fcntl.h>

#include "lcdlib.h"
#include "lcdfont.h"


//#include <linux/mm.h>		/* PAGE_ALIGN */

unsigned char *fb_mem = 0;
static unsigned int Xn = 7391; /* Initial random seed */

struct fb_var_screeninfo vinfo;
struct fb_fix_screeninfo finfo;

void fb_set_pixel(int y_min,int x_min,int couleur);
int circleYpos(int cXpos, int radius);

long int screensize = 0;

/*
  Initialize the framebuffer, in other words get a virtual address at the first pixel of the LCD
*/
void fb_init(void)
{
        int fbfd = 0;
        int x = 0;

        fbfd = open("/dev/fb0", O_RDWR);
        ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo);
        ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo);
        screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;
        fb_mem = (unsigned char *)mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED,fbfd, 0);
        for ( x = 0; x < screensize; x++ ) {
        	fb_mem[x] = 0x0; //Clear the screen
        }
        //printf("fb_init:xres=%d yres=%d bbp=%d fb_mem=%d\n",vinfo.xres,vinfo.yres,vinfo.bits_per_pixel,fb_mem);
}

/*
  Generate a random 32-bit numbers
*/
unsigned int get_random(void)
{
  static unsigned int Xnp1, a, c, m;

  /* Initializing the random generator */
  a = 13;
  c = 7;
  m = 0xFFFFFFFF;

  Xnp1 = (a * Xn + c) % m;

  Xn = Xnp1;

  return Xn;
}

void fb_set_pixel(int y, int x, int couleur)
{
  //printf("0x%8.8X ",couleur);
  int location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
  if ( location >= screensize || location < 0 )
	  	  return;
  *((unsigned int*)(fb_mem + location)) = couleur;
}

int fb_get_pixel(int y, int x)
{
  //printf("0x%8.8X ",couleur);
  int location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
  if ( location >= screensize || location < 0 )
	  	  return 0;
  return *((unsigned int*)(fb_mem + location));
}

void fb_rect_fill(int y_min, int y_max, int x_min, int x_max, int couleur) {

  int x = 0, y = 0;

  for (y = y_min; y <= y_max; y++)
    for (x = x_min; x <= x_max; x++)
      fb_set_pixel(y, x, couleur);
}



/******************************************/

/* calcule la coordonnee y d'un quart de cercle en fonction de x
   et du rayon => x et y positifs				*/

int mySqrt(int sq)
{
   int an,i,an1;
   an=sq;

   if (sq == 0)
     return 0;
   for (i=0;i<6;i++) {
	 an1=(an+sq/an)/2;
	 an=an1;
   }
   return (an);
}

int circleYpos(int cXpos, int radius)
{
   return mySqrt(radius*radius - cXpos*cXpos);
}

/* remplit un cercle en fonction des coordonn�es du centre, du rayon,
   et de la couleur 							*/
int fb_circle_fill(int yPos,int xPos,int radius,int color){
   int i,j,y;
   if (radius >= LCD_MAX_X/2) radius = LCD_MAX_X/2;
   if (xPos+radius >= LCD_MAX_X) xPos = LCD_MAX_X-1-radius;
   if (yPos+radius >= LCD_MAX_Y) yPos = LCD_MAX_Y-1-radius;
   if (xPos-radius < 0 ) xPos = radius;
   if (yPos-radius < 0 ) yPos = radius;
   for (i=0;i<=radius;i++){
      y = circleYpos( i, radius);             /* calcul pixel p�riph�rie */
      for (j=0;j<=y;j++) {                     /* remplissage */
         fb_set_pixel(yPos+j,xPos+i,color);
         fb_set_pixel(yPos-j,xPos+i,color);
         fb_set_pixel(yPos+j,xPos-i,color);
         fb_set_pixel(yPos-j,xPos-i,color);
      }
   }
 return 0;
}

/* Dessine un cercle en fonction des coordonn�es du centre, du rayon,
   et de la couleur 							*/
int fb_circle(int yPos, int xPos, int radius, int color)
{
	int i, y;
	int a, b, c, d;
	if (radius >= LCD_MAX_X / 2)
		radius = LCD_MAX_X / 2;

	for (i = 0; i <= radius; i++) {
		y = circleYpos(i, radius); /* calcul pixel p�riph�rie */

		a = (yPos > LCD_MAX_Y) ? LCD_MAX_Y : yPos;
		b = (yPos < 0) ? 0 : yPos;
		c = (xPos + i > LCD_MAX_X) ? LCD_MAX_X : xPos + i;
		d = (xPos - i < 0) ? 0 : xPos - i;
		fb_set_pixel(a, c, color);
		fb_set_pixel(b, c, color);
		fb_set_pixel(a, d, color);
		fb_set_pixel(b, d, color);

		a = (yPos + y > LCD_MAX_Y) ? LCD_MAX_Y : yPos + y;
		b = (yPos - y < 0) ? 0 : yPos - y;
		c = (xPos + i > LCD_MAX_X) ? LCD_MAX_X : xPos + i;
		d = (xPos - i < 0) ? 0 : xPos - i;
		fb_set_pixel(a, c, color);
		fb_set_pixel(b, c, color);
		fb_set_pixel(a, d, color);
		fb_set_pixel(b, d, color);
	}
	return 0;
}

/*---------------------------------------------------------------------------*-
   fb_print_char
  -----------------------------------------------------------------------------
   Descriptif: Cette fonction permet d'afficher un caractère en fonction
   	   	   	   de la couleur et de la position choisie par l'utilisateur.
   Entrée    : int color 			: couleur choisie
   	   	   	   int color_fond 		: couleur de fond
   	   	   	   unsigned char car 	: caractère choisi
   	   	   	   int x 				: position en x sur l'afficheur
   	   	   	   int y 				: position en y sur l'afficheur
   Sortie    : Aucune
-*---------------------------------------------------------------------------*/
void fb_print_char(int color, int color_fond, unsigned char car, int x, int y)
{
	unsigned char ligne;
	unsigned int j;
	unsigned int i;
	unsigned int tab[] = { 1, 2, 4, 8, 16, 32, 64, 128};

	// Permet de parcourir les 8 lignes
	for (i = 0; i < 8 ; i++)
	{
		// Permet de récuperer la valeur codé sur 8 bits
		// pour le caractère.
		ligne = fb_font_data[car-FIRST_CHAR][i];

		// Permet de parcourir les 8 colonnes
		for (j = 0 ; j < 8; j++)
		{
			// Permet de déterminer si on doit allumer le pixel cocnerné
			if (ligne & tab[7-j])
				fb_set_pixel(y+i,x+j,color);
			else
				fb_set_pixel(y+i,x+j,color_fond);
		}
	}
}

/*---------------------------------------------------------------------------*-
   fb_print_string
  -----------------------------------------------------------------------------
   Descriptif: Cette fonction permet d'afficher un texte (une chaîne de caractère)
   	   	   	   sur l'afficheur LCD. La fonction gère automatiquement le retour
   	   	   	   à la ligne si le texte est trop long ou si le caractère '\n' est
   	   	   	   présent.

   Remarque  : Il faut faire attention car aucun contrôle de dépassement du
    		   texte en bas de l'écran est effectué

   Entrée    : int color 					: couleur choisie.
   	   	   	   int color_fond 				: couleur de fond
      	   	   unsigned char *ptr_texte 	: texte à afficher
   	   	   	   int x 						: position de départ en x sur l'afficheur
   	   	   	   int y 						: position de déaprt en y sur l'afficheur
   Sortie    : Aucune
-*---------------------------------------------------------------------------*/
void fb_print_string(int color, int color_fond, unsigned char *ptr_texte, int x, int y)
{
	unsigned char pos_car = 0;			// Position du caractère sur l'afficheur.
	unsigned char pos_car_ligne = 0;	// Position d'un caractère au sein d'une ligne de l'afficheur.

	// Tant que la chaîne de caractère n'a pas été complétement parcourue
	while(*(pos_car + ptr_texte) != '\0')
	{
		// Si le caractère de retour à la ligne est présent
		if(*(pos_car + ptr_texte) == '\n')
		{
			pos_car_ligne = 0;
			x = 0;
			y += 10;
			pos_car++;
			continue;
		}
		// Si le nombre maximum de caractère est présent sur la ligne (100 caractères)?
		else if(((pos_car_ligne % 100) == 0) && pos_car_ligne)
		{
			pos_car_ligne = 0;
			x = 0;
			y += 10;
		}

		// Affichage du caractère sur l'afficheur LCD
		fb_print_char(color,color_fond, *(ptr_texte + pos_car), x + (8*pos_car_ligne), y);
		pos_car++;
		pos_car_ligne++;
	}
}

//------------------------------------------------------------------------------
void clear_screen(void) {
    int x;
    
    for ( x = 0; x < screensize; x++ ) {
        fb_mem[x] = 0x0; //Clear the screen
    }
}
