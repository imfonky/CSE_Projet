/*
 * display.h
 *
 *  Created on : May 28, 2018
 *      Authors: Denise Gemesio
 * 				 Luca Sivillica
 * 				 Gaetan Othenin-Girard
 * 				 Pierre-Benjamin Monaco
 *
 *  Description: Functions of display for errors and solution reception
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include "commandes.h"

cse_request_t* request_init(void);
cse_response_t* response_init(void);
void printRequestError(cse_request_t * errorRequest);
void printResponseError(cse_response_t * errorResponse);
void printErrorNotFound(void);
void printErrorRequestNotValid(void);

#endif /* DISPLAY_H_ */
