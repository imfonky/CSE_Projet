#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

CSE_DEV="/dev/cse_drivers"

CSE_DRIVERS="/home/cse_labo/cse_drivers"

echo
echo Creation and insertion of the drivers...
echo ========================================
echo

mkdir "$CSE_DEV" # Création du répertoir pour nos drivers

# Création de l'entrée pour le driver gérant les périph de la fpga SP6
mknod "$CSE_DEV/led" c 126 0			#LEDs
mknod "$CSE_DEV/lcd_4x20" c 126 1		#lcd_4x20
mknod "$CSE_DEV/7seg1" c 126 2			#7seg1
mknod "$CSE_DEV/7seg2" c 126 3			#7seg2
mknod "$CSE_DEV/7seg3" c 126 4			#7seg3
mknod "$CSE_DEV/buzz" c 126 5			#buzz
mknod "$CSE_DEV/switch" c 126 6			#Switch
mknod "$CSE_DEV/encoder_count" c 126 7	#encoder_count
mknod "$CSE_DEV/encoder_dir" c 126 8	#encoder_dir

echo      ----- Insertion driver FPGA_SP6 success... -----
echo

# Création de l'entrée pour le driver gérant l'écran tactile 
mknod "$CSE_DEV/touch_screen" c 127 0
echo      ----- Insertion driver Touch Sreen success... -----
echo

echo Folder CSE_drivers :
echo ============================
echo

ls -l "$CSE_DEV/"

insmod "$CSE_DRIVERS/fpga.ko"
insmod "$CSE_DRIVERS/touch-screen.ko"


#Use Access Point 
# variables
echo      ----- Connexion a un access point -----
export REPTAR_NO=15
export SSID="OpenWrt"

echo      ----- Creation interface... -----
# interface
ip link set wlan0 down
ip link set wlan0 address 00:11:22:33:44:${REPTAR_NO}
ip link set wlan0 up
iw dev wlan0 connect ${SSID}

echo      ----- Setting adresse IP -----
# ip address
dhcpcd wlan0

echo      ----- Routing Table -----
# routing table
ip route flush table main
ip route add default dev wlan0
