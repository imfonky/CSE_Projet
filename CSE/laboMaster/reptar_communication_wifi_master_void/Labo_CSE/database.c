#include "database.h"
#include <stdio.h>
#include <dirent.h>
#include <error.h>
#include <errno.h>

int database_init() {

	DIR *dir = opendir(DIR_PATH);
	if (dir) {
		closedir(dir);
	} else if (ENOENT == errno) {
		return mkdir(DIR_PATH, 0700);
	} else {
		printf("Le dossier \"%s\" n'as pas pu être créé\n", DIR_PATH);
		return errno;
	}
	return 0;
}

int database_get(int item, cse_response_t* response) {
	char file_name[CSE_PROCEDURE_MAX_SIZE];
	sprintf(file_name, "%s%u.dat", DIR_PATH, item);
	FILE *file = fopen(file_name, "r");

	response->error = item;

	printf("Struct : \n\terror : %d\n\t", response->error);

	if (file) {
		response->who_know = 0;
		fread(response->procedure, CSE_PROCEDURE_MAX_SIZE, 1, file);

		printf("procedure : \n%s\n", response->procedure);

		fclose(file);

		return 0;
	} else {
		response->who_know = -1;
		sprintf(response->procedure, "Unkown");
		printf("Le fichier \"%s\" n'as pas pu être lu\n", file_name);
		return errno;
	}
}
