/*
 * display.c
 *
 *  Created on : May 28, 2018
 *      Authors: Denise Gemesio
 * 				 Luca Sivillica
 * 				 Gaetan Othenin-Girard
 * 				 Pierre-Benjamin Monaco
 *
 *  Description: Functions of display for errors and solution reception
 */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <time.h>

#include "display.h"
#include "lcdlib.h"
#include "lcdfont.h"
#include "drivers_toolbox.h"

#define WHITE (RED(255) | GREEN(255) | BLUE(255))
#define BLACK (RED(0) | GREEN(0) | BLUE(0))

typedef unsigned char uchar;

cse_request_t* request_init(void) {

	cse_request_t * request;

	request = malloc(sizeof(cse_request_t));

	request->device = 1;
	request->error = 2;

	return request;
}

cse_response_t* response_init(void) {

	cse_response_t * response;

	response = malloc(sizeof(cse_response_t));

	response->device = 3;
	response->error = 4;
	response->who_know = 5;
	memset(response->procedure,0,CSE_PROCEDURE_MAX_SIZE);
	//response->procedure = malloc(sizeof(char) * CSE_PROCEDURE_MAX_SIZE);

	return response;
}

// Print a request
void printRequestError(cse_request_t * errorRequest) {

	char request[CSE_PROCEDURE_MAX_SIZE];
	memset(request,0,CSE_PROCEDURE_MAX_SIZE);

	sprintf(request,
			"Device %d has the following error : ERROR %d. Wait for solution...\n",
			errorRequest->device, errorRequest->error);

	fb_print_string(WHITE, BLACK, (uchar*)request, 0, 0);

}

// Print a response
void printResponseError(cse_response_t * errorResponse) {

	char response[CSE_PROCEDURE_MAX_SIZE];
	memset(response,0,CSE_PROCEDURE_MAX_SIZE);

	sprintf(response,
			"Device %d has the solution for ERROR %d from Device %d. Please use the following procedure :\n%s",
			errorResponse->device, errorResponse->error, errorResponse->who_know, errorResponse->procedure);

	fb_print_string(WHITE, BLACK, (uchar*)response, 0, 2 * FONT_HEIGHT);

}

// Print an error: the procedure has not been found
void printErrorNotFound(void) {
	char error[100] = {'\0'};

	sprintf(error, "No procedure has been found!\n");

	fb_print_string(WHITE, BLACK, (uchar*)error, 0, 2 * FONT_HEIGHT);
}

// Print an error: the request is not valid
void printErrorRequestNotValid(void) {
	char error[100] = {'\0'};

	sprintf(error, "The request is not valid!\n");

	fb_print_string(WHITE, BLACK, (uchar*)error, 0, 2 * FONT_HEIGHT);
}
