/*
+--------------------------------------------------+

                       IEM
                  CASSE-BRIQUES

                 Stéphane COLLET
                   Kevin HENZER
                Patrick JAQUENOUD
                Florian SEGGINGER

+--------------------------------------------------+

                driver_defines.h

Contient tous les define relatifs à l'utilisation
des drivers custom. Comme par ex. les numeros
pour les appels de type ioctl.

+--------------------------------------------------+

   Créé par: Patrick
   Date:     18.01.2013

+--------------------------------------------------+
*/
#ifndef DRIVER_DEFINES_H_
#define DRIVER_DEFINES_H_

#define WR_LEDS 0
#define RD_LEDS 1


#define RD_SW 10
#define RD_TOUCHSCREEN 11


/* ---------- LCD 4x24 ---------- */
#define WR_LCD_4x20	 		0x03
#define SET_DDRAM_CMD	 	0x480U
#define WRITE_TO_RAM_CMD	0x600U
#define LINE1				0x0
#define LINE2				0x1
#define LINE3				0x2
#define LINE4				0x3

#endif /* DRIVER_DEFINES_H_ */
