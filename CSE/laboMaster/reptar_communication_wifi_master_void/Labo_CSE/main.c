/*
+--------------------------------------------------+

                     LABO CSE
              COMMUNICATION MASTER
+--------------------------------------------------+

                      main.c

Fichier principal du programme
+--------------------------------------------------+

   Créé par: 
   Date:  

+--------------------------------------------------+
*/

#include <sys/mman.h>
#include <stdlib.h>

//Requis pour utiliser le driver
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <fcntl.h>

#include "database.h"
#include "display.h"
#include "lcdlib.h"
#include "commandes.h"
#include "server.h"


#define MAIN_DEBUG 1

#define MALLOC_CHECK_ 3


void init(){
	fb_init();
}


//Main
int main( int argc, char * argv[] ) {
	init();
	start_unicast_server();
}
