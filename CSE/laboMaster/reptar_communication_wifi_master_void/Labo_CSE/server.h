/*
 * server.c
 *
 *  Created on : May 28, 2018
 *      Authors: Denise Gemesio
 * 				 Luca Sivillica
 * 				 Gaetan Othenin-Girard
 * 				 Pierre-Benjamin Monaco
 *
 *  Description: Two socket servers (one unicast and one broadcast).
 */

#ifndef SERVER_DEFINES_H_
#define SERVER_DEFINES_H_

 
#include "commandes.h"
 
#define MAX_CLIENTS 256

	int start_unicast_server();
	int broadcast_request(cse_request_t* packed_request);

#endif /* SERVER_DEFINES_H_ */
