
/* ---------- Commande possible pour appelle à "ioctl" ---------- */

/*Commande pour les périphérique de la FPGA SP6*/
#define READ_CMD		0
#define WRITE_CMD		1

/*Commande pour le touch screen*/
#define RD_TOUCHSCREEN_CMD 	0

/* ---------- Afficheur LCD 4x20 ---------- */
#define SET_DDRAM	 		0x80U
#define CURSOR_HOME	 		0x0U
#define START			 	0x400U
#define WRITE_TO_RAM		0x200U
#define CLEAR_DISPLAY		0x1U
#define CLEAR_LINE			0x20U
#define LINE1				0x0
#define LINE2				0x1
#define LINE3				0x2
#define LINE4				0x3

/* ---------- Buzz ---------- */
#define	 BUZZ_EN			1
#define	 BUZZ_OFF			0
#define	 BUZZ_HIGH_FREQ		0x2
#define	 BUZZ_LOW_FREQ		0x4

/* ---------- Afficheurs 7 segments ---------- */
#define	 _7SEG1_REG		0x10
#define	 _7SEG2_REG		0x2e
#define	 _7SEG3_REG		0x30
#define	 _7SEG_ALL_OFF	1000
#define	 WR_7_SEG		5
#define MAX_TO_DISPLAY 999

/* ------------ Leds ------------- */
#define LEDS_OFF		0b00000000
#define LED_1 			0b00000001
#define LED_2 			0b00000010
#define LED_3 			0b00000100
#define LED_4 			0b00001000
#define LED_5 			0b00010000
#define LED_6 			0b00100000

/* ------------ Switch ------------- */
#define SW_1 			0b00000001
#define SW_2 			0b00000010
#define SW_3 			0b00000100
#define SW_4 			0b00001000
#define SW_5 			0b00010000
#define SW_6 			0b00100000
#define SW_7 			0b01000000
#define SW_8 			0b10000000

/* ---------- Encodeur Incrémental --------- */
#define ROT_TO_LEFT 			64
#define ROT_TO_RIGHT 			128

int lcd4x20_print(int fd,int line, const char *str);
void lcd4x20_clearDisplay(int fd);
void lcd4x20_clearLine(int fd,int line);
int getLineAddr(int line);
int print_7SEG_number_3digits(int fd_7seg1,int fd_7seg2,int fd_7seg3, int number);
int print_LED(int fd_Led, int Led_ID, int Led_State);
