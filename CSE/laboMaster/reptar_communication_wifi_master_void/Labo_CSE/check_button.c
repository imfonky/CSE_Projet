/**
 *
 *
 *
 */

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include "drivers_toolbox.h"
#include "check_button.h"

static void wait_release_sw(int sw, int sw_nb) {
    while (sw & sw_nb);
}

int write_button(char towrite){
	int fout;
	if((fout = open(FLAG_FILE, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR)) < 0){	
		perror("open error");
		printf("Error opening file: %s\nFd : %d\n", strerror( errno ),fout);
		return fout;
	}
		
	write(fout,&towrite,1);
	close(fout);
		
	return 0;

}

int main(void) {
    int fd_switch;
    int switch_state = 0;

    /* open the node of the switches */
    fd_switch = open(PATH_NODE, O_RDONLY);
    if (fd_switch == -1) {
        perror("Cannot open the switch node " PATH_NODE "\n");
        return FAILURE;
    }

    /* check the switch state by using the polling */
    while (TRUE) {
        /* read the switch's button */
        ioctl(fd_switch, READ_CMD, &switch_state);

        /* check the switch 6 */
        if (switch_state & SW_6) {
            printf("Switch 6 pressed!\n");
            write_button('0');

            /* wait that the switch is released */
            while (switch_state & SW_6) {
                /* read the switch's button */
                ioctl(fd_switch, READ_CMD, &switch_state);
            }

        /* check the switch 7 */
        } else if (switch_state & SW_7) {
            printf("Switch 7 pressed!\n");
            write_button('1');

            /* wait that the switch is released */
            while (switch_state & SW_7) {
                /* read the switch's button */
                ioctl(fd_switch, READ_CMD, &switch_state);
            }

        /* check the switch 8 */
        } else if (switch_state & SW_8) {
            printf("Switch 8 pressed!\n");
            write_button('2');

            /* wait that the switch is released */
            while (switch_state & SW_8) {
                /* read the switch's button */
                ioctl(fd_switch, READ_CMD, &switch_state);
            }
        }
    }
}
