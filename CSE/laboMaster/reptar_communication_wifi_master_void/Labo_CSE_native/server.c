/*
 * server.c
 *
 *  Created on : May 28, 2018.025659
 *      Authors: Denise-Magdalena Gemesio
 * 				 Luca-Salvatore Sivillica
 * 				 Gaetan-Gérard Othenin-Girard
 * 				 Jean-Pierre-Benjamin Monaco
 *
 *  Description: Two socket servers (one unicast and one broadcast).
 */

#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h> /* memset() */
#include <sys/time.h> /* select() */ 
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>


#include "server.h"
#include "database.h"

char mode = 1; // 0 for normal 1 for bypass, 2 for auto

int socket_desc, client_sock, c, read_size;
cse_request_t orig_request;
cse_request_t unpacked_request;
cse_response_t sent_response;
cse_response_t unpacked_response;
char unknown_packet[sizeof(cse_response_t)];
int waiting_socks[MAX_CLIENTS];
struct sockaddr_in server, client;


void bypass_mode(){	
	/* Saves the waiting device socket */
	waiting_socks[unpacked_request.device] = client_sock;

	/* Broadcasts request to all slaves */
	broadcast_request(&orig_request);
}

 int start_unicast_server() {


	 //Create socket
	 socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	 if (socket_desc == -1) {
		 printf("Could not create socket");
	 }
	 puts("Socket created");

	 //Prepare the sockaddr_in structure
	 server.sin_family = AF_INET;
	 server.sin_addr.s_addr = INADDR_ANY;
	 server.sin_port = htons(TCP_PORT);

	 //Bind
	 if (bind(socket_desc, (struct sockaddr *) &server, sizeof(server)) < 0) {
		 //print the error message
		 perror("bind failed. Error");
		 return 1;
	 }
	 puts("bind done");

	 //Listen
	 listen(socket_desc, 3);

	 //Accept and incoming connection
	 puts("Waiting for incoming connections...");
	 c = sizeof(struct sockaddr_in);

	 //accept connection from an incoming client
	 while ((client_sock = accept(socket_desc, (struct sockaddr *) &client, (socklen_t *) &c)) > 0) {
		 		 
		 if (client_sock < 0) {
			 perror("accept failed");
			 return 1;
		 }
		 printf("Connection accepted\n");

		 //Receive an unkown message from client
		 if ((read_size = recv(client_sock, unknown_packet, sizeof(cse_response_t), 0)) > 0) {
			 /* Gets data as request */
			 memcpy(&orig_request, unknown_packet, sizeof(cse_request_t));
			 unpacked_request = CSE_UNPACK_REQUEST(orig_request);

			 /* Checks if request is in fact response */
			 if (unpacked_request.magic == CSE_RESPONSE_MAGIC) {
				 printf("Response recieved\n");

				 /* Gets data as response */
				 memcpy(&sent_response, unknown_packet, sizeof(cse_response_t));

				 /* Sends response to waiting client */
				 write(waiting_socks[unpacked_request.device], &sent_response, sizeof(cse_response_t));

				 /* Unpacks and display response */
				 unpacked_response = CSE_UNPACK_RESPONSE(sent_response);
				 strcpy(unpacked_response.procedure, sent_response.procedure);

			 }
			 /* Checks if request is not a known command */
			 if (unpacked_request.magic != CSE_REQUEST_MAGIC) {
				 perror("Bad message sent\n");
			 } else {

				 /* reset response procedure and sets device number */
				 memset(unpacked_response.procedure, 0, CSE_PROCEDURE_MAX_SIZE);
				 unpacked_response.device = unpacked_request.device;

				 /* Mode selection, 0 for normal, 1 for bypass */
				 switch (mode) {
					 case '0':
					 case '2':
						 /* Normal mode : checks into database for responses */
						 if (database_get(unpacked_request.error, &unpacked_response) == 0) {


						 } else {

							 /* Super mode */
							 if(mode == '2'){
								 bypass_mode();
								 break;
							 }
							 else{
								 perror("Resolution pas trouvée\n");
							} 
						 }

						 /* Sends response (even if unknown) */
						 sent_response = CSE_PACK_RESPONSE(unpacked_response);
						 strcpy(sent_response.procedure, unpacked_response.procedure);
						 write(client_sock, &sent_response, sizeof(cse_response_t));

						 break;
					 case '1':
					 default:
						 /* Bypass mode : asks slaves for responses */
						 bypass_mode();
						 break;
				 }
			 }
		 } else {
			 perror("read failed\n");
		 }

		 if (read_size == 0) {
			 puts("Client disconnected");
			 fflush(stdout);
		 } else if (read_size == -1) {
			 perror("recv failed\n");
		 }
		 puts("Waiting for incoming connections...");
	 }

	 return 0;
 }

int broadcast_request(cse_request_t* packed_request) {

	int sd;
	struct sockaddr_in cliAddr, remoteServAddr;
	struct hostent *h;
	int broadcast = 1;

	/* Gets IP address in a readable way */
	h = gethostbyname(SLAVE_IP);
	if (h == NULL) {
		printf("unknown host '%s' \n", SLAVE_IP);
		exit(1);
	}

	printf("sending data to '%s' (IP : %s) \n", h->h_name,
		   inet_ntoa(*(struct in_addr *) h->h_addr_list[0]));

	/* Sets broadcast sending address */
	remoteServAddr.sin_family = h->h_addrtype;
	memcpy((char *) &remoteServAddr.sin_addr.s_addr,
		   h->h_addr_list[0], h->h_length);
	remoteServAddr.sin_port = htons(UDP_PORT);

	/* Creates UDP socket */
	sd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sd < 0) {
		printf("cannot open socket \n");
		exit(1);
	}

	printf("socket created \n");

	/* Sets socket option */
	if (setsockopt(sd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof broadcast) == -1) {
		perror("setsockopt (SO_BROADCAST)");
		exit(1);
	}

	printf("socket option sat \n");

	/* Sets allowed UDP ports */
	cliAddr.sin_family = AF_INET;
	cliAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	cliAddr.sin_port = htons(0);

	/* Binds UDP port */
	if (bind(sd, (struct sockaddr *) &cliAddr, sizeof(cliAddr)) < 0) {
		printf("cannot bind port\n");
		exit(1);
	}
	printf("socket bound \n");

	/* Broadcasts request */
	if (sendto(sd, packed_request, sizeof(cse_request_t), 0,
			   (struct sockaddr *) &remoteServAddr,
			   sizeof(remoteServAddr)) < 0) {
		printf("cannot send data \n");
		close(sd);
		exit(1);
	}

	printf("data sent \n");


	return 0;

}
