#include <sys/mman.h>
#include <stdlib.h>

//Requis pour utiliser le driver
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <fcntl.h>

#include "database.h"
#include "commandes.h"
#include "server.h"


//Main
int main( int argc, char * argv[] ) {
	start_unicast_server();
}
