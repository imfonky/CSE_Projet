/**
 * @copyright HEIG-VD - Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud,
 *            REDS    - Reconfigurable Embedded Digital Systems
 * 
 * @file      commandes.h
 * @author    Baeriswyl Julien <julien.baeriswyl@heig-vd.ch>
 * @since     2018-05-28
 * @version   0.1
 * @brief     Superb misunderstanding between title and content.
 */
#ifndef COMMANDES_DEFINES_H_
#define COMMANDES_DEFINES_H_

#include <stddef.h>
#include <inttypes.h>
#include <string.h>
#include <arpa/inet.h>

/* -------------------------------------------------------------------------- *
 * -------------------------- Protocol declaration -------------------------- *
 * -------------------------------------------------------------------------- */

typedef int             cse_msg_field_t;
typedef cse_msg_field_t cse_prot_magic_t;
typedef cse_msg_field_t cse_error_t;
typedef cse_msg_field_t cse_device_t;
typedef char cse_procedure_t;

typedef struct
{
    cse_prot_magic_t magic;
    cse_error_t      error;
    cse_device_t     device;
} cse_request_t;

#define CSE_PROCEDURE_MAX_SIZE   1024
typedef struct
{
    cse_prot_magic_t magic;
    cse_error_t      error;
    cse_device_t     device,
                     who_know;
    cse_procedure_t  procedure[CSE_PROCEDURE_MAX_SIZE];
} cse_response_t;

#define CSE_REQUEST_MAGIC  0xDECEA5ED
#define CSE_RESPONSE_MAGIC 0xDA7ABA5E
#define CSE_UNKNOWN_DEVICE -1
#define CSE_UNKNOWN_ERROR  -1

#define CSE_REQUEST_HEADER_SIZE  sizeof(cse_request_t)
#define CSE_REQUEST_MAX_SIZE     CSE_REQUEST_HEADER_SIZE
#define CSE_RESPONSE_HEADER_SIZE offsetof(cse_response_t, procedure)
#define CSE_RESPONSE_MAX_SIZE    (CSE_RESPONSE_HEADER_SIZE + CSE_PROCEDURE_MAX_SIZE)

#define CSE_ISINVALID_REQUEST(r)  ((r).magic != CSE_REQUEST_MAGIC  || (r).error < 0 || (r).device < 0)
#define CSE_ISINVALID_RESPONSE(r) ((r).magic != CSE_RESPONSE_MAGIC || (r).error < 0 || (r).device < 0 || (r).procedure == NULL)

#define CSE_PACK_REQUEST(request)     \
(cse_request_t) {                     \
    .magic  = htonl((request).magic), \
    .error  = htonl((request).error), \
    .device = htonl((request).device) \
}

#define CSE_PACK_RESPONSE(response)          \
(cse_response_t) {                           \
    .magic     = htonl((response).magic),    \
    .error     = htonl((response).error),    \
    .device    = htonl((response).device),   \
    .who_know  = htonl((response).who_know)  \
}\

#define CSE_UNPACK_REQUEST(request)   \
(cse_request_t) {                     \
    .magic  = ntohl((request).magic), \
    .error  = ntohl((request).error), \
    .device = ntohl((request).device) \
}

#define CSE_UNPACK_RESPONSE(response)        \
(cse_response_t) {                           \
    .magic     = ntohl((response).magic),    \
    .error     = ntohl((response).error),    \
    .device    = ntohl((response).device),   \
    .who_know  = ntohl((response).who_know), \
}

#define CSE_REQUEST_TO_RESPONSE(request) \
(cse_response_t) {                       \
    .magic     = CSE_RESPONSE_MAGIC,     \
    .error     = (request).error,        \
    .device    = (request).device,       \
    .procedure = NULL                    \
}

#define CSE_REQ_SENTINEL          \
(cse_request_t) {                 \
    .magic  = CSE_REQUEST_MAGIC,  \
    .error  = CSE_UNKNOWN_ERROR,  \
    .device = CSE_UNKNOWN_DEVICE  \
}

#define CSE_RESP_SENTINEL            \
(cse_response_t) {                   \
    .magic     = CSE_RESPONSE_MAGIC, \
    .error     = CSE_UNKNOWN_ERROR,  \
    .device    = CSE_UNKNOWN_DEVICE, \
    .who_know  = CSE_UNKNOWN_DEVICE, \
    .procedure = NULL                \
}

/* -------------------------------------------------------------------------- *
 * -------------------------- Network declaration --------------------------- *
 * -------------------------------------------------------------------------- */
typedef enum
{
    CSE_NET_OK,
    CSE_NET_NOHOST,
    CSE_NET_REFUSED,
    CSE_NET_NORESPONSE,
    CSE_NET_EMPTY
} cse_net_status_t;

/* Error solving network */
#define MASTER_IP                 "192.168.1.15"
#define TCP_PORT               	  6666
#define UDP_PORT               	  9999
#define MASTER_PORT               8080
#define SLAVE_PORT                8081
#define SLAVE_IP                  "255.255.255.255"

/* -------------------------------------------------------------------------- *
 * ------------------------- POSIX status checking -------------------------- *
 * -------------------------------------------------------------------------- */
#define POSIX_FAILURE             -1
#define OPEN_FAILED(fd)           ((fd) < 0)
#define POLL_FAILED(status)       ((status) == POSIX_FAILURE)
#define POLL_TIMEUP(status)       ((status) == 0)
#define READ_FAILED(status)       ((status) == POSIX_FAILURE)
#define CONN_FAILED(status)       ((status) == POSIX_FAILURE)
#define SEND_FAILED(status)       ((status) == POSIX_FAILURE)
#define RECV_FAILED(status)       ((status) == POSIX_FAILURE)

/* -------------------------------------------------------------------------- *
 * -------------------------- Logging declaration --------------------------- *
 * -------------------------------------------------------------------------- */
#define CSE_REQUEST_FMT(fmt)       "Device %"fmt" has the following error : ERROR %"fmt". Wait for solution"
#define CSE_RESPONSE_FMT(fmt)      "Device %"fmt" has the solution for ERROR %"fmt" from Device %"fmt". Please use the following procedure"

#define CSE_REQUEST_LINE_MAX_SIZE  (sizeof(CSE_REQUEST_FMT())  + 24)
#define CSE_RESPONSE_LINE_MAX_SIZE (sizeof(CSE_RESPONSE_FMT()) + 24)

/* -------------------------------------------------------------------------- *
 * -------------------------------- Parsers --------------------------------- *
 * -------------------------------------------------------------------------- */
/**
 * @brief  Parse string containing error message from production machine.
 * @param  message  line of log containing device and error numbers
 * @param  request  structure to modify, already allocated
 * @return request pointer
 */
cse_request_t* parse_error(const char * message, cse_request_t *request);

/**
 * @brief  Parse packet of request from slave or master.
 * @param  message  payload of network frame
 * @param  request  structure to modify, already allocated
 * @return modified request on successful parse, else CSE_REQ_SENTINEL
 */
cse_request_t* parse_request(const char * message, cse_request_t *request);

/**
 * @brief  Parse packet of response from slave or master.
 * @param  message   payload of network frame
 * @param  response  structure to modify, already allocated
 * @return modified response on successful parse, else CSE_RESP_SENTINEL
 */
cse_response_t* parse_response(char * message, cse_response_t *response);

/**
 * @brief  Send to and/or Receive from master.
 * @remark Buffer must be preallocated, used in both send and receive.
 * @param  buf     contains data to send and will contain received data
 * @param  lenup   length of data to send,      <= 0 means no send
 * @param  maxdown max length of received data, <= 0 means no receive
 * @param  lendown length of received data, overwritten only if maxdown > 0
 * @return CSE_NET_OK if all worked, else other status code
 */
cse_net_status_t cse_sendrecv (char *buf, ssize_t lenup, ssize_t maxdown, ssize_t *lendown);

cse_net_status_t cse_do_request(cse_response_t *response, cse_request_t *request, char * buf);
cse_net_status_t cse_do_response(cse_response_t *response, cse_request_t *request, char * buf);

#endif /* COMMANDES_DEFINES_H_ */
