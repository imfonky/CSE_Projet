
#include <stdio.h> //printf
#include <string.h>    //strlen
#include <sys/socket.h>    //socket
#include <arpa/inet.h> //inet_addr
#include <fcntl.h> // for open
#include <unistd.h> // for close

#include "commandes.h"
 
int main(int argc , char *argv[]) {
	int sock;
	struct sockaddr_in server;
	int error, device;

	cse_request_t orig_request;
	cse_request_t packed_request;
	cse_response_t sent_response;
	cse_response_t unpacked_response;
	//keep communicating with server
	while (1) {

		//Create socket
		sock = socket(AF_INET, SOCK_STREAM, 0);

		struct timeval timeout;
		timeout.tv_sec = 15;
		timeout.tv_usec = 0;

		if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout,
					   sizeof(timeout)) < 0)
			perror("setsockopt failed\n");

		if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (char *) &timeout,
					   sizeof(timeout)) < 0)
			perror("setsockopt failed\n");

		if (sock == -1) {
			printf("Could not create socket");
		}
		puts("Socket created");

		server.sin_addr.s_addr = inet_addr(MASTER_IP);
		server.sin_family = AF_INET;
		server.sin_port = htons(TCP_PORT);

		//Connect to remote server
		if (connect(sock, (struct sockaddr *) &server, sizeof(server)) < 0) {
			perror("connect failed. Error");
			return 1;
		}

		puts("Connected\n");

		printf("\nEnter error number : ");
		scanf("%d", &error);
		printf("\nEnter device number : ");
		scanf("%d", &device);

		orig_request.magic = CSE_REQUEST_MAGIC;
		orig_request.error = error;
		orig_request.device = device;

		packed_request = CSE_PACK_REQUEST(orig_request);

		//Send some data
		if (send(sock, &packed_request, sizeof(cse_request_t), 0) < 0) {
			puts("Send failed");
			return 1;
		}

		//Receive a reply from the server
		if (recv(sock, &sent_response, sizeof(cse_response_t), 0) < 0) {
			puts("recv failed");
			return 1;
		}

		unpacked_response = CSE_UNPACK_RESPONSE(sent_response);
		strcpy(unpacked_response.procedure, sent_response.procedure);

		printf("\nServer reply :\n\terror : %d\n\tdevice : %d\n\twho know : %d\n\tprocedure : \n%s\n",
			   unpacked_response.error, unpacked_response.device, unpacked_response.who_know,
			   unpacked_response.procedure);

		close(sock);

	}

	return 0;
}
