
#include <stdio.h> //printf
#include <string.h>    //strlen
#include <sys/socket.h>    //socket
#include <arpa/inet.h> //inet_addr
#include <fcntl.h> // for open
#include <unistd.h> // for close

#include "commandes.h"

/* fpont 12/99 */
/* pont.net    */
/* udpClient.c */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h> /* memset() */
#include <sys/time.h> /* select() */ 

//for Mac OS X
#include <stdlib.h>

#define REMOTE_SERVER_PORT 1500
#define MAX_MSG 100


/* fpont 12/99 */
/* pont.net    */
/* udpServer.c */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h> /* close() */
#include <string.h> /* memset() */

//for Mac OS X
#include <stdlib.h>

#define MAX_MSG 100

int main(int argc, char *argv[]) {

	cse_request_t orig_request;
	cse_request_t packed_request;

	////UDP SIDE
	int sd, rc, n, cliLen;
	struct sockaddr_in cliAddr, servAddr;
	char msg[MAX_MSG];
	int broadcast = 1;

	/* socket creation */
	sd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sd < 0) {
		printf("%s: cannot open socket \n", argv[0]);
		exit(1);
	}

	if (setsockopt(sd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof broadcast) == -1) {
		perror("setsockopt (SO_BROADCAST)");
		exit(1);
	}

	/* bind local server port */
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servAddr.sin_port = htons(UDP_PORT);
	rc = bind(sd, (struct sockaddr *) &servAddr, sizeof(servAddr));
	if (rc < 0) {
		printf("cannot bind port number %d \n", UDP_PORT);
		exit(1);
	}

	printf("waiting for data on port UDP %u\n", UDP_PORT);

	/* server infinite loop */
	while (1) {

		/* init buffer */
		memset(msg, 0x0, MAX_MSG);


		/* receive message */
		cliLen = sizeof(cliAddr);
		n = recvfrom(sd, &packed_request, sizeof(cse_request_t), 0,
					 (struct sockaddr *) &cliAddr, &cliLen);

		if (n < 0) {
			printf("cannot receive data \n");
			continue;
		}

		orig_request = CSE_UNPACK_REQUEST(packed_request);

		/* print received message */
		printf("from %s:UDP%u : [ device %d |error %d ]\n",
			   inet_ntoa(cliAddr.sin_addr),
			   ntohs(cliAddr.sin_port),
			   orig_request.device,
			   orig_request.error);

		////TCP SIDE

		int sock;
		struct sockaddr_in server;
		char procedure[CSE_PROCEDURE_MAX_SIZE];
		int who_know = 0;
		cse_response_t sent_response;
		cse_response_t unpacked_response;

		//Create tcp socket
		sock = socket(AF_INET, SOCK_STREAM, 0);

		struct timeval timeout;
		timeout.tv_sec = 5;
		timeout.tv_usec = 0;

		if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout,
					   sizeof(timeout)) < 0)
			perror("setsockopt failed\n");

		if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (char *) &timeout,
					   sizeof(timeout)) < 0)
			perror("setsockopt failed\n");

		if (sock == -1) {
			printf("Could not create socket");
		}
		puts("Socket created");

		server.sin_addr.s_addr = inet_addr(MASTER_IP);
		server.sin_family = AF_INET;
		server.sin_port = htons(TCP_PORT);

		//Connect to remote server
		if (connect(sock, (struct sockaddr *) &server, sizeof(server)) < 0) {
			perror("connect failed. Error");
			return 1;
		}

		puts("Connected\n");

		printf("\nEnter procedure : ");
		scanf("%s", procedure);
		printf("\nEnter number of you device : ");
		scanf("%d", &who_know);

		unpacked_response.magic = CSE_RESPONSE_MAGIC;
		unpacked_response.error = orig_request.error;
		unpacked_response.device = orig_request.device;
		unpacked_response.who_know = who_know;
		sent_response = CSE_PACK_RESPONSE(unpacked_response);
		strcpy(sent_response.procedure, procedure);

		//Send some data
		if (send(sock, &sent_response, sizeof(cse_response_t), 0) < 0) {
			puts("Send failed");
			return 1;
		}

	}/* end of server infinite loop */

	return 0;
}
