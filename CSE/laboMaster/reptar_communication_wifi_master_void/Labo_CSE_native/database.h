#ifndef DATABASE_DEFINES_H_
#define DATABASE_DEFINES_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "commandes.h"

#define DIR_PATH "./database/"
#define MAX_NUMBERS 10

int database_init();

int database_get(int item, cse_response_t* response);

int database_set(int item, cse_response_t response);

#endif /* DATABASE_DEFINES_H_ */
