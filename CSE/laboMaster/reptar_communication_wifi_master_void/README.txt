Marche à suivre pour compiler et exécuter le laboratoire :

	1) Compiler les drivers et le jeu (Il faut mettre à jour les paths dans le Makefile par rapport à l'emplacement de votre buildroot)

    # source setup-env.sh
		# cd Labo_CSE
		# make


	2) Copier les fichiers suivants sur la carte REPTAR

		- Labo_CSE/communication_wifi_master     (  /root/home/communication_wifi_master  )
		- CSE_drivers/fpga.ko         (  /root/home/CSE_drivers/fpga.ko  )
		- CSE_drivers/touch-screen.ko (  /root/home/CSE_drivers/touch-screen.ko  )
		- Le script Labo_CSE/insertion_drivers.sh  (  dans /root/home/insertion_drivers.sh  )

	3) Copier le device tree (dts/omap3-reptar.dtb) sur la partition de boot

	4) Booter la carte Reptar avec le noyau Xenomai + le device tree Arkanoid
    # Bloquer le boot auto en appuyant sur une touche
    # run boot_xeno

	5) Lancer le script d'insertion des drivers

		# ./insertion_drivers.sh

	6) Et enfin... lancer le programme:
		
		# ./communication_wifi_master
