#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <fcntl.h>

#include "drivers_toolbox.h"


const unsigned short segMask[] = {
		0b00111111, //0
		0b00000110, //1
		0b01011011, //2
		0b01001111, //3
		0b01100110, //4
		0b01101101, //5
		0b01111101, //6
		0b00000111, //7
		0b01111111, //8
		0b01101111, //9
		0b00000000, //null
	};
	

// comande pour le terminal linux : scp rt-app.ko root@192.168.1.200:/home

int main(int argc, char **argv) {

	int fd_led 				= open("/dev/arkanoid_drivers/led",O_RDWR);
	int fd_lcd_4x20			= open("/dev/arkanoid_drivers/lcd_4x20",O_RDWR);
	int fd_7seg1			= open("/dev/arkanoid_drivers/7seg1",O_RDWR);
	int fd_7seg2			= open("/dev/arkanoid_drivers/7seg2",O_RDWR);
	int fd_7seg3			= open("/dev/arkanoid_drivers/7seg3",O_RDWR);
	int fd_buzz 			= open("/dev/arkanoid_drivers/buzz",O_RDWR);
	int fd_switch 			= open("/dev/arkanoid_drivers/switch",O_RDWR);
	int fd_encoder_count 	= open("/dev/arkanoid_drivers/encoder_count",O_RDWR);
	int fd_encoder_dir 		= open("/dev/arkanoid_drivers/encoder_dir",O_RDWR);
	int fd_ts 				= open("/dev/arkanoid_drivers/touch_screen",O_RDWR);

	char valeurByte = 0;
	unsigned short valeurWord = 0;
	unsigned short count =0;
	unsigned short directionWord = 0;

	int coordXtouchScreen = 0;
	int oldCoord = 0;

	struct coordTS_t {
		int x;
		int y;
		unsigned int pressure;
	};

	struct coordTS_t coordTS;
	struct coordTS_t oldCoordTS;

	printf("Le test debute!\n");

	while(1){
		usleep(50000);
		ioctl(fd_ts,RD_TOUCHSCREEN_CMD,&coordTS);
		if (coordTS.pressure > 0){
			//if(oldCoordTS.x != coordTS.x || oldCoordTS.y != coordTS.y){
				printf("valeur de x = %d\n",coordTS.x);
				printf("valeur de y = %d\n",coordTS.y);
				oldCoordTS.x = coordTS.x;
				oldCoordTS.y = coordTS.y;
				printf("\n---------------\n");
			//}
		}
		ioctl(fd_switch,RD_TOUCHSCREEN_CMD,&coordTS);
		if(valeurWord++ == 128)
			break;
	}
	valeurWord = 0;

	sleep(2);
	lcd4x20_clearDisplay(fd_lcd_4x20);
	sleep(1);
	lcd4x20_print(fd_lcd_4x20,LINE1,"HELLO WORLD");
	sleep(2);
	lcd4x20_clearLine(fd_lcd_4x20,LINE1);
	lcd4x20_print(fd_lcd_4x20,LINE1,"WHATSUP NIGGA");
	sleep(3);
	lcd4x20_clearDisplay(fd_lcd_4x20);
	sleep(2);
	lcd4x20_print(fd_lcd_4x20,LINE1,"Youpieee!");
	lcd4x20_print(fd_lcd_4x20,LINE2,"les drivers");
	lcd4x20_print(fd_lcd_4x20,LINE3,"c'est trop bien...");
	lcd4x20_print(fd_lcd_4x20,LINE4,"     ;-)    ");


	print_7SEG_number_3digits(fd_7seg1,fd_7seg2,fd_7seg3,666);

	ioctl(fd_buzz,WRITE_CMD,BUZZ_EN | BUZZ_HIGH_FREQ);
	sleep(1);
	ioctl(fd_buzz,WRITE_CMD,BUZZ_EN | BUZZ_LOW_FREQ);
	sleep(1);
	ioctl(fd_buzz,WRITE_CMD,BUZZ_OFF);

	do{
		ioctl(fd_switch,READ_CMD,&valeurWord);
		ioctl(fd_encoder_count,READ_CMD,&count);
		ioctl(fd_encoder_dir,READ_CMD,directionWord);
		printf("count = %d\n",count);
		printf("directionWord = %d\n",directionWord);
		sleep(1);
	}while(valeurWord != 128);


	valeurWord =0;
	//- Switchs et LEDs
	printf("Test boutons poussoir et LEDs:\n");

	ioctl(fd_led,WRITE_CMD,LEDS_OFF);
	ioctl(fd_led,WRITE_CMD,LED_1);
	sleep(1);
	ioctl(fd_led,WRITE_CMD,LED_2);
	sleep(1);
	do{

		ioctl(fd_switch,READ_CMD,&valeurWord);
		printf("DBG: valeurWord = %d\n",valeurWord);
		ioctl(fd_led,WRITE_CMD,valeurWord);
		sleep(1);

		if(valeurWord == 128)
			break;
	}while(1);
	printf("LEDs et Switch OK!\n");

	return 0; //SUCCESS
}

/*---------------------- FONCTION USER POUR PERIFERIQUE ----------------------- */

int lcd4x20_print(int fd,int line, const char *str){

	int nbrCharToSend;
	int posCursor;

	// On recupère la position du curseur pour la ligne désirée.
	posCursor = getLineAddr(line);

	ioctl(fd,WRITE_CMD,START|SET_DDRAM|posCursor);

	for (nbrCharToSend = 0; nbrCharToSend < strlen(str) && nbrCharToSend < 20; nbrCharToSend++){

		ioctl(fd,WRITE_CMD,START|WRITE_TO_RAM|((unsigned int)str[nbrCharToSend]) );
	}

	//lcd4x20_cursorHome(fd);

	return nbrCharToSend;
}

void lcd4x20_clearLine(int fd,int line){
	int i;
	int posCursor;

	// On recupère la position du curseur pour la ligne désirée.
	posCursor = getLineAddr(line);

	// On positionne le curseur à la ligne désirée.
	ioctl(fd,WRITE_CMD,START|SET_DDRAM|posCursor);

	// On clear la ligne désirée.
	for (i = 0; i < 20; i++){
		ioctl(fd,WRITE_CMD,START|WRITE_TO_RAM|CLEAR_LINE);
	}

	//lcd4x20_cursorHome(fd);
}

void lcd4x20_clearDisplay(int fd){

	// On clear l'afficheur tout entier.
	ioctl(fd,WRITE_CMD,START|CLEAR_DISPLAY);

}

int getLineAddr(int line){

	if (line > 3)
		return -1;
	switch (line) {
		case 0:
			return 0x00;
		case 1:
			return 0x40;
		case 2:
			return 0x14;
		case 3:
			return 0x54;
		default:
			printf("invalid line number %d for lcd4x20\n", line);
			return -1;
	}

	return line;
}

int print_7SEG_number_3digits(int fd_7seg1,int fd_7seg2,int fd_7seg3, int number){

	char digit_1,digit_2,digit_3; // de droit à gauche

	if(number > MAX_TO_DISPLAY)
		return -1;

	if(number == _7SEG_ALL_OFF){
		ioctl(fd_7seg1,WRITE_CMD,segMask[10] & 0xff);
		ioctl(fd_7seg2,WRITE_CMD,segMask[10] & 0xff);
		ioctl(fd_7seg3,WRITE_CMD,segMask[10] & 0xff);
		return 0; //SUCCESS
	}
	else{
		digit_1 = (char)(number%10);
		digit_2 = (char)((number/10)%10);
		digit_3 = (char)(number/100);
	}
	ioctl(fd_7seg1,WRITE_CMD,segMask[digit_1]);
	ioctl(fd_7seg2,WRITE_CMD,segMask[digit_2]);
	ioctl(fd_7seg3,WRITE_CMD,segMask[digit_3]);
	return 0; //SUCCESS
}

