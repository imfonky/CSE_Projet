#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <fcntl.h>

#include "drivers_toolbox.h"


/*---------------------- FONCTION USER POUR PERIPHERIQUE ----------------------- */

int lcd4x20_print(int fd,int line, const char *str){

	int nbrCharToSend;
	int posCursor;

	// On recupère la position du curseur pour la ligne désirée.
	posCursor = getLineAddr(line);

	ioctl(fd,WRITE_CMD,START|SET_DDRAM|posCursor);

	for (nbrCharToSend = 0; nbrCharToSend < strlen(str) && nbrCharToSend < 20; nbrCharToSend++){

		ioctl(fd,WRITE_CMD,START|WRITE_TO_RAM|((unsigned int)str[nbrCharToSend]) );
	}

	//lcd4x20_cursorHome(fd);

	return nbrCharToSend;
}

void lcd4x20_clearLine(int fd,int line){
	int i;
	int posCursor;

	// On recupère la position du curseur pour la ligne désirée.
	posCursor = getLineAddr(line);

	// On positionne le curseur à la ligne désirée.
	ioctl(fd,WRITE_CMD,START|SET_DDRAM|posCursor);

	// On clear la ligne désirée.
	for (i = 0; i < 20; i++){
		ioctl(fd,WRITE_CMD,START|WRITE_TO_RAM|CLEAR_LINE);
	}

	//lcd4x20_cursorHome(fd);
}

void lcd4x20_clearDisplay(int fd){

	// On clear l'afficheur tout entier.
	ioctl(fd,WRITE_CMD,START|CLEAR_DISPLAY);

}

int getLineAddr(int line){

	if (line > 3)
		return -1;
	switch (line) {
		case 0:
			return 0x00;
		case 1:
			return 0x40;
		case 2:
			return 0x14;
		case 3:
			return 0x54;
		default:
			printf("invalid line number %d for lcd4x20\n", line);
			return -1;
	}

	return line;
}

int print_7SEG_number_3digits(int fd_7seg1,int fd_7seg2,int fd_7seg3, int number){
	
	const unsigned short segMask[] = {
		0b00111111, //0
		0b00000110, //1
		0b01011011, //2
		0b01001111, //3
		0b01100110, //4
		0b01101101, //5
		0b01111101, //6
		0b00000111, //7
		0b01111111, //8
		0b01101111, //9
		0b00000000, //null
	};
	
	char digit_1,digit_2,digit_3; // de droit à gauche

	if(number > MAX_TO_DISPLAY)
		return -1;

	if(number == _7SEG_ALL_OFF){
		ioctl(fd_7seg1,WRITE_CMD,segMask[10] & 0xff);
		ioctl(fd_7seg2,WRITE_CMD,segMask[10] & 0xff);
		ioctl(fd_7seg3,WRITE_CMD,segMask[10] & 0xff);
		return 0; //SUCCESS
	}
	else{
		digit_1 = (char)(number%10);
		digit_2 = (char)((number/10)%10);
		digit_3 = (char)(number/100);
	}
	ioctl(fd_7seg1,WRITE_CMD,segMask[digit_1]);
	ioctl(fd_7seg2,WRITE_CMD,segMask[digit_2]);
	ioctl(fd_7seg3,WRITE_CMD,segMask[digit_3]);
	return 0; //SUCCESS
}


