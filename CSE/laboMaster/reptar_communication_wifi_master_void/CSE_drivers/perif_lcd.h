
/*
 * ======================== KERNEL SPACE ===========================
 */


// Macros imposable celon la commande aux registre du LCD.

#define START_FLAG				0x400U

#define FUNCTION_SET			0x20U
#define _8BITS_MODE_BUS 		0x10U
#define DISPLAY_CTRL			0x08U
#define DISPLAY_ON				0x04U
#define DISPLAY_OFF				0x00U
#define CLEAR_DISPLAY			0x01U
#define ENTRY_MODE_CTRL			0x04U
#define ENTRY_MODE_INC			0x02U
#define ENTRY_MODE_SHIFT_RIGHT	0x00U
#define RETURN_HOME				0x02U
#define READWRITE_FLAG			0x0100U
#define SET_DDRAM				0x80U

#define READY_FLAG				0x0100U

#define _1LINE_DISPLAY			0x00U
#define _2LINE_DISPLAY			0x08U
#define LINE_SIZE	20

#define _5x8DOTS_DISPLAY		0x00U
#define _5x11DOTS_DISPLAY		0x04U

#define CURSOR_ON				0x01U
#define CURSOR_OFF				0x00U
#define CURSOR_BLINK_ON			0x01U
#define CURSOR_BLINK_OFF		0x00U

#define CMD_NULL				0x00U


#define SUCCESS 0
//#define DRIVER_NAME		"reptar-lcd4x20"
//#define DRIVER_VERSION	"3.0"
#define LCD4X20_MAX_BUF_LEN	100  /* for 4 lines of 20 characters */
