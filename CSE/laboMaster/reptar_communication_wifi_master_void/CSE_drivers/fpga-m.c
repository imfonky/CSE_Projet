
/*
 * ======================== KERNEL SPACE ===========================
 * =              DRIVER PERIFERIQUE FPGA SPARTAN 6                =
 * =================================================================
 *
 * fpga-m.c
 *
 * ioctl implémenter pour répondre aux demandes de l'espace utilisateur
 * sur le périphériques de la fpga SP6 suivant :
 * 	- LEDs
 * 	- Afficheur LCD_4x20
 * 	- 3 Afficheur 7 segments
 * 	- Buzz
 * 	- Switchs
 * 	- Encodeur Incrémental
 *
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>

#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <arch/arm/include/asm/delay.h>
#include <linux/delay.h>

#include "fpga-m.h"
#include "perif_lcd.h"

// Variable(s)
struct cdev *my_dev;

static void __iomem *fpga_base;		/* FPGA base address */

struct fpga_data {
	int minor;
};

/* Callback */
static struct file_operations fops =
{
		.open = fpga_open,
		.release = fpga_close,
		.unlocked_ioctl	= fpga_ioctl,
};

/* Very sub-optimal design, the busy flag should be checked... */
static void fpga_write(unsigned int reg, uint16_t cmd)
{
	writew(cmd, fpga_base + reg);
}

static uint16_t fpga_read(unsigned reg)
{
	return readw(fpga_base + reg);
}

static int lcd_write_cmd(uint16_t cmd)
{
	int retries = 50;
	uint16_t reg = fpga_read(LCD_4x20_STATUS_REG);

	/* Wait for the FPGA to be ready */
	while (!(reg & LCD_4x20_STATUS_READY) && retries--) {
		udelay(200);
		reg = fpga_read(LCD_4x20_STATUS_REG);
	}

	if (retries == 0) {
		pr_warn("%s: Timeout waiting for fpga response\n", __func__);
		return -EBUSY;
	}

	fpga_write(LCD_4x20_REG, START_FLAG | cmd);

	return 0;
}

static int lcd_busy_wait(void)
{
	int retries = 10;
	uint16_t reg;

	do {
		lcd_write_cmd(LCD_4x20_CTL_READ);
		reg = fpga_read(LCD_4x20_STATUS_REG);
	} while ((reg & LCD_4x20_STATUS_BUSY) && --retries);

	if (retries == 0) {
		pr_err("%s: Timeout waiting for busyflag\n", __func__);
		return -EBUSY;
	}

	return 0;
}

static int lcd_write_cmd_busy(uint16_t cmd)
{
	int err;

	err = lcd_write_cmd(cmd);
	if (err) {
		;
	}

	err = lcd_busy_wait();
	if (err) {
		;
	}

	return 0;
}

long fpga_ioctl(struct file * file, unsigned int cmd, unsigned long arg)
{
	struct fpga_data *fpga = (struct fpga_data *)file->private_data;
	/* Identification du périphérique voulu grace au mineur.
	   Et verification du droit en écriture */
	int minor; /*Le mineur correspondant au périférique désiré*/
	void __iomem *registerSP6 = NULL; /*Registre accèssible de la fpga SP6*/
	unsigned short valChecked;/*Valeur word controlée*/

	minor = fpga->minor;

	switch(minor){

		case PERIF_LED: /* 0) */
			registerSP6 = fpga_base + LED_REG;
			valChecked = (unsigned short)(arg & MASK_6BITS_LSB);
			break;

		case PERIF_LCD_4x20: /* 1) */
			valChecked = (unsigned short)(arg & MASK_11BITS_LSB);
			lcd_write_cmd_busy(valChecked);
			return 0;

		case PERIF_7SEG1: /* 2) */
			if(cmd == READ_ACCESS){
				printk("ERROR > read access on this device not allowed");
				return -1;
			}
			registerSP6 = fpga_base + _7SEG1_REG;
			valChecked = (unsigned short)(arg & MASK_8BITS_LSB);
			break;

		case PERIF_7SEG2: /* 3) */
			if(cmd == READ_ACCESS){
				printk("ERROR > read access on this device not allowed");
				return -1;
			}
			registerSP6 = fpga_base + _7SEG2_REG;
			valChecked = (unsigned short)(arg & MASK_8BITS_LSB);
			break;

		case PERIF_7SEG3: /* 4) */
			if(cmd == READ_ACCESS){
				printk("ERROR > read access on this device not allowed");
				return -1;
			}
			registerSP6 = fpga_base + _7SEG3_REG;
			valChecked = (unsigned short)(arg & MASK_8BITS_LSB);
			break;

		case PERIF_BUZZ: /* 5) */
			registerSP6 = fpga_base + BUZZ_REG;
			valChecked = (unsigned short)(arg & MASK_3BITS_LSB);
			break;

		case PERIF_SWITCH: /* 6) */
			if(cmd == WRITE_ACCESS){
				printk("ERROR > write access on this device not allowed");
				return -1;
			}
			registerSP6 = fpga_base + SW_REG;
			valChecked =  MASK_8BITS_LSB & readw(registerSP6); /*On lit la valeur déjà ici pour
															   masquer les bits inutils*/
			break;

		case PERIF_ENCODER_COUN: /* 7) */
			if(cmd == WRITE_ACCESS){
				printk("ERROR > write access on this device not allowed");
				return -1;
			}
			registerSP6 = fpga_base + ENCODER_COUNT_REG;
			valChecked = MASK_16BITS_LSB & readw(registerSP6); /*On lit la valeur déjà ici pour
															   masquer les bits inutils*/
			break;

		case PERIF_ENCODER_DIRECTION: /* 8) */
			if(cmd == WRITE_ACCESS){
				printk("ERROR > write access on this device not allowed");
				return -1;
			}
			registerSP6 = fpga_base + ENCODER_COUNT_DIR_REG;
			valChecked = MASK_2BITS_MSB & readw(registerSP6); /*On lit la valeur déjà ici pour
															   masquer les bits inutils*/
			break;

		default:
			pr_err("%s: Unkown ioctl!", __func__);
			return -EINVAL;
	}

	switch(cmd){

		/* --------------- Lecture ----------------- */
		case READ_ACCESS:
			if(copy_to_user((unsigned short *)arg,&valChecked,sizeof(unsigned short)) != 0){
				printk("send FAIL:\n");
				return -1; // FAILURE
			}
			break;

		/* --------------- Ecriture ----------------- */
		case WRITE_ACCESS:
			writew(valChecked,registerSP6);

		default:
			break;
	}

	return 0;
}

int fpga_open (struct inode *inode, struct file *file)
{
	/* On récupère le mineur l'or de l'open, pour pouvoir par la suite
	   fair la distinction de chaque périphérique se trouvant sur
	   le même bus.
	*/
	struct fpga_data *fpga;

	fpga = kzalloc(sizeof(*fpga), GFP_KERNEL);
	if (fpga == NULL) {
		pr_err("%s: cannot allocate memory for private structure\n", __func__);
		return -ENOMEM;
	}

	fpga->minor = iminor(inode);

	file->private_data = fpga;

	return 0; // SUCCESS
}

int fpga_close(struct inode *inode, struct file *file)
{
	kfree(file->private_data);

	return 0;
}


int __init init_module(void)
{
	dev_t dev;

	// majeur de 126 pour le driver FPGA avec ses perifériques
	dev = MKDEV(126,0);
	my_dev = cdev_alloc();
	cdev_init(my_dev, &fops);

	my_dev->owner = THIS_MODULE;
	cdev_add(my_dev,dev,9); // nombre de périphérique sur le bus à modifier si rajout de mineur

	// On fait un mappage virtuelle du nombre de byte que l'on désire.
	fpga_base = ioremap(REPTAR_FPGA_SP6_BASE,REPTAR_FPGA_SP6_TO_MAP);

	//initialisation de l'afficheur LCD_4x20.
	lcd4x20_8bits_init();
	printk("rt-app: LCD_4x20 initialized\n");

	printk("Kernel init_module FPGA DONE !\n");

	return 0;
}

void __exit cleanup_module(void)
{
	iounmap(fpga_base);
	cdev_del(my_dev);
	printk("Kernel cleanup_module FPGA DONE !\n");
}

//--------------------- Fonction(s) de service pour le DRIVER -------------------------------

/*
 * But : Initialise l'afficheur LCD_4x20 pour le rendre prêt
 * 		 à recevoir des commandes d'affichage.
 */
void lcd4x20_8bits_init(void){

	unsigned short commande;

	// 1er envoie d'init
	commande = FUNCTION_SET
		   | _8BITS_MODE_BUS;
	lcd_write_cmd_busy(commande);
	udelay(2000);
	udelay(2000);
	udelay(1000);

	// 2ème envoie d'init
	commande = FUNCTION_SET
		   | _8BITS_MODE_BUS;
	lcd_write_cmd_busy(commande);


	// 3ème envoie d'init
	commande = FUNCTION_SET
		   | _8BITS_MODE_BUS;
	lcd_write_cmd_busy(commande);

	// On specifie le nombre de ligne d'affichage et la "font" des caractère(sur 5x8 ou 5x11 pixels)
	commande = FUNCTION_SET
		   | _8BITS_MODE_BUS
		   | _2LINE_DISPLAY
		   | _5x8DOTS_DISPLAY;
	lcd_write_cmd_busy(commande);

	// On éteint l'afficheur.
	commande = DISPLAY_CTRL
		   | DISPLAY_OFF
		   | CURSOR_OFF
		   | CURSOR_BLINK_OFF;
	lcd_write_cmd_busy(commande);

	// On allume l'afficheur.
	commande = DISPLAY_CTRL
		   | DISPLAY_ON
		   | CURSOR_ON
		   | CURSOR_BLINK_ON;
	lcd_write_cmd_busy(commande);

	// On clear l'afficheur.
	commande = CLEAR_DISPLAY;
	lcd_write_cmd_busy(commande);

	// On fixe la direction de déplacement du curseur et de l'affichage.
	// On déplace le curseur vers la droite.
	commande = ENTRY_MODE_CTRL;
	commande |= ENTRY_MODE_INC
			 | ENTRY_MODE_SHIFT_RIGHT;
	lcd_write_cmd_busy(commande);

	// On met le compteur d'adresse à 0.
	commande = RETURN_HOME;
	lcd_write_cmd_busy(commande);
}

MODULE_LICENSE("GPL");
