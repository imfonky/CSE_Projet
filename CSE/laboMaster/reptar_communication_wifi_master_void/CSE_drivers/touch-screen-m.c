/******************************************************************************
 * File		: rt-app-m.c
 *
 * Authors	: (DRE), (XBC) 2012
 *
 * Comment	:
 *  The lab goal is to implement a Arkanoid clone on the laboratory boards
 *  (Reptar) as a hard realtime Xenomai kernel application.
 *
 ******************************************************************************/
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/slab.h>

#include <asm/uaccess.h>
#include <asm/io.h>
#include <asm-generic/delay.h>


#include "xeno-ts.h"
#include "touch-screen-m.h"

struct cdev *my_dev;

struct fpga_data {
	int minor;
};

struct coordTS_t {
	int x;
	int y;
	unsigned int pressure;
};

struct coordTS_t coordTS = {0,0,0};

/*---------------------- Definition du callback open -----------------------*/
int fpga_open (struct inode *inode, struct file *file)
{
	struct fpga_data *fpga;

	fpga = kzalloc(sizeof(*fpga), GFP_KERNEL);
	if (fpga == NULL) {
		pr_err("%s: cannot allocate memory for private structure\n", __func__);
		return -ENOMEM;
	}

	/* On récupère le mineur l'or de l'open, pour pouvoir par la suite
	   fair la distinction de chaque périphérique se trouvant sur
	   le même bus.
	*/
	fpga->minor = iminor(inode);

	file->private_data = fpga;

	return 0; // SUCCESS
}

#define NUM_SAMPLES 50

/*---------------------- Definition du callback ioctl -----------------------*/
long fpga_ioctl(struct file * file, unsigned int cmd, unsigned long arg)
{
	struct fpga_data *fpga = (struct fpga_data *)file->private_data;
	int minor;
	struct ts_sample samples[NUM_SAMPLES];

	minor = fpga->minor;

	switch(minor){
	case 0: // RD_TOUCHSCREEN
		if ( xeno_ts_read(&samples, NUM_SAMPLES, O_NONBLOCK) == 0 ){
			return -1;
		}

		if(samples[0].pressure > 0 && samples[0].y < 5000 && samples[0].x < 1000){
			coordTS.x = ( (samples[0].x + 12) * 339) /100;
			coordTS.y = 480 - ((samples[0].y-157)*269)/100;
		}
		coordTS.pressure = samples[0].pressure;

		if(copy_to_user((struct coordTS_t *)arg,&coordTS,sizeof(struct coordTS_t) ) != 0){
			return -1; // FAILURE
		}
		break;

	default:
		break;
	}

	return 0;
}

/*---------------------- Definition du callback close -----------------------*/
int fpga_close(struct inode *inode, struct file *file)
{
	kfree(file->private_data);

	return 0;
}

/*------------ Callback --------------*/
static struct file_operations fops =
{
		.open = fpga_open,
		.release = fpga_close,
		.unlocked_ioctl	= fpga_ioctl,
};

int __init init_module(void) {
	// pour l'entrée /dev
	dev_t dev;

	dev = MKDEV(127,0); // majeur de 127 et mineur de 0
	my_dev = cdev_alloc();
	cdev_init(my_dev, &fops);

	my_dev->owner = THIS_MODULE;
	cdev_add(my_dev,dev,1); // nombre de mineur = 1 car un seul périph.

	//initialisation du Touch Screen.
	xeno_ts_init();
	printk("rt-app: Touchscreen initialized\n");

	/* To Be Completed */

	return 0;
}

void __exit cleanup_module(void) {
	cdev_del(my_dev);
	xeno_ts_exit();
	printk("Kernel cleanup_module RT-APP DONE !\n");
}

MODULE_LICENSE("GPL");
