#ifndef __XENO_FPGA_M_H__
#define __XENO_FPGA_M_H__

/*
 * xeno-fpga.h
 *
 * Simple wrapper components on the fpga SP6
 *
 */

#define REPTAR_FPGA_SP6_BASE	0x18000000 /* Base address of the FPGA Spartan 6 */
#define REPTAR_FPGA_SP6_TO_MAP	0x72 /* nombre d'adresse à maper pour utiliser les périphériques
										désirer */

/*----------------------------------------------------------------------------------------------
-           ADRESSE DES PERIPHERIQUE UTILE DE LA FPGA SPARTAN6                              	   -
----------------------------------------------------------------------------------------------*/

/* mineur possible vis-à-vis des périphérique utilisé.*/
#define PERIF_LED					0
#define PERIF_LCD_4x20				1
#define PERIF_7SEG1					2
#define PERIF_7SEG2					3
#define PERIF_7SEG3					4
#define PERIF_BUZZ					5
#define PERIF_SWITCH				6
#define PERIF_ENCODER_COUN			7
#define PERIF_ENCODER_DIRECTION		8

/*--------- Action désirée sur le périphérique -------------*/
#define READ_ACCESS		0
#define WRITE_ACCESS	1

/*--------- Masque de controle pour les valeurs de l'argument -------------*/
#define MASK_2BITS_MSB		0xC0
#define MASK_3BITS_LSB		0xF
#define MASK_6BITS_LSB		0x3F
#define MASK_8BITS_LSB		0xFF
#define MASK_11BITS_LSB		0x7FF
#define MASK_16BITS_LSB		0x7FFFF

/* ------------ Leds ------------- */
#define LED_REG 		0x3a
#define WR_LED			1
#define LED_ON 			0x1

/* ----------- Switch ----------- */
#define SW_REG			0x12
#define RD_SW			10

/* ---------- Afficheur LCD 4x20 ---------- */
#define LCD_4x20_REG 	0x36
#define WR_LCD_4x20		3
#define LCD_4x20_CTL_READ	(1 << 8)

#define LCD_4x20_STATUS_REG	0x38
#define LCD_4x20_STATUS_BUSY		(1 << 7)
#define LCD_4x20_STATUS_READY_OFFSET	8
#define LCD_4x20_STATUS_READY		(1 << LCD_4x20_STATUS_READY_OFFSET)

/* ---------- Buzz ---------- */
#define BUZZ_REG 		0x3c
#define WR_BUZZ			4

/* ---------- Afficheurs 7 segments ---------- */
#define	 _7SEG1_REG		0x30
#define	 _7SEG2_REG		0x32
#define	 _7SEG3_REG		0x34


/* ---------- Encodeur Incrémental --------- */
#define ENCODER_COUNT_REG 		0x16
#define RD_ENCODER_COUNT		0x11
#define ENCODER_COUNT_DIR_REG	0x14
#define RD_ENCODER_DIRECTION	0x12

int fpga_open(struct inode *inode, struct file *file);
int fpga_close(struct inode *inode, struct file *file);
long fpga_ioctl(struct file *, unsigned int, unsigned long);

//--------------------- Fonction de service pour le DRIVER -------------------------------
void lcd4x20_8bits_init(void);


#endif /* __XENO_FPGA_M_H__ */
