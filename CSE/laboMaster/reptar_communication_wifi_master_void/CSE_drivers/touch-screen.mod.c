#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xeea9e15a, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x53c4fe30, __VMLINUX_SYMBOL_STR(rcu_lock_map) },
	{ 0x11a8c12b, __VMLINUX_SYMBOL_STR(cdev_alloc) },
	{ 0x273677ec, __VMLINUX_SYMBOL_STR(cdev_del) },
	{ 0x4ab23e60, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x12da5bb2, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x6a22af15, __VMLINUX_SYMBOL_STR(cdev_init) },
	{ 0x94c9a17c, __VMLINUX_SYMBOL_STR(input_get_keycode) },
	{ 0x1559601b, __VMLINUX_SYMBOL_STR(_raw_spin_unlock) },
	{ 0x67c2fa54, __VMLINUX_SYMBOL_STR(__copy_to_user) },
	{ 0x2e5810c6, __VMLINUX_SYMBOL_STR(__aeabi_unwind_cpp_pr1) },
	{ 0x97255bdf, __VMLINUX_SYMBOL_STR(strlen) },
	{ 0x908fcdcf, __VMLINUX_SYMBOL_STR(input_release_device) },
	{ 0xe88f3160, __VMLINUX_SYMBOL_STR(input_unregister_handle) },
	{ 0xea7fcddf, __VMLINUX_SYMBOL_STR(input_flush_device) },
	{ 0xb1ad28e0, __VMLINUX_SYMBOL_STR(__gnu_mcount_nc) },
	{ 0x11a4406c, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xbd95af8a, __VMLINUX_SYMBOL_STR(input_register_handler) },
	{ 0x6bcb6e07, __VMLINUX_SYMBOL_STR(__might_sleep) },
	{ 0x5136de06, __VMLINUX_SYMBOL_STR(input_inject_event) },
	{ 0x4a0e4bde, __VMLINUX_SYMBOL_STR(lock_release) },
	{ 0xaf8e8fef, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x7cc4a5d, __VMLINUX_SYMBOL_STR(printk_timed_ratelimit) },
	{ 0xe707d823, __VMLINUX_SYMBOL_STR(__aeabi_uidiv) },
	{ 0xfe096515, __VMLINUX_SYMBOL_STR(lock_acquire) },
	{ 0x9d847b4e, __VMLINUX_SYMBOL_STR(input_unregister_handler) },
	{ 0x4874e23f, __VMLINUX_SYMBOL_STR(device_del) },
	{ 0x9217c6c2, __VMLINUX_SYMBOL_STR(input_grab_device) },
	{ 0x4145835b, __VMLINUX_SYMBOL_STR(input_close_device) },
	{ 0xdcc19041, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x6b51a1fd, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irq) },
	{ 0xd770f751, __VMLINUX_SYMBOL_STR(input_open_device) },
	{ 0x2469810f, __VMLINUX_SYMBOL_STR(__rcu_read_unlock) },
	{ 0x51f6d8f2, __VMLINUX_SYMBOL_STR(device_add) },
	{ 0x6091797f, __VMLINUX_SYMBOL_STR(synchronize_rcu) },
	{ 0x2196324, __VMLINUX_SYMBOL_STR(__aeabi_idiv) },
	{ 0x1fa10e97, __VMLINUX_SYMBOL_STR(cdev_add) },
	{ 0x93f39472, __VMLINUX_SYMBOL_STR(input_class) },
	{ 0x5f2924eb, __VMLINUX_SYMBOL_STR(input_set_keycode) },
	{ 0x1e85c3fd, __VMLINUX_SYMBOL_STR(put_device) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0x14705d05, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0x1e06d7c2, __VMLINUX_SYMBOL_STR(__raw_spin_lock_init) },
	{ 0xd99ecc59, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x80b8b59, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x648c1f52, __VMLINUX_SYMBOL_STR(get_device) },
	{ 0xe6cba35f, __VMLINUX_SYMBOL_STR(input_register_handle) },
	{ 0x21659a1a, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xefe1a13f, __VMLINUX_SYMBOL_STR(prepare_to_wait_event) },
	{ 0x4f68e5c9, __VMLINUX_SYMBOL_STR(do_gettimeofday) },
	{ 0x143c13d1, __VMLINUX_SYMBOL_STR(mutex_lock_nested) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x9d669763, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x5a209365, __VMLINUX_SYMBOL_STR(device_initialize) },
	{ 0x31659e94, __VMLINUX_SYMBOL_STR(kill_fasync) },
	{ 0xadcb92c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0xed975377, __VMLINUX_SYMBOL_STR(mutex_lock_interruptible_nested) },
	{ 0x99bb8806, __VMLINUX_SYMBOL_STR(memmove) },
	{ 0xe37015d4, __VMLINUX_SYMBOL_STR(dev_set_name) },
	{ 0x8d522714, __VMLINUX_SYMBOL_STR(__rcu_read_lock) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("input:b*v*p*e*-e*k*r*a*m*l*s*f*w*");

MODULE_INFO(srcversion, "C61556F26619CD7D0DCAFFE");
