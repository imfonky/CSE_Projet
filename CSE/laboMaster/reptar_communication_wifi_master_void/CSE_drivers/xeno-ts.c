/* Input handler wrapper similar to standard Linux evdev but passing events to
 * Xenomai "device" instead of userspace.
 * Based on reptar linux 3.0.12 evdev.c */

//#define EVDEV_MINOR_BASE	64
#define EVDEV_MINOR_BASE	96 /* Do not overlap with standard evdev and other input dev */
#define EVDEV_MINORS		32
#define EVDEV_MIN_BUFFER_SIZE	64U
#define EVDEV_BUF_PACKETS	8

#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/major.h>
#include <linux/device.h>
#include <drivers/input/input-compat.h>	/* Private Linux input header in drivers/input */

#include "xeno-ts.h"

struct evdev {
	int open;
	int minor;
	struct input_handle handle;
	wait_queue_head_t wait;
	struct evdev_client __rcu *grab;
	struct list_head client_list;
	spinlock_t client_lock; /* protects client_list */
	struct mutex mutex;
	struct device dev;
	bool exist;
};

struct evdev_client {
	unsigned int head;
	unsigned int tail;
	unsigned int packet_head; /* [future] position of the first element of next packet */
	spinlock_t buffer_lock; /* protects access to buffer, head and tail */
	struct fasync_struct *fasync;
	struct evdev *evdev;
	struct list_head node;
	unsigned int bufsize;
	struct input_event buffer[];
};

/* Global client reference for tracking evdev client normally stored in file->private_data */
static struct evdev_client *xeno_client;

static struct evdev *xeno_evdev_table[EVDEV_MINORS];

static DEFINE_MUTEX(evdev_table_mutex);

/* An input_event received from a attached device is forwarded (copied)
 * into client/reader buffer */
static void xeno_evdev_pass_event(struct evdev_client *client,
			     struct input_event *event)
{
	/* Interrupts are disabled, just acquire the lock. */
	spin_lock(&client->buffer_lock);

	client->buffer[client->head++] = *event;
	client->head &= client->bufsize - 1;

	if (unlikely(client->head == client->tail)) {
		/*
		 * This effectively "drops" all unconsumed events, leaving
		 * EV_SYN/SYN_DROPPED plus the newest event in the queue.
		 */
		client->tail = (client->head - 2) & (client->bufsize - 1);

		client->buffer[client->tail].time = event->time;
		client->buffer[client->tail].type = EV_SYN;
		client->buffer[client->tail].code = SYN_DROPPED;
		client->buffer[client->tail].value = 0;

		client->packet_head = client->tail;
	}

	if (event->type == EV_SYN && event->code == SYN_REPORT) {
		client->packet_head = client->head;
		kill_fasync(&client->fasync, SIGIO, POLL_IN);
	}

	spin_unlock(&client->buffer_lock);
}

/* Pass incoming event to all connected clients (=opened fds). */
/* called by input core's input_pass_event() */
static void xeno_evdev_event(struct input_handle *handle,
			unsigned int type, unsigned int code, int value)
{
	struct evdev *evdev = handle->private;
	struct evdev_client *client;
	struct input_event event;

	do_gettimeofday(&event.time);
	event.type = type;
	event.code = code;
	event.value = value;

	rcu_read_lock();

	client = rcu_dereference(evdev->grab);
	if (client)
		xeno_evdev_pass_event(client, &event);
	else
		list_for_each_entry_rcu(client, &evdev->client_list, node)
			xeno_evdev_pass_event(client, &event);

	rcu_read_unlock();

	if (type == EV_SYN && code == SYN_REPORT)
		wake_up_interruptible(&evdev->wait);
}

#if 0 /* Unused with xenomai */
static int evdev_fasync(int fd, struct file *file, int on)
{
	struct evdev_client *client = file->private_data;

	return fasync_helper(fd, file, on, &client->fasync);
}

static int evdev_flush(struct file *file, fl_owner_t id)
{
	struct evdev_client *client = file->private_data;
	struct evdev *evdev = client->evdev;
	int retval;

	retval = mutex_lock_interruptible(&evdev->mutex);
	if (retval)
		return retval;

	if (!evdev->exist)
		retval = -ENODEV;
	else
		retval = input_flush_device(&evdev->handle, file);

	mutex_unlock(&evdev->mutex);
	return retval;
}
#endif // 0

/* Called as release callback of top level struct device */
static void xeno_evdev_free(struct device *dev)
{
	struct evdev *evdev = container_of(dev, struct evdev, dev);

	input_put_device(evdev->handle.dev);
	kfree(evdev);
}

/*
 * Grabs an event device (along with underlying input device).
 * This function is called with evdev->mutex taken.
 */
static int evdev_grab(struct evdev *evdev, struct evdev_client *client)
{
	int error;

	if (evdev->grab)
		return -EBUSY;

	error = input_grab_device(&evdev->handle);
	if (error)
		return error;

	rcu_assign_pointer(evdev->grab, client);

	return 0;
}

static int evdev_ungrab(struct evdev *evdev, struct evdev_client *client)
{
	if (evdev->grab != client)
		return  -EINVAL;

	rcu_assign_pointer(evdev->grab, NULL);
	synchronize_rcu();
	input_release_device(&evdev->handle);

	return 0;
}

/* Called from open() to add the caller to the client list */
static void evdev_attach_client(struct evdev *evdev,
				struct evdev_client *client)
{
	spin_lock(&evdev->client_lock);
	list_add_tail_rcu(&client->node, &evdev->client_list);
	spin_unlock(&evdev->client_lock);
}

/* Called from close() to remove the caller from the client list */
static void evdev_detach_client(struct evdev *evdev,
				struct evdev_client *client)
{
	spin_lock(&evdev->client_lock);
	list_del_rcu(&client->node);
	spin_unlock(&evdev->client_lock);
	synchronize_rcu();
}

/* Open the underlying physical input_dev device */
static int evdev_open_device(struct evdev *evdev)
{
	int retval;

	retval = mutex_lock_interruptible(&evdev->mutex);
	if (retval)
		return retval;

	if (!evdev->exist)
		retval = -ENODEV;
	else if (!evdev->open++) {
		retval = input_open_device(&evdev->handle);
		if (retval)
			evdev->open--;
	}

	mutex_unlock(&evdev->mutex);
	return retval;
}

/* Close the underlying physical input_dev device */
static void evdev_close_device(struct evdev *evdev)
{
	mutex_lock(&evdev->mutex);

	if (evdev->exist && !--evdev->open)
		input_close_device(&evdev->handle);

	mutex_unlock(&evdev->mutex);
}

/*
 * Wake up users waiting for IO so they can disconnect from
 * dead device.
 */
static void evdev_hangup(struct evdev *evdev)
{
	struct evdev_client *client;

	spin_lock(&evdev->client_lock);
	list_for_each_entry(client, &evdev->client_list, node)
		kill_fasync(&client->fasync, SIGIO, POLL_HUP);
	spin_unlock(&evdev->client_lock);

	wake_up_interruptible(&evdev->wait);
}

//static int evdev_release(struct inode *inode, struct file *file)
static int xeno_evdev_release(void)
{
	//struct evdev_client *client = file->private_data;
	struct evdev_client *client = xeno_client;
	struct evdev *evdev = client->evdev;

	mutex_lock(&evdev->mutex);
	if (evdev->grab == client)
		evdev_ungrab(evdev, client);
	mutex_unlock(&evdev->mutex);

	evdev_detach_client(evdev, client);
	kfree(client);

	evdev_close_device(evdev);
	put_device(&evdev->dev);

	return 0;
}


static unsigned int evdev_compute_buffer_size(struct input_dev *dev)
{
	unsigned int n_events =
		max(dev->hint_events_per_packet * EVDEV_BUF_PACKETS,
		    EVDEV_MIN_BUFFER_SIZE);

	return roundup_pow_of_two(n_events);
}


// static int evdev_open(struct inode *inode, struct file *file)
static int xeno_evdev_open(void)
{
	struct evdev *evdev;
	struct evdev_client *client;
	//int i = iminor(inode) - EVDEV_MINOR_BASE;
	int i = 0;

	unsigned int bufsize;
	int error;

	if (i >= EVDEV_MINORS)
		return -ENODEV;

	error = mutex_lock_interruptible(&evdev_table_mutex);
	if (error)
		return error;
	evdev = xeno_evdev_table[i];
	if (evdev)
		get_device(&evdev->dev);
	mutex_unlock(&evdev_table_mutex);

	if (!evdev)
		return -ENODEV;

	bufsize = evdev_compute_buffer_size(evdev->handle.dev);

	client = kzalloc(sizeof(struct evdev_client) +
				bufsize * sizeof(struct input_event),
			 GFP_KERNEL);
	if (!client) {
		error = -ENOMEM;
		goto err_put_evdev;
	}

	client->bufsize = bufsize;
	spin_lock_init(&client->buffer_lock);
	client->evdev = evdev;

	/* Add the newly created client to the list of cliebts to be notified on event */
	evdev_attach_client(evdev, client);

	error = evdev_open_device(evdev);
	if (error)
		goto err_free_client;

	// No inode, no file
	//file->private_data = client;
	//nonseekable_open(inode, file);

	// Local reference replacing file->private_data
	xeno_client = client;

	return 0;

 err_free_client:
	evdev_detach_client(evdev, client);
	kfree(client);
 err_put_evdev:
	put_device(&evdev->dev);
	return error;
}

#if 0 /* */
static ssize_t evdev_write(struct file *file, const char __user *buffer,
			   size_t count, loff_t *ppos)
{
	struct evdev_client *client = file->private_data;
	struct evdev *evdev = client->evdev;
	struct input_event event;
	int retval;

	if (count < input_event_size())
		return -EINVAL;

	retval = mutex_lock_interruptible(&evdev->mutex);
	if (retval)
		return retval;

	if (!evdev->exist) {
		retval = -ENODEV;
		goto out;
	}

	do {
		if (input_event_from_user(buffer + retval, &event)) {
			retval = -EFAULT;
			goto out;
		}
		retval += input_event_size();

		input_inject_event(&evdev->handle,
				   event.type, event.code, event.value);
	} while (retval + input_event_size() <= count);

 out:
	mutex_unlock(&evdev->mutex);
	return retval;
}
#endif // 0

static int evdev_fetch_next_event(struct evdev_client *client,
				  struct input_event *event)
{
	int have_event;

	spin_lock_irq(&client->buffer_lock);

	have_event = client->head != client->tail;
	if (have_event) {
		*event = client->buffer[client->tail++];
		client->tail &= client->bufsize - 1;
	}

	spin_unlock_irq(&client->buffer_lock);

	return have_event;
}

//static ssize_t evdev_read(struct file *file, char __user *buffer, size_t count, loff_t *ppos)
static ssize_t xeno_evdev_read(char *buffer, size_t count, int f_flags)
{
	/*struct evdev_client *client = file->private_data;*/
	struct evdev_client *client = xeno_client;
	struct evdev *evdev = client->evdev;
	struct input_event event;
	int retval;

	f_flags |= O_RDONLY;

	if (count < input_event_size())
		return -EINVAL;

	if (client->packet_head == client->tail && evdev->exist &&
	    (f_flags & O_NONBLOCK)) {
		return -EAGAIN;
	}

	retval = wait_event_interruptible(evdev->wait,
		client->packet_head != client->tail || !evdev->exist);
	if (retval){
		printk("wait failed!\n");
		return retval;
	}
	if (!evdev->exist)
		return -ENODEV;

	while (retval + input_event_size() <= count &&
	       evdev_fetch_next_event(client, &event)) {

		memcpy(buffer + retval, &event, sizeof(struct input_event));

		retval += input_event_size();
	}

	return retval;
}
#if 0
/* No kernel lock - fine */
static unsigned int evdev_poll(struct file *file, poll_table *wait)
{
	/*struct evdev_client *client = file->private_data;*/
	struct evdev_client *client = xeno_client;
	struct evdev *evdev = client->evdev;
	unsigned int mask;

	poll_wait(file, &evdev->wait, wait);

	mask = evdev->exist ? POLLOUT | POLLWRNORM : POLLHUP | POLLERR;
	if (client->packet_head != client->tail)
		mask |= POLLIN | POLLRDNORM;

	return mask;
}
#endif

//static int bits_to_user(unsigned long *bits, unsigned int maxbit, unsigned int maxlen, void __user *p, int compat)
static int bits_to_user(unsigned long *bits, unsigned int maxbit, unsigned int maxlen, void *p, int compat)
{
	int len = BITS_TO_LONGS(maxbit) * sizeof(long);

	if (len > maxlen)
		len = maxlen;

	/* return copy_to_user(p, bits, len) ? -EFAULT : len; */
	memcpy(p, bits, len);
	return len;
}

/* Simple copy in kernel space */
static int str_to_user(const char *str, unsigned int maxlen, void *p)
//static int str_to_user(const char *str, unsigned int maxlen, void __user *p)
{
	int len;

	if (!str)
		return -ENOENT;

	len = strlen(str) + 1;
	if (len > maxlen)
		len = maxlen;

	/* return copy_to_user(p, str, len) ? -EFAULT : len; */
	memcpy(p, str, len);
	return len;
}

#define OLD_KEY_MAX	0x1ff
/*static int handle_eviocgbit(struct input_dev *dev, unsigned int type, unsigned int size, void __user *p, int compat_mode)*/
static int handle_eviocgbit(struct input_dev *dev, unsigned int type, unsigned int size, void *p, int compat_mode)
{
	static unsigned long keymax_warn_time;
	unsigned long *bits;
	int len;

	switch (type) {

	case      0: bits = dev->evbit;  len = EV_MAX;  break;
	case EV_KEY: bits = dev->keybit; len = KEY_MAX; break;
	case EV_REL: bits = dev->relbit; len = REL_MAX; break;
	case EV_ABS: bits = dev->absbit; len = ABS_MAX; break;
	case EV_MSC: bits = dev->mscbit; len = MSC_MAX; break;
	case EV_LED: bits = dev->ledbit; len = LED_MAX; break;
	case EV_SND: bits = dev->sndbit; len = SND_MAX; break;
	case EV_FF:  bits = dev->ffbit;  len = FF_MAX;  break;
	case EV_SW:  bits = dev->swbit;  len = SW_MAX;  break;
	default: return -EINVAL;
	}

	/*
	 * Work around bugs in userspace programs that like to do
	 * EVIOCGBIT(EV_KEY, KEY_MAX) and not realize that 'len'
	 * should be in bytes, not in bits.
	 */
	if (type == EV_KEY && size == OLD_KEY_MAX) {
		len = OLD_KEY_MAX;
		if (printk_timed_ratelimit(&keymax_warn_time, 10 * 1000))
			printk("(EVIOCGBIT): Suspicious buffer size %u, "
				   "limiting output to %zu bytes. See "
				   "http://userweb.kernel.org/~dtor/eviocgbit-bug.html\n",
				   OLD_KEY_MAX,
				   BITS_TO_LONGS(OLD_KEY_MAX) * sizeof(long));
	}

	return bits_to_user(bits, len, size, p, compat_mode);
}
#undef OLD_KEY_MAX

/*static int evdev_handle_get_keycode(struct input_dev *dev, void __user *p)*/
static int evdev_handle_get_keycode(struct input_dev *dev, void *p)
{
	struct input_keymap_entry ke = {
		.len	= sizeof(unsigned int),
		.flags	= 0,
	};
	int *ip = (int *)p;
	int error;

	/* legacy case */
	/* if (copy_from_user(ke.scancode, p, sizeof(unsigned int)))
		return -EFAULT;
	*/
	memcpy(ke.scancode, p, sizeof(unsigned int));

	error = input_get_keycode(dev, &ke);
	if (error)
		return error;

	/*
	if (put_user(ke.keycode, ip + 1))
		return -EFAULT;
	 */
	*(ip + 1) = ke.keycode;

	return 0;
}

/*static int evdev_handle_get_keycode_v2(struct input_dev *dev, void __user *p)*/
static int evdev_handle_get_keycode_v2(struct input_dev *dev, void *p)
{
	struct input_keymap_entry ke;
	int error;

	/*
	if (copy_from_user(&ke, p, sizeof(ke)))
		return -EFAULT;
	*/
	memcpy(&ke, p, sizeof(ke));

	error = input_get_keycode(dev, &ke);
	if (error)
		return error;

	/*
	if (copy_to_user(p, &ke, sizeof(ke)))
		return -EFAULT;
	*/
	memcpy(p, &ke, sizeof(ke));
	return 0;
}

/*static int evdev_handle_set_keycode(struct input_dev *dev, void __user *p)*/
static int evdev_handle_set_keycode(struct input_dev *dev, void *p)
{
	struct input_keymap_entry ke = {
		.len	= sizeof(unsigned int),
		.flags	= 0,
	};
	int *ip = (int *)p;

	/*
	if (copy_from_user(ke.scancode, p, sizeof(unsigned int)))
		return -EFAULT;
	*/
	memcpy(ke.scancode, p, sizeof(unsigned int));


	/* if (get_user(ke.keycode, ip + 1))
		return -EFAULT;
	*/
	ke.keycode =  *(ip + 1);

	return input_set_keycode(dev, &ke);
}

/*static int evdev_handle_set_keycode_v2(struct input_dev *dev, void __user *p)*/
static int evdev_handle_set_keycode_v2(struct input_dev *dev, void *p)
{
	struct input_keymap_entry ke;

	/*
	if (copy_from_user(&ke, p, sizeof(ke)))
		return -EFAULT;
	*/
	memcpy(&ke, p, sizeof(ke));

	if (ke.len > sizeof(ke.scancode))
		return -EINVAL;

	return input_set_keycode(dev, &ke);
}

/*static long evdev_do_ioctl(struct file *file, unsigned int cmd, void __user *p, int compat_mode)*/
static long evdev_do_ioctl(unsigned int cmd, void *p, int compat_mode)
{
	/*struct evdev_client *client = file->private_data;*/
	struct evdev_client *client = xeno_client;

	struct evdev *evdev = client->evdev;
	struct input_dev *dev = evdev->handle.dev;
	struct input_absinfo abs;
	/*struct ff_effect effect;*/
	/*int __user *ip = (int __user *)p;*/
	int *ip = (int *)p;
	unsigned int i, t, u, v;
	unsigned int size;
	/*int error;*/

	/* First we check for fixed-length commands */
	switch (cmd) {
	case EVIOCGVERSION:
		*ip = EV_VERSION;
		return 0;
	case EVIOCGID:
		/*
		if (copy_to_user(p, &dev->id, sizeof(struct input_id)))
			return -EFAULT;
		*/
		memcpy(p, &dev->id, sizeof(struct input_id));
		return 0;

	case EVIOCGREP:
		if (!test_bit(EV_REP, dev->evbit))
			return -ENOSYS;
		/*
		if (put_user(dev->rep[REP_DELAY], ip))
			return -EFAULT;
		if (put_user(dev->rep[REP_PERIOD], ip + 1))
			return -EFAULT;
		*/
		*ip = dev->rep[REP_DELAY];
		*(ip+1) = dev->rep[REP_PERIOD];
		return 0;

	case EVIOCSREP:
		if (!test_bit(EV_REP, dev->evbit))
			return -ENOSYS;
		/*
		if (get_user(u, ip))
			return -EFAULT;
		if (get_user(v, ip + 1))
			return -EFAULT;
		*/
		u = *ip;
		v = *(ip+1);
		input_inject_event(&evdev->handle, EV_REP, REP_DELAY, u);
		input_inject_event(&evdev->handle, EV_REP, REP_PERIOD, v);

		return 0;

	case EVIOCRMFF:
		/* No concept of file in this handler. Force feedback not required --> ioctl ignored */
		/*return input_ff_erase(dev, (int)(unsigned long) p, file);*/
		return 0;
	case EVIOCGEFFECTS:
		i = test_bit(EV_FF, dev->evbit) ?
				dev->ff->max_effects : 0;
		/*
		 if (put_user(i, ip))
			return -EFAULT;
		*/
		*ip = i;
		return 0;

	case EVIOCGRAB:
		if (p)
			return evdev_grab(evdev, client);
		else
			return evdev_ungrab(evdev, client);

	case EVIOCGKEYCODE:
		return evdev_handle_get_keycode(dev, p);

	case EVIOCSKEYCODE:
		return evdev_handle_set_keycode(dev, p);

	case EVIOCGKEYCODE_V2:
		return evdev_handle_get_keycode_v2(dev, p);

	case EVIOCSKEYCODE_V2:
		return evdev_handle_set_keycode_v2(dev, p);
	}

	size = _IOC_SIZE(cmd);

	/* Now check variable-length commands */
#define EVIOC_MASK_SIZE(nr)	((nr) & ~(_IOC_SIZEMASK << _IOC_SIZESHIFT))
	switch (EVIOC_MASK_SIZE(cmd)) {

	case EVIOCGPROP(0):
		return bits_to_user(dev->propbit, INPUT_PROP_MAX,
				    size, p, compat_mode);

	case EVIOCGKEY(0):
		return bits_to_user(dev->key, KEY_MAX, size, p, compat_mode);

	case EVIOCGLED(0):
		return bits_to_user(dev->led, LED_MAX, size, p, compat_mode);

	case EVIOCGSND(0):
		return bits_to_user(dev->snd, SND_MAX, size, p, compat_mode);

	case EVIOCGSW(0):
		return bits_to_user(dev->sw, SW_MAX, size, p, compat_mode);

	case EVIOCGNAME(0):
		return str_to_user(dev->name, size, p);

	case EVIOCGPHYS(0):
		return str_to_user(dev->phys, size, p);

	case EVIOCGUNIQ(0):
		return str_to_user(dev->uniq, size, p);

	case EVIOC_MASK_SIZE(EVIOCSFF):
		/*
		 if (input_ff_effect_from_user(p, size, &effect))
			return -EFAULT;

		error = input_ff_upload(dev, &effect, file);

		if (put_user(effect.id, &(((struct ff_effect __user *)p)->id)))
			return -EFAULT;
		*/
		return -ENOSYS;
	}

	/* Multi-number variable-length handlers */
	if (_IOC_TYPE(cmd) != 'E')
		return -EINVAL;

	if (_IOC_DIR(cmd) == _IOC_READ) {

		if ((_IOC_NR(cmd) & ~EV_MAX) == _IOC_NR(EVIOCGBIT(0, 0)))
			return handle_eviocgbit(dev,
						_IOC_NR(cmd) & EV_MAX, size,
						p, compat_mode);

		if ((_IOC_NR(cmd) & ~ABS_MAX) == _IOC_NR(EVIOCGABS(0))) {

			if (!dev->absinfo)
				return -EINVAL;

			t = _IOC_NR(cmd) & ABS_MAX;
			abs = dev->absinfo[t];
			/*
			if (copy_to_user(p, &abs, min_t(size_t,
					size, sizeof(struct input_absinfo))))
				return -EFAULT;
			*/
			memcpy(p, &abs, min_t(size_t, size, sizeof(struct input_absinfo)));
			return 0;
		}
	}

	if (_IOC_DIR(cmd) == _IOC_WRITE) {

		if ((_IOC_NR(cmd) & ~ABS_MAX) == _IOC_NR(EVIOCSABS(0))) {

			if (!dev->absinfo)
				return -EINVAL;

			t = _IOC_NR(cmd) & ABS_MAX;

			/*
			if (copy_from_user(&abs, p, min_t(size_t,
					size, sizeof(struct input_absinfo))))
				return -EFAULT;
			*/
			memcpy(&abs, p, min_t(size_t, size, sizeof(struct input_absinfo)));
			if (size < sizeof(struct input_absinfo))
				abs.resolution = 0;

			/* We can't change number of reserved MT slots */
			if (t == ABS_MT_SLOT)
				return -EINVAL;

			/*
			 * Take event lock to ensure that we are not
			 * changing device parameters in the middle
			 * of event.
			 */
			spin_lock_irq(&dev->event_lock);
			dev->absinfo[t] = abs;
			spin_unlock_irq(&dev->event_lock);

			return 0;
		}
	}

	return -EINVAL;
}

/* static long evdev_ioctl_handler(struct file *file, unsigned int cmd, void __user *p, int compat_mode)*/
static long evdev_ioctl_handler(unsigned int cmd, void *p, int compat_mode)
{
	/*struct evdev_client *client = file->private_data;*/
	struct evdev_client *client = xeno_client;
	struct evdev *evdev = client->evdev;
	int retval;
#if 0
	/* This mutex lock generates exceptions with Xenomai !
	 * evdev and evdev->mutex are valid */
	printk("%s/%d client/evdev %p/%p/%p\n", __FUNCTION__,__LINE__, client, evdev, &evdev->mutex);
	retval = mutex_lock_interruptible(&evdev->mutex);
	printk("%s/%d\n", __FUNCTION__,__LINE__);
	if (retval)
		return retval;
#endif

	if (!evdev->exist) {
		retval = -ENODEV;
		goto out;
	}

	retval = evdev_do_ioctl(cmd, p, compat_mode);

 out:
#if 0
	mutex_unlock(&evdev->mutex);
#endif
	return retval;
}

static long xeno_evdev_ioctl(unsigned int cmd, unsigned long arg)
{
	//printk("%s/%d\n", __FUNCTION__,__LINE__);
	return evdev_ioctl_handler(cmd, (void *)arg, 0);
}

static int evdev_install_chrdev(struct evdev *evdev)
{
	/*
	 * No need to do any locking here as calls to connect and
	 * disconnect are serialized by the input core
	 */
	xeno_evdev_table[evdev->minor] = evdev;
	return 0;
}

static void evdev_remove_chrdev(struct evdev *evdev)
{
	/*
	 * Lock evdev table to prevent race with evdev_open()
	 */
	mutex_lock(&evdev_table_mutex);
	xeno_evdev_table[evdev->minor] = NULL;
	mutex_unlock(&evdev_table_mutex);
}

/*
 * Mark device non-existent. This disables writes, ioctls and
 * prevents new users from opening the device. Already posted
 * blocking reads will stay, however new ones will fail.
 */
static void evdev_mark_dead(struct evdev *evdev)
{
	mutex_lock(&evdev->mutex);
	evdev->exist = false;
	mutex_unlock(&evdev->mutex);
}

static void evdev_cleanup(struct evdev *evdev)
{
	struct input_handle *handle = &evdev->handle;

	evdev_mark_dead(evdev);
	evdev_hangup(evdev);
	evdev_remove_chrdev(evdev);

	/* evdev is marked dead so no one else accesses evdev->open */
	if (evdev->open) {
		input_flush_device(handle, NULL);
		input_close_device(handle);
	}
}

/*
 * Create new evdev device. Note that input core serializes calls
 * to connect and disconnect so we don't need to lock evdev_table here.
 */
static int xeno_evdev_connect(struct input_handler *handler, struct input_dev *dev,
			 const struct input_device_id *id)
{
	struct evdev *evdev;
	int minor;
	int error;

	/* Find the first available minor */
	for (minor = 0; minor < EVDEV_MINORS; minor++)
		if (!xeno_evdev_table[minor])
			break;

	if (minor == EVDEV_MINORS) {
		printk("no more free evdev devices\n");
		return -ENFILE;
	}

	evdev = kzalloc(sizeof(struct evdev), GFP_KERNEL);
	if (!evdev)
		return -ENOMEM;

	INIT_LIST_HEAD(&evdev->client_list);
	spin_lock_init(&evdev->client_lock);
	mutex_init(&evdev->mutex);
	init_waitqueue_head(&evdev->wait);

	/*dev_set_name(&evdev->dev, "event%d", minor);*/
	dev_set_name(&evdev->dev, "xenoevent%d", minor);
	evdev->exist = true;
	evdev->minor = minor;

	evdev->handle.dev = input_get_device(dev);
	evdev->handle.name = dev_name(&evdev->dev);
	evdev->handle.handler = handler;
	evdev->handle.private = evdev;

	evdev->dev.devt = MKDEV(INPUT_MAJOR, EVDEV_MINOR_BASE + minor);
	evdev->dev.class = &input_class;
	evdev->dev.parent = &dev->dev;
	evdev->dev.release = xeno_evdev_free;
	device_initialize(&evdev->dev);

	/* Register the input handle (=handler/device association) */
	error = input_register_handle(&evdev->handle);
	if (error)
		goto err_free_evdev;

	error = evdev_install_chrdev(evdev);
	if (error)
		goto err_unregister_handle;

	error = device_add(&evdev->dev);
	if (error)
		goto err_cleanup_evdev;

	return 0;

 err_cleanup_evdev:
	evdev_cleanup(evdev);
 err_unregister_handle:
	input_unregister_handle(&evdev->handle);
 err_free_evdev:
	put_device(&evdev->dev);
	return error;
}

static void xeno_evdev_disconnect(struct input_handle *handle)
{
	struct evdev *evdev = handle->private;

	device_del(&evdev->dev);
	evdev_cleanup(evdev);
	input_unregister_handle(handle);
	put_device(&evdev->dev);
}

static const struct input_device_id xeno_evdev_ids[] = {
	{ .driver_info = 1 },	/* Matches all devices */
	{ },			/* Terminating zero entry */
};

MODULE_DEVICE_TABLE(input, xeno_evdev_ids);

static struct input_handler xeno_evdev_handler = {
	.event		= xeno_evdev_event,
	.connect	= xeno_evdev_connect,
	.disconnect	= xeno_evdev_disconnect,
	.minor		= EVDEV_MINOR_BASE,
	.name		= "evdev",
	.id_table	= xeno_evdev_ids,
};


/* tslib wrappers in kernel **********************************/

struct tslib_input {
	//struct tslib_module_info module;

	int	current_x;
	int	current_y;
	int	current_p;

	int	sane_fd;
	int	using_syn;
} inf;

static long xeno_evdev_ioctl(unsigned int cmd, unsigned long arg);

static int check_fd(struct tslib_input *i)
{
	//struct tsdev *ts = i->module.dev;
	unsigned int version;
	u_int32_t bit;
	u_int64_t absbit;

	/* Underlying device must:
	 * - be compiled for the right evdev version
	 * - report EV_ABS events
	 * - be able to report ABS_X, ABS_Y events
	 * - if EV_PRESSURE events are reported they will be used for touch detection else
	 *   pressure is forced depending on BTN_TOUCH state */
	if (! ((xeno_evdev_ioctl(EVIOCGVERSION, (unsigned long)&version) >= 0) &&
		(version == EV_VERSION) &&
		(xeno_evdev_ioctl(EVIOCGBIT(0, sizeof(bit) * 8), (unsigned long)&bit) >= 0) &&
		(bit & (1 << EV_ABS)) &&
		(xeno_evdev_ioctl(EVIOCGBIT(EV_ABS, sizeof(absbit) * 8), (unsigned long)&absbit) >= 0) &&
		(absbit & (1 << ABS_X)) &&
		(absbit & (1 << ABS_Y)))) {
		printk("selected device is not a touchscreen I understand\n");
		return -1;
	}

	/* If the underlying touch device does not report pressure events
	 * force it to 255 by default and to 0 only when BTN_TOUCH = 0 is reported */
	if (!(absbit & (1 << ABS_PRESSURE)))
		i->current_p = 255;

	if (bit & (1 << EV_SYN))
		i->using_syn = 1;

	return 0;
}

int xeno_input_read(struct ts_sample *samp, int nr, int f_flags) {
  struct tslib_input *i = (struct tslib_input *)&inf;
  //struct tsdev *ts = inf->dev;
  struct input_event ev;
  int ret = nr;
  int total = 0;

	if (i->sane_fd == 0)
		i->sane_fd = check_fd(i);

	if (i->sane_fd == -1)
 		return 0;

	if (i->using_syn) {
		while (total < nr) {
			ret = xeno_evdev_read((char *)&ev, sizeof(struct input_event), f_flags);
			if (ret < (int)sizeof(struct input_event)) {
				total = -1;
				break;
			}

			switch (ev.type) {
			case EV_KEY:
				switch (ev.code) {
				case BTN_TOUCH:
					if (ev.value == 0) {
						/* pen up */
						samp->x = 0;
						samp->y = 0;
						samp->pressure = 0;
						samp->tv = ev.time;
						samp++;
						total++;
					}
					break;
				}
				break;
			case EV_SYN:
				/* Fill out a new complete event */
				samp->x = i->current_x;
				samp->y = i->current_y;
				samp->pressure = i->current_p;
				samp->tv = ev.time;
	#ifdef DEBUG
				fprintf(stderr, "RAW---------------------> %d %d %d %d.%d\n",
						samp->x, samp->y, samp->pressure, samp->tv.tv_sec,
						samp->tv.tv_usec);
	#endif		 /*DEBUG*/
					samp++;
				total++;
				break;
			case EV_ABS:
				switch (ev.code) {
				case ABS_X:
					//printk("ABS_X %d \n",ev.value);
					i->current_x = ev.value;
					break;
				case ABS_Y:
					//printk("ABS_Y %d \n",ev.value);
					i->current_y = ev.value;
					break;
				case ABS_PRESSURE:
					//printk("ABS_PRESSURE %d \n",ev.value);
					i->current_p = ev.value;
					break;
				}
				break;
			}
		}
		ret = total;
	} else {
		unsigned char *p = (unsigned char *) &ev;
		int len = sizeof(struct input_event);

		while (total < nr) {
			ret = xeno_evdev_read(p, len, f_flags);
			if (ret == -1) {
				break;
			}

			if (ret < (int)sizeof(struct input_event)) {
				/* short read
				 * restart read to get the rest of the event
					 */
					p += ret;
					len -= ret;
					continue;
				}
				/* successful read of a whole event */

				if (ev.type == EV_ABS) {
					switch (ev.code) {
					case ABS_X:
						if (ev.value != 0) {
							samp->x = i->current_x = ev.value;
							samp->y = i->current_y;
							samp->pressure = i->current_p;
						} else {
							printk("tslib: dropped x = 0\n");
							continue;
						}
						break;
					case ABS_Y:
						if (ev.value != 0) {
							samp->x = i->current_x;
							samp->y = i->current_y = ev.value;
							samp->pressure = i->current_p;
						} else {
							printk("tslib: dropped y = 0\n");
							continue;
						}
						break;
					case ABS_PRESSURE:
						samp->x = i->current_x;
						samp->y = i->current_y;
						samp->pressure = i->current_p = ev.value;
						break;
					}
					samp->tv = ev.time;
		#ifdef DEBUG
					fprintf(stderr, "RAW---------------------------> %d %d %d\n",
						samp->x, samp->y, samp->pressure);
		#endif	 /*DEBUG*/
					samp++;
					total++;
				} else if (ev.type == EV_KEY) {
					switch (ev.code) {
					case BTN_TOUCH:
						if (ev.value == 0) {
							/* pen up */
							samp->x = 0;
							samp->y = 0;
							samp->pressure = 0;
							samp->tv = ev.time;
							samp++;
							total++;
						}
						break;
					}
				} else {
					printk("tslib: Unknown event type %d\n", ev.type);
				}
				p = (unsigned char *) &ev;
			}
			ret = total;
		}

		return ret;
}
/* end of tslib wrappers in kernel ***************************/

extern void linear_init(void);

int xeno_ts_init(void)
{
	struct tslib_input *i = &inf;
	int ret;

	/* Register our input_handler into input core.
	 * This kicks attachment with all compatible and already registered input
	 * devices with calls to the .connect callback of the handler. */
	ret = input_register_handler(&xeno_evdev_handler);

	if (ret) {
		printk("%s failed to register xeno_evdev_handler: %d\n", __FUNCTION__, ret);
		return ret;
	}

	/* Open our local "file" */
	xeno_evdev_open();

	/* Initialize tslib compatible structure */
	//i->module.ops = &__ts_input_ops;
	i->current_x = 0;
	i->current_y = 0;
	i->current_p = 0;
	i->sane_fd = 0;
	i->using_syn = 0;


	linear_init();

	return ret;
}

void xeno_ts_exit(void)
{
	input_unregister_handler(&xeno_evdev_handler);
	xeno_evdev_release();
}

EXPORT_SYMBOL(xeno_ts_init);
EXPORT_SYMBOL(xeno_ts_exit);
EXPORT_SYMBOL(xeno_input_read);

